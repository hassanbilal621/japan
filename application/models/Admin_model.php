<?php
class Admin_model extends CI_Model{
    
    public function login($username, $password){

        $this->db->where('username', $username);
        $result = $this->db->get('admin');

        if($result->num_rows() == 1){
			$hash = $result->row(0)->password;
			
			if (password_verify($password, $hash))
			{
				return $result->row(0)->id;
			}
			else
			{
				return false;
			}
			
			
        } else {
            return false;
        }
	}


	public function insert($data)
	{
		$this->db->insert('stocks', $data);
		$stockid = $this->db->insert_id();

	
		return $stockid;
	}

	public function insert_auction($stockid, $dataauction){
		$dataauction['stockid'] = $stockid;
		
		$this->security->xss_clean($dataauction);
        $this->db->insert('auction', $dataauction);

	}
	
	public function insert_payment($stockid, $datapayment){
		$datapayment['stockid'] = $stockid;
		
		$this->security->xss_clean($datapayment);
        $this->db->insert('payment', $datapayment);
	}
	public function insert_price($stockid, $dataprice){
		$dataprice['stockid'] = $stockid;
		
		$this->security->xss_clean($dataprice);
        $this->db->insert('price', $dataprice);
	}

	public function insert_shipment($stockid, $datashipment){

		$datashipment['stockid'] = $stockid;
			
		$this->security->xss_clean($datashipment);
		$this->db->insert('shipment', $datashipment);
		}

	public function insert_document($stockid, $datadocument){
		$datadocument['stockid'] = $stockid;
		
		$this->security->xss_clean($datadocument);
        $this->db->insert('documents', $datadocument);
	}

	public function insert_ricksu($stockid, $dataricksu){
		$dataricksu['stockid'] = $stockid;
		
		$this->security->xss_clean($dataricksu);
        $this->db->insert('ricksu', $dataricksu);
	}

	public function insert_reservation($stockid, $datareservation){
		$datareservation['stockid'] = $stockid;
		
		$this->security->xss_clean($datareservation);
        $this->db->insert('reservation', $datareservation);
	}
		
	public function get_admininfo($admin_id)
	{
		$this->db->where('id', $admin_id);
		$result = $this->db->get('admin');
		
		return $result->row_array();
	}

	public function get_bloggerinfo($blogger_id)
	{
		$this->db->where('id', $blogger_id);
		$result = $this->db->get('blogger');
		
		return $result->row_array();
	}



	
	public function get_stock($stockid)
	{
		$this->db->where('stock_id', $stockid);
		$result = $this->db->get('stocks');
		
		return $result->row_array();
	}
	// public function getstockcount()
	// {
	// 	$this->db->count_all('stocks')
	// }

	public function get_auction($stockid)
	{
		$this->db->join('stocks', 'auction.stockid = stocks.stock_id', 'left');

		$this->db->where('stockid', $stockid);
		$result = $this->db->get('auction');
		
		return $result->row_array();
	}

	
	public function get_ricksu($stockid)
	{
		$this->db->join('stocks', 'ricksu.stockid = stocks.stock_id', 'left');

		$this->db->where('stockid', $stockid);
		$result = $this->db->get('ricksu');
		
		return $result->row_array();
	}

	public function get_price($stockid)
	{
		$this->db->join('stocks', 'price.stockid = stocks.stock_id', 'left');

		$this->db->where('stockid', $stockid);
		$result = $this->db->get('price');
		
		return $result->row_array();
	}

	public function get_payment($stockid)
	{
		$this->db->join('stocks', 'payment.stockid = stocks.stock_id', 'left');

		$this->db->where('stockid', $stockid);
		$result = $this->db->get('payment');
		
		return $result->row_array();
	}

	
	public function get_reserved($stockid)
	{
		$this->db->join('stocks', 'reservation.stockid = stocks.stock_id', 'left');

		$this->db->where('stockid', $stockid);
		$result = $this->db->get('reservation');
		
		return $result->row_array();
	}

	public function get_shipment($stockid)
	{
		$this->db->join('stocks', 'shipment.stockid = stocks.stock_id', 'left');

		$this->db->where('stockid', $stockid);
		$result = $this->db->get('shipment');
		
		return $result->row_array();
	}

	public function get_document($stockid)
	{
		$this->db->join('stocks', 'documents.stockid = stocks.stock_id', 'left');

		$this->db->where('stockid', $stockid);
		$result = $this->db->get('documents');
		
		return $result->row_array();
	}


	public function add_image($stockid, $imgname)
	{ 
		
		$data = array( 
			
			'stock_picture_filename' => $imgname,
			'stock_id_image' => $stockid
		);


		$this->security->xss_clean($data);
		$this->db->insert('stock_pictures', $data);
	}
	

	public function get_stock_pictures($stockid){
		$this->db->where('stock_id_image', $stockid);
        $query = $this->db->get('stock_pictures');
        return $query->result_array();
	}


	public function get_stock_pictures_all(){
        $query = $this->db->get('stock_pictures');
        return $query->result_array();
	}

	public function get_image($stockid)
	{
		$this->db->join('stocks', 'stocks.stock_id = stock_pictures.stock_id_image', 'left');
		$this->db->where('stock_id_image', $stockid);
		$result = $this->db->get('stock_pictures');
		
		return $result->row_array();
	}
	
	

	// Users Get / Add / Update / Delete 

	public function add_model()
	{
		$data = array(
            'model_name' => $this->input->post('modelname'),
			'model_maker_id' => $this->input->post('makerid')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('model', $data);
	}

	public function add_bank()
	{
		$data = array(
            'beneficiary_name' => $this->input->post('beneficiary_name'),
			'bank_name' => $this->input->post('bank_name'),
			'account_no' => $this->input->post('account_no'),
			'branch_name' => $this->input->post('branch_name'),
			'swift_code' => $this->input->post('swift_code'),
			'bank_address' => $this->input->post('bank_address'),
			'acceptable_currency' => $this->input->post('acceptable_currency')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('bank', $data);
	}

	public function get_bank(){
        $query = $this->db->get('bank');
        return $query->result_array();
	}

	public function add_nexco_invoice()
	{
		$data = array(
 
			'invoice_beneficiary_id' => $this->input->post('beneficiary_id')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('nexco_invoice', $data);
	}

	public function get_manage_invoice(){

		$this->db->join('orders', 'invoice.orderid = orders.orderid', 'left');
		$this->db->join('bank', 'invoice.bankid = bank.bank_id', 'left');
		$this->db->join('stocks', 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users', 'orders.userid = users.id', 'left');


        $query = $this->db->get('invoice');
        return $query->result_array();
	}

	public function get_paid_invoice(){

		$this->db->join('orders', 'invoice.orderid = orders.orderid', 'left');
		$this->db->join('bank', 'invoice.bankid = bank.bank_id', 'left');
		$this->db->join('stocks', 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users', 'orders.userid = users.id', 'left');

		$this->db->where('invoice.invoicestatus', 'paid');

		$this->db->order_by('invoice.invoice_id', 'DESC');
        $query = $this->db->get('invoice');
        return $query->result_array();
	}
	public function paidinvoice($invoiceid){
		$data = array(
			'invoicestatus' => "paid",
			'statusimg' => 'paid.png',
			'statusdate' => date('Y-m-d')
        );
		
		$this->security->xss_clean($data);
		$this->db->where('invoice_id', $invoiceid);
		$this->db->update('invoice', $data);

	} 

	public function update_order_paid_amount($ordertotal, $orderid){
		$data = array(
	
			'orderpaidamount' => $ordertotal
		);
		
		
		
		$this->security->xss_clean($data);
		$this->db->where('orderid', $orderid);
		$this->db->update('orders', $data);

	} 
	public function unpaidinvoice($invoiceid){
		$data = array(
			'invoicestatus' => "unpaid",
			'statusimg' => 'unpaid.png',
			'statusdate' => 'null'
        );
		
		$this->security->xss_clean($data);
		$this->db->where('invoice_id', $invoiceid);
		$this->db->update('invoice', $data);

	} 

	public function get_unpaid_invoice(){

		$this->db->join('orders', 'invoice.orderid = orders.orderid', 'left');
		$this->db->join('bank', 'invoice.bankid = bank.bank_id', 'left');
		$this->db->join('stocks', 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users', 'orders.userid = users.id', 'left');

		$this->db->where('invoice.invoicestatus', 'unpaid');

		$this->db->order_by('invoice.invoice_id', 'DESC');
        $query = $this->db->get('invoice');
        return $query->result_array();
	}

	public function add_maker()
	{
		$data = array(
            'maker_name' => $this->input->post('makername'),
			'maker_icon' => $this->input->post('makericon')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('maker', $data);
	}

	public function add_type()
	{
		$data = array(
			'type_name' => $this->input->post('typename'),
			'type_icon' => $this->input->post('typeicon')
			
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('type', $data);
	}

	public function add_countryport()
	{
		$data = array(
			'port_name' => $this->input->post('portname')
			
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('ports', $data);
	}




	public function get_countryport(){
        $query = $this->db->get('ports');
        return $query->result_array();
	}

	public function del_countryport($portid)
	{
		$this->db->where('port_id', $portid);
		$this->db->delete('ports'); 
	}

	

	
	public function add_user($imgname, $enc_password)
	{
		$data = array(
            'name' => $this->input->post('name'),
			'fathername' => $this->input->post('fname'),
			'phone' => $this->input->post('phone'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s'),
			'address' => $this->input->post('address'),
			//'country' => $this->input->post('country'),
            //'city' => $this->input->post('city'),
			'picture' => $imgname
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('users', $data);
	}

	public function update_user($user_id, $imgname)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'fathername' => $this->input->post('fname'),
			'phone' => $this->input->post('phone'),
			'picture' => $imgname
        );
		
		$this->security->xss_clean($data);
		$this->db->where('id', $user_id);
		$this->db->update('users', $data);

	}
	public function del_model($modelid)
	{
		$this->db->where('model_id', $modelid);
		$this->db->delete('model');
	}

	public function del_maker($makerid)
	{
		$this->db->where('maker_id', $makerid);
		$this->db->delete('maker');
	}

	public function del_type($typeid)
	{
		$this->db->where('type_id', $typeid);
		$this->db->delete('type');
	}


	public function del_user($userid)
	{
		$this->db->where('id', $userid);
		$this->db->delete('users');
	}

	public function get_ajax_bank($bankid){
        $this->db->where('bank_id', $bankid); 
        $result = $this->db->get('bank');
        return $result->row_array();
    }


	public function get_orders(){
		
		$this->db->join('stocks' , 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users' , 'orders.userid = users.id', 'left');
		
        $query = $this->db->get('orders');
        return $query->result_array();
	}
	
	public function deliverorder($orderid){
		$data = array(
			'status' => "deliverd"
        );
		
		$this->security->xss_clean($data);
		$this->db->where('orderid', $orderid);
		$this->db->update('orders', $data);

	}
	public function cancledorder($orderid){
		$data = array(
			'status' => "cancelled"
        );
		
		$this->security->xss_clean($data);
		$this->db->where('orderid', $orderid);
		$this->db->update('orders', $data);

	}

	public function deleteorder($orderid)
	{
		$this->db->where('orderid', $orderid);
		$this->db->delete('orders');
	}


	public function get_delivedrorder(){

		$this->db->join('stocks' , 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users' , 'orders.userid = users.id', 'left');

		$this->db->where('orders.status', 'deliverd');

		$this->db->order_by('orders.orderid', 'DESC');
        $query = $this->db->get('orders');
        return $query->result_array();
	}

	public function get_activeorders(){ 

		$this->db->join('stocks' , 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users' , 'orders.userid = users.id', 'left');
		
		$this->db->where('orders.status', 'ready');

		$this->db->order_by('orders.orderid', 'DESC');
        $query = $this->db->get('orders');
        return $query->result_array();
	}

	

	public function get_canclededorders(){
		
		$this->db->join('stocks' , 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users' , 'orders.userid = users.id', 'left');

		$this->db->where('orders.status', 'cancelled');

		$this->db->order_by('orders.orderid', 'DESC');
        $query = $this->db->get('orders');
        return $query->result_array();
	}

	public function get_order($orderid){

		$this->db->join( 'stocks' , 'orders.stockid = stocks.stock_id', 'left');

		// $this->db->join( 'invoice' , 'orders.invoiceid = invoice.invoice_id', 'left');
		$this->db->join( 'users' , 'orders.userid = users.id', 'left');
		
		$this->db->where('orders.orderid', $orderid);

		$this->db->order_by('orders.orderid', 'DESC');

		$result = $this->db->get('orders');
		
		return $result->row_array();
	}

	public function get_order_invoice($orderid){

		
		$this->db->where('orders.orderid', $orderid);

		$this->db->order_by('orders.orderid', 'DESC');

		$result = $this->db->get('orders');
		
		return $result->row_array();
	}



	public function edit_invoice($invoiceid){

		
		$this->db->join('orders', 'invoice.orderid = orders.orderid', 'left');
		$this->db->join('stocks', 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users', 'orders.userid = users.id', 'left');

		$this->db->where('invoice.invoice_id', $invoiceid);

        $result = $this->db->get('invoice');
		
		return $result->row_array();
	}

	public function deleteinvoice($invoiceid)
	{
		$this->db->where('invoice_id', $invoiceid);
		$this->db->delete('invoice'); 
	}


	public function get_invoicesinfo($orderid){

		$this->db->join('orders', 'invoice.orderid = orders.orderid', 'left');
		$this->db->join('bank', 'invoice.bankid = bank.bank_id', 'left');
		$this->db->join('stocks', 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users', 'orders.userid = users.id', 'left');

		$this->db->where('invoice.orderid', $orderid);

        $result = $this->db->get('invoice');
		
		return $result->result_array();

	}
	public function get_orderinfo($orderid){

		$this->db->join('orders', 'invoice.orderid = orders.orderid', 'left');
		$this->db->join('bank', 'invoice.bankid = bank.bank_id', 'left');
		$this->db->join('stocks', 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('stocks', 'orders.stockid = stocks.stock_id', 'left');

		$this->db->join('users', 'orders.userid = users.id', 'left');

		$this->db->where('invoice.orderid', $orderid);

        $result = $this->db->get('invoice');
		
		return $result->row_array();

	}

	public function get_invoice($invoiceid){

		$this->db->join('orders', 'invoice.orderid = orders.orderid', 'left');
		$this->db->join('bank', 'invoice.bankid = bank.bank_id', 'left');
		$this->db->join('stocks', 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users', 'orders.userid = users.id', 'left');

		$this->db->where('invoice.invoice_id', $invoiceid);

        $result = $this->db->get('invoice');
		
		return $result->row_array();

	}



	public function get_activestock(){
		$this->db->where('status', 'active');

		$this->db->order_by('stocks.stock_id', 'DESC');
        $query = $this->db->get('stocks');
        return $query->result_array();
	}



	public function get_pendingstock(){
		$this->db->where('status', 'pending');

		$this->db->order_by('stocks.stock_id', 'DESC');
        $query = $this->db->get('stocks');
        return $query->result_array();
	}



	public function get_stocksignle($stockid)
	{
		$this->db->where('stock_id', $stockid);
		$result = $this->db->get('stocks');
		
		return $result->row_array();
	}
	public function get_stocksignleimg($stockid)
	{
		
		// $this->db->join('stock_pictures', 'stocks.stock_id = stock_pictures.stock_img_id', 'left');

		$this->db->where('stock_id', $stockid);
		$result = $this->db->get('stock_pictures');
		
		return $result->result_array();
	}
	




	public function get_pendingpayment(){

		$this->db->join('stocks' , 'orders.stockid = stocks.stock_id', 'left');
		$this->db->join('users' , 'orders.userid = users.id', 'left');
		
		$this->db->where('orders.status', 'panding');

		// $this->db->order_by('orders.orderid', 'DESC');
        $query = $this->db->get('orders');
        return $query->result_array();
	}

	



	public function readorder($orderid){
		$data = array(
			'status' => "ready"
        );
		
		$this->security->xss_clean($data);
		$this->db->where('orderid', $orderid);
		$this->db->update('orders', $data);

	}


	public function get_makers(){
        $query = $this->db->get('maker');
        return $query->result_array();
	}


	public function get_searchstock(){

		if(isset($_GET['maker'])){
			$this->db->like('maker', $_GET['maker']);	
		}
		
		$this->db->where('status', 'active');

        $query = $this->db->get('stocks');
        return $query->result_array();
	}

	public function get_types(){
		
        $query = $this->db->get('type');
        return $query->result_array();
	}

	public function get_model(){
		

		$this->db->join('maker', 'maker.maker_id = model.model_maker_id', 'left');

        $query = $this->db->get('model');
        return $query->result_array();
	}

	public function get_expiredstocks(){
		$this->db->where('status', 'expired');

		$this->db->order_by('stocks.stock_id', 'DESC');
        $query = $this->db->get('stocks');
        return $query->result_array();
	}


	public function get_invoices(){
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('academic_level', 'users_orders.academic_level_id = academic_level.academic_level_id', 'left');
		$this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
		$this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
		$this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

        $query = $this->db->get('users_orders');
        return $query->result_array();
	}

	public function get_countries(){
        $this->db->order_by('countries.country_name', 'ASC');
        $query = $this->db->get('countries');
        return $query->result_array();
	}
	
	public function get_ports(){
        $query = $this->db->get('ports');
        return $query->result_array();
	}

	public function get_api_countries(){ 
        // $this->db->where('status', 'active');
        // $this->db->order_by('orders.OrderID', 'DESC');
        $query = $this->db->get('api_countries');
        return $query->result_array();
	}
	public function get_country($countryid)
	{
		$this->db->where('country_id', $countryid);
		$result = $this->db->get('api_countries');
		
		return $result->row_array();
	}


	public function updatecountry($imgname01 ,$imgname02 ,$imgname03)
	{
		$data = array(
			'policy' => $this->input->post('policy'),
			'newarival' => $this->input->post('newarival'),
            'brand' => $this->input->post('brand'),
            'discount' => $this->input->post('discount'),
			'img01' => $imgname01,
			'img02' => $imgname02,
			'img03' => $imgname03
        );
		
		$this->security->xss_clean($data);
		$this->db->where('country_id', $countryid);
		$this->db->update('api_countries', $data);

	}
	
	public function get_cities($country_name){
		$this->db->where('country_name', $country_name);
        $this->db->order_by('cities.city_name', 'ASC');
        $query = $this->db->get('cities');
        return $query->result_array();
	}

	public function get_bloggers(){
        $this->db->order_by('blogger.id', 'DESC');
        $query = $this->db->get('blogger');
        return $query->result_array();
	}

	public function get_admins(){
        $this->db->order_by('admin.id', 'DESC');
        $query = $this->db->get('admin');
        return $query->result_array();
    }

	public function add_admin($enc_password)
	{
		$data = array(
            'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('admin', $data);
	}

	public function add_city()
	{
		$data = array(
            'geoname_id' => $this->input->post('geoname_id'),
			'country_iso_code' => $this->input->post('country_iso_code'),
			'country_name' => $this->input->post('country_name'),
			'time_zone' => $this->input->post('time_zone'),
			'city_name' => $this->input->post('city_name')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('cities', $data);
	}

	
	
	public function add_blogger($enc_password)
	{
		$data = array(
            'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
        );
		
		$this->security->xss_clean($data);
        $this->db->insert('blogger', $data);
	}

	public function update_admin($admin_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
            'password' => $enc_password
        );
		
		$this->security->xss_clean($data);
		$this->db->where('id', $admin_id);
		$this->db->update('admin', $data);

	}

	





	

	public function addinvoice($orderid)
	{
		$data = array(
			'orderid' => $orderid,
			'bankid' =>  $this->input->post('beneficiary_id'),
			'date' =>  $this->input->post('date'),
			'pay_amount' =>  $this->input->post('pay'),
			'remaing_amount' => $this->input->post('total_amount'),
			'invoicestatus' => 'unpaid',
			'statusimg' => 'unpaid.png',

        );
		
		$this->security->xss_clean($data);
		$this->db->insert('invoice', $data);
	}
	public function updateinvoice($invoiceid)
	{
		$data = array(
			'orderid' => $orderid,
			'bankid' =>  $this->input->post('beneficiary_id'),
			'date' =>  $this->input->post('date'),
			'pay_amount' =>  $this->input->post('pay'),
			'remaing_amount' => $this->input->post('total_amount'),
			'invoicestatus' => 'unpaid',
			'statusimg' => 'unpaid.png',

        );
		
		$this->security->xss_clean($data);
		$this->db->where('invoice_id', $invoiceid);
		$this->db->update('invoice', $data);
	}	

	public function activestatus($stockid){
		$data = array(
			'status' => "active"
        );
		
		$this->security->xss_clean($data);
		$this->db->where('stock_id', $stockid);
		$this->db->update('stocks', $data);

	}

	public function deletestock($stockid)
	{
		$this->db->where('stock_id', $stockid);
		$this->db->delete('stocks');
	}
	

	public function completestatus($stockid){
		$data = array(
			'status' => "complete"
        );
		
		$this->security->xss_clean($data);
		$this->db->where('stock_id', $stockid);
		$this->db->update('stocks', $data);

	}


	public function update_blogger($blogger_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
            'password' => $enc_password
		);
		$this->security->xss_clean($data);
		$this->db->where('id', $blogger_id);
		$this->db->update('blogger', $data);
	}
	

	public function del_admin($adminid)
	{
		$this->db->where('id', $adminid);
		$this->db->delete('admin');
	}
	public function del_bloggers($bloggerid)
	{
		$this->db->where('id', $bloggerid);
		$this->db->delete('blogger');
	}
	public function del_city($city_name)
	{
		$this->db->where('city_name', $city_name);
		$this->db->delete('cities');
	}

	public function get_jobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}

	public function get_approvejobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}
	
	public function get_pendingjobs(){
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');
		
        return $query->result_array();
	}
	
	public function get_appliedjobs(){
		$this->db->order_by('applications.appid', 'DESC');
		$this->db->join('users', 'applications.userid = users.id', 'left');
		$this->db->join('jobs', 'applications.jobid = jobs.jobid', 'left');
		$query = $this->db->get('applications');
		
        return $query->result_array();
	}

	public function get_users(){
		$this->db->order_by('users.id', 'DESC');
		$query = $this->db->get('users');
		
        return $query->result_array();

	}

	

	public function get_api_countriess(){ 
		// $this->db->where('status', 'active');
		// $this->db->order_by('orders.OrderID', 'DESC');
        $query = $this->db->get('api_countries');
        return $query->result_array();
	}

	public function updatestock($stockid){
		$data = array(
			'auction_date' => $this->input->post('auction_date'),
			'auction' => $this->input->post('auction'),
			'lot_number' => $this->input->post('lot_number'),
			'maker' => $this->input->post('maker'),
			'model' => $this->input->post('model'),
			'model_code' => $this->input->post('model_code'),
			'chassis_number' => $this->input->post('chassis_number'),
			'year' => $this->input->post('year'),
			'engine_cc' => $this->input->post('engine_cc'),
			'transmission' => $this->input->post('transmission'),
			'version_class' => $this->input->post('version_class'),
			'drive' => $this->input->post('drive'),
			'engine_code' => $this->input->post('engine_code'),
			'fuel' => $this->input->post('fuel'),
			'color' => $this->input->post('color'),
			'doors' => $this->input->post('doors'),
			'seats' => $this->input->post('seats'),
			'dimesion' => $this->input->post('dimesion'),
			'm3' => $this->input->post('m3'),
			'weight' => $this->input->post('weight'),
			'package' => $this->input->post('package'),
			'milage' => $this->input->post('milage'),
			'condition' => $this->input->post('condition'),
			'city_location' => $this->input->post('city_location'),
			'start_price' => $this->input->post('start_price'),
			'won_price' => $this->input->post('won_price'),
			'sale_price' => $this->input->post('sale_price'),
			'standard_features' => $this->input->post('standard_features'),
			'reference' => $this->input->post('reference'),
			'sale_price'  => $this->input->post('sale_price'),
			'standard_features' => $this->input->post('standard_features'),
			'km' => $this->input->post('km'),
			'height' => $this->input->post('height'),
			'km2' => $this->input->post('km2'),
			'length' => $this->input->post('length'),
			'width' => $this->input->post('width'),
			'options' => $this->input->post('options'),
			'totalweight' => $this->input->post('totalweight'),
			'enginetype' => $this->input->post('enginetype'),
			'document_accessories' => $this->input->post('document_accessories'),
			'lhd/rhd' => $this->input->post('lhd/rhd'),
			'loadingcapacity' => $this->input->post('loadingcapacity'),
			'interiorgrade' => $this->input->post('interiorgrade'),
			'exteriorgrade' => $this->input->post('exteriorgrade'),
			'note' => $this->input->post('note')
			
        );
		
		$this->security->xss_clean($data);
		$this->db->where('stock_id', $stockid);
		$this->db->update('stocks', $data);

	}

	public function updateauction($stockid){
		$data = array(
			'actionname' => $this->input->post('actionname'),
			'actiondate' => $this->input->post('actiondate'),
			'lotno' => $this->input->post('lotno'),
			'transportaition_date' => $this->input->post('transportaition_date'),
			'auction_name_house' => $this->input->post('auction_name_house'),
			'binder_name' => $this->input->post('binder_name'),
			'binderid' => $this->input->post('binderid'),
			'turn' => $this->input->post('turn'),
			'auction_fee' => $this->input->post('auction_fee'),
			'win_fee' => $this->input->post('win_fee'),
			'start_price' => $this->input->post('start_price'),
			'recycle_fee' => $this->input->post('recycle_fee'),
			'winprice' => $this->input->post('winprice'),
			'note' => $this->input->post('note')
			
            
        );
		
		$this->security->xss_clean($data);
		$this->db->where('stockid', $stockid);
		$this->db->update('auction', $data);

	}

	public function updateprice($stockid){
		$data = array(

			'soldprice' => $this->input->post('soldprice'),
			'soldterm' => $this->input->post('soldterm'),
			'currency' => $this->input->post('currency'),
			'fobprice' => $this->input->post('fobprice'), 
			'freight' => $this->input->post('freight'), 
			'shippedcountry' => $this->input->post('shippedcountry'), 
			'shipment_type' => $this->input->post('shipment_type'),
			'required_inspection' => $this->input->post('required_inspection'), 
			'service' => $this->input->post('service'), 
			'avail_dis' => $this->input->post('avail_dis'), 
			'total_price' => $this->input->post('total_price'), 
			'note' => $this->input->post('note'),
			'100%paymentfor3days' => $this->input->post('100%paymentfor3days'), 
			'50%paymentfor3days' => $this->input->post('50%paymentfor3days'), 
			'30%paymentfor3days' => $this->input->post('30%paymentfor3days'), 
			'100%paymentfor1week' => $this->input->post('100%paymentfor1week'), 
			'50%paymentfor1week' => $this->input->post('50%paymentfor1week'), 
			'30%paymentfor1week' => $this->input->post('30%paymentfor1week'), 
			'100%payment' => $this->input->post('100%payment'), 
			'50%payment' => $this->input->post('50%payment'), 
			'30%payment' => $this->input->post('30%payment'), 
            
        );
		
		$this->security->xss_clean($data);
		$this->db->where('stockid', $stockid);
		$this->db->update('price', $data);

	}

	public function updatericksu($stockid){
		
		$data = array(

			'yard_name' => $this->input->post('yard_name'),
			'loadingpoint' => $this->input->post('loadingpoint'),
			'picture' => $this->input->post('picture'),
			'remaingfreedays' => $this->input->post('remaingfreedays'),
			'ricksu_company'  => $this->input->post('ricksu_company'),
			'ricksufee'  => $this->input->post('ricksufee'),
			'yard_arrival_date'  => $this->input->post('yard_arrival_date'),
			'delevery_report'  => $this->input->post('delevery_report'),
			'receivedby'  => $this->input->post('receivedby'),
			'repairinfo'  => $this->input->post('repairinfo'),
			'yard_service'  => $this->input->post('yard_service'),
			'repair_fee'  => $this->input->post('repair_fee'),
			'aditional_service'  => $this->input->post('aditional_service'),
			'chargis_for_additional_services'  => $this->input->post('chargis_for_additional_services'),
			'notes'  => $this->input->post('notes'),
			'deliveryby'  => $this->input->post('deliveryby'),
			'yardleavingdate'  => $this->input->post('yardleavingdate'),

		);
		
		$this->security->xss_clean($data);
		$this->db->where('stockid', $stockid);
		$this->db->update('ricksu', $data);
		
	}

	public function updatepayment($stockid){
			
		$data = array(

			'payamount' => $this->input->post('payamount'),
			'date' => $this->input->post('date'),
			'inperson' => $this->input->post('inperson'),
			'receivingbankname'  => $this->input->post('receivingbankname'),
			'sender_name'  => $this->input->post('sender_name'),
			'customer_name'  => $this->input->post('customer_name'),
			'total_amount_received'  => $this->input->post('total_amount_received'),
			'sent_country'  => $this->input->post('sent_country'),
			'received_amount'  => $this->input->post('received_amount'),
			'currency'  => $this->input->post('currency'),
			'purpose'  => $this->input->post('purpose'),
			'bankcharges'  => $this->input->post('bankcharges'),
			'paymentid'  => $this->input->post('paymentid'),
			'exchange_date'  => $this->input->post('exchange_date'), 
			'exchange_rate'  => $this->input->post('exchange_rate'),
			'dateof_receiving'  => $this->input->post('dateof_receiving'),
			'notes'  => $this->input->post('notes')

		);
		
		$this->security->xss_clean($data);
		$this->db->where('stockid', $stockid);
		$this->db->update('payment', $data);
		
	}

    public function updatereservation($stockid){	
		$data = array(

			'reservedby' => $this->input->post('reservedby'),
			'soldby' => $this->input->post('soldby'),
			'reservation_expiry' => $this->input->post('reservation_expiry'),
			'date_of_sold' => $this->input->post('date_of_sold'),
			'customername'  => $this->input->post('customername'),
			'queno'  => $this->input->post('queno'),
			'reservationdate'  => $this->input->post('reservationdate'),
			'reservation_startingdate'  => $this->input->post('reservation_startingdate'),
			'soldprice'  => $this->input->post('soldprice'),
			'paymenttrem'  => $this->input->post('paymenttrem'),
			'activereservation'  => $this->input->post('activereservation'),
			'notes'  => $this->input->post('notes'),

		);
		
		$this->security->xss_clean($data);
		$this->db->where('stockid', $stockid);
		$this->db->update('reservation', $data);
		
	}

	public function updateshipment($stockid){
			
		$data = array(

			'shiping_date'=>  $this->input->post('shiping_date'),
			'nameofship'=>  $this->input->post('nameofship'),
			'shippingcompany' =>  $this->input->post('shippingcompany'),
			'shippingorderno' =>  $this->input->post('shippingorderno'),
			'blno' =>  $this->input->post('blno'),
			'shippedcountry' =>  $this->input->post('shippedcountry'),
			'portofloading' =>  $this->input->post('portofloading'),
			'portofdischarge' =>  $this->input->post('portofdischarge'),
			'voyageno' =>  $this->input->post('voyageno'),
			'shipmenttype' =>  $this->input->post('shipmenttype'),
			'containerno' =>  $this->input->post('containerno'),
			'shippingordercuttingdate' =>  $this->input->post('shippingordercuttingdate'),
			'consigneename' =>  $this->input->post('consigneename'),
			'consigneeaddress' =>  $this->input->post('consigneeaddress'),
			'consigneecontactno' =>  $this->input->post('consigneecontactno'),
			'notifypartyname' =>  $this->input->post('notifypartyname'),
			'notifypartyaddress' =>  $this->input->post('notifypartyaddress'),
			'partycontactno' =>  $this->input->post('partycontactno'),
			'finaldestination' =>  $this->input->post('finaldestination'),
			'measuredm3' =>  $this->input->post('measuredm3'),
			'shippingweight' =>  $this->input->post('shippingweight'),
			'accessorieswithcargo'=>  $this->input->post('accessorieswithcargo'), 
			'note' =>  $this->input->post('note')
			
		);
		
		$this->security->xss_clean($data);
		$this->db->where('stockid', $stockid);
		$this->db->update('shipment', $data);
		
	}

	public function updatedocument($stockid){
			
		$data = array(

			'mashou'=>  $this->input->post('mashou'),
			'mashou_status' =>  $this->input->post('mashou_status'),
			'mashou_date' =>  $this->input->post('mashou_date'),
			'shipingorder'=>  $this->input->post('shipingorder'),
			'shipingorder_status' =>  $this->input->post('shipingorder_status'),
			'shipingorder_date'=>  $this->input->post('shipingorder_date'), 
			'inspection'=>  $this->input->post('inspection'),
			'inspection_status'=>  $this->input->post('inspection_status'), 
			'inspection_date' =>  $this->input->post('inspection_date'),
			'exportcertificate'=>  $this->input->post('exportcertificate'),
			'exportcertificate_date'=> $this->input->post('exportcertificate_date'),
			'bl_arrival' =>$this->input->post('bl_arrival'),
			'bl_arrival_date' =>  $this->input->post('bl_arrival_date'),
			'translations' =>  $this->input->post('translations'),
			'translations_status' =>  $this->input->post('translations_status'),
			'translations_date' =>  $this->input->post('translations_date'),
			'parcelno' =>  $this->input->post('parcelno'),
			'servicescompany' =>  $this->input->post('servicescompany'),
			'requestby' =>  $this->input->post('requestby'),
			'requestrecevingdate' =>  $this->input->post('requestrecevingdate'),
			'dateofdipatch' =>  $this->input->post('dateofdipatch'),
			'receivername' =>  $this->input->post('receivername'),
			'receveraddress' =>  $this->input->post('receveraddress'),
			'contactno' =>  $this->input->post('contactno'),
			'parcelweight' =>  $this->input->post('parcelweight'),
			'documentsdetails' =>  $this->input->post('documentsdetails'),
			'accessories' =>  $this->input->post('accessories')

		);
		
		$this->security->xss_clean($data);
		$this->db->where('stockid', $stockid);
		$this->db->update('documents', $data);
		
	}



	/////////////////////////////consignee FOrm //////////////

	public function get_user($userid){
		$this->db->order_by('users.id', 'DESC');
		$this->db->where('id', $userid);
		$result = $this->db->get('users');
        return $result->row_array();

	}

	public function get_consigneeforms($userid){

		$this->db->join('users', 'consigneeform.userid = users.id', 'left');
		$this->db->order_by('consigneeform.consigneeformid', 'DESC');
		$this->db->where('userid', $userid);
		$result = $this->db->get('consigneeform');
        return $result->result_array();

	}
	public function get_consigneeform($consigneeformid){
		$this->db->join('users', 'consigneeform.userid = users.id', 'left');

		$this->db->where('consigneeformid', $consigneeformid);
		$result = $this->db->get('consigneeform');
        return $result->row_array();

	}


	public function	addconsigneeform($userid){
			
			$data = array( 
				
				'userid' => $userid,
				'customername' => $this->input->post('customername'),
				'consigneename' => $this->input->post('consigneename'),
				'consigneeaddress' => $this->input->post('consigneeaddress'),
				'contact' => $this->input->post('contact'),
				'partyname' => $this->input->post('partyname'),
				'partyaddress' => $this->input->post('partyaddress'),
				'finaldesport' => $this->input->post('finaldesport'),
				'note' => $this->input->post('note')

			);
	
				$this->security->xss_clean($data);
				$this->db->insert('consigneeform', $data);
		}


		public function	updateconsigneeform($consigneeformid){
			
			$data = array( 
				
				// 'userid' => $userid,
				'customername' => $this->input->post('customername'),
				'consigneename' => $this->input->post('consigneename'),
				'consigneeaddress' => $this->input->post('consigneeaddress'),
				'contact' => $this->input->post('contact'),
				'partyname' => $this->input->post('partyname'),
				'partyaddress' => $this->input->post('partyaddress'),
				'finaldesport' => $this->input->post('finaldesport'),
				'note' => $this->input->post('note')
				
			);
			$this->security->xss_clean($data);
			$this->db->where('consigneeformid', $consigneeformid);
			$this->db->update('consigneeform', $data);
		}
		
	
		public function deleteconsigneeform($consigneeformid)
		{
			$this->db->where('consigneeformid', $consigneeformid);
			$this->db->delete('consigneeform');
		}

}