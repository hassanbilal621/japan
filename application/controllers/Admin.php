<?php
class admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('excel');
    }


    public function addstock()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/addstock.php');
        $this->load->view('templates/admin/footer.php');
    }


    public function deletemodel($modelid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->del_model($modelid);

        redirect('admin/model');
    }

    public function deletemaker($makerid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->del_maker($makerid);

        redirect('admin/maker');
    }

    public function deletetype($typeid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->del_type($typeid);

        redirect('admin/type');
    }





    public function model()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }


        $this->form_validation->set_rules('makerid', 'makerid', 'required');
        $this->form_validation->set_rules('modelname', 'modelname', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['makers'] = $this->admin_model->get_makers();
            $data['models'] = $this->admin_model->get_model();



            //         echo '<pre>';
            //         print_r($data);
            //         echo '<pre>';
            // exit;
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/model.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->add_model();

            redirect('admin/model');
        }
    }



    public function maker()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }



        $this->form_validation->set_rules('makername', 'makername', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['makers'] = $this->admin_model->get_makers();
            $data['models'] = $this->admin_model->get_model();



            //         echo '<pre>';
            //         print_r($data);
            //         echo '<pre>';
            // exit;
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/maker.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->add_maker();

            redirect('admin/maker');
        }
    }

    public function type()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }



        $this->form_validation->set_rules('typename', 'typername', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['types'] = $this->admin_model->get_types();




            //         echo '<pre>';
            //         print_r($data);
            //         echo '<pre>';
            // exit;
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/type.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->add_type();

            redirect('admin/type');
        }
    }

    public function countryport()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }



        $this->form_validation->set_rules('portname', 'portname', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['ports'] = $this->admin_model->get_countryport();




            //         echo '<pre>';
            //         print_r($data);
            //         echo '<pre>';
            // exit;
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/countryport.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->add_countryport();

            redirect('admin/countryport');
        }
    }

    public function del_countryport($portid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->del_countryport($portid);

        redirect('admin/countryport');
    }

    public function pendingpayment()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['orders'] = $this->admin_model->get_pendingpayment();


        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // exit;
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/pendingpayment.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    public function readorder($orderid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $this->admin_model->readorder($orderid);
        redirect('admin/pendingpayment');
    }


    public function activeorders()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }


        $this->form_validation->set_rules('Name', 'Name', 'required');

        if ($this->form_validation->run() === FALSE) {

            $data['orders'] = $this->admin_model->get_activeorders();


            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/activeorders.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $this->user_model->addauctionorder();

            redirect('admin/login');
        }
    }



    public function deliveredorder()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['orders'] = $this->admin_model->get_delivedrorder();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/deliveredorder.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function search()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $data['makers'] = $this->admin_model->get_makers();
        $data['models'] = $this->admin_model->get_model();
        $data['types'] = $this->admin_model->get_types();

        $data['stocks'] = $this->admin_model->get_searchstock();


        $data['makers'] = $this->admin_model->get_makers();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php', $data);
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/search.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function bank()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $this->form_validation->set_rules('beneficiary_name', 'beneficiary_name', 'required');

        if ($this->form_validation->run() === FALSE) {
            //         echo '<pre>';
            //         print_r($data);
            //         echo '<pre>';
            // exit;
            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/bank.php');
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->add_bank();

            redirect('admin/login');
        }
    }



    public function cancledorder()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['orders'] = $this->admin_model->get_canclededorders();


        //         echo '<pre>';
        //         print_r($data);
        //         echo '<pre>';
        // exit;
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/cancledorder.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function viewinvoice($invoiceid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
		$data['invoice'] =$this->admin_model->get_invoice($invoiceid);


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/users/invoice.php', $data);
        $this->load->view('templates/admin/footer.php');
    }



    public function ajax_pending_stock()
    {
        $currUser = $this->user_model->get_userinfo($userid);
        $data['countries'] = $this->admin_model->get_countries();

        $data['user'] = $currUser;

        $this->load->view('templates/ajax/edituser.php', $data);
    }

    function import()
    {
        if (isset($_FILES["file"]["name"])) {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach ($object->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $auction_date = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $auction = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $lot_number = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $maker = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $model = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $model_code = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $chassis = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $year = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $engine_cc = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $transmission = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $version_class = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $drive = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $engine_code = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $fuel = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                    $color = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                    $doors = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                    $seats = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                    $dimension = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                    $m3 = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                    $weight = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                    $package = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                    $milage = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
                    $condition_grade = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
                    $city_location = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                    $start_price = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                    $won_price = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                    $sale_price = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                    $standard_features = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                    $reference = $worksheet->getCellByColumnAndRow(28, $row)->getValue();


                    $data = array(
                        'maker'            =>    $maker,
                        'model'            =>    $model,
                        'model_code'    =>    $model_code,
                        'chassis'        =>    $chassis,
                        'year'            =>    $year,
                        'engine_cc'        =>    $engine_cc,
                        'transmission'    =>    $transmission,
                        'version_class'    =>    $version_class,
                        'drive'            =>    $drive,
                        'engine_code'    =>    $engine_code,
                        'fuel'            =>    $fuel,
                        'color'            =>    $color,
                        'doors'            =>    $doors,
                        'seats'            =>    $seats,
                        'dimension'        =>    $dimension,
                        'm3'            =>    $m3,
                        'weight'        =>    $weight,
                        'package'        =>    $package,
                        'milage'        =>    $milage,
                        'condition_grade'    =>    $condition_grade,
                        'city_location'        =>    $city_location,
                        'sale_price'        =>    $sale_price,
                        'standard_features'    =>    $standard_features,
                        'reference'            =>    $reference,
                        'status'            =>    "pending"
                    );


                    $dataauction = array(
                        'actiondate'    =>    $auction_date,
                        'actionname'        =>    $auction,
                        'start_price'        =>    $start_price,
                        'lotno'    =>    $lot_number,
                        'winprice'            =>    $won_price,
                    );

                    $datapayment = array();

                    $dataprice = array();

                    $dataricksu = array();

                    $datashipment = array();
                    $datadocument = array();
                    $datareservation = array();


                    $stockid = $this->admin_model->insert($data);
                    $this->admin_model->insert_auction($stockid, $dataauction);
                    $this->admin_model->insert_payment($stockid, $datapayment);
                    $this->admin_model->insert_price($stockid, $dataprice);
                    $this->admin_model->insert_shipment($stockid, $datashipment);
                    $this->admin_model->insert_document($stockid, $datadocument);
                    $this->admin_model->insert_ricksu($stockid, $dataricksu);
                    $this->admin_model->insert_reservation($stockid, $datareservation);
                }
            }





            echo 'Data Imported successfully';
        }
    }


    public function login()
    {

        if ($this->session->admindata('japan_admin_id')) {
            redirect('admin/');
        }

        $data['title'] = 'Nexco Japan';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE) {


            $this->load->view('templates/admin/login.php', $data);
        } else {

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //$user_id = $this->user_model->login($username, $password);
            $japan_admin_id = $this->admin_model->login($username, $password);

            if ($japan_admin_id) {
                $admin_data = array(
                    'japan_admin_id' => $japan_admin_id,
                    'japan_email' => $username,
                    'submit_alogged_in' => true
                );
                $this->session->set_admindata($admin_data);

                redirect('admin/');
            } else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('admin/login');
            }
        }
    }



    public function ajax_edit_usermodal($userid)
    {
        $currUser = $this->user_model->get_userinfo($userid);
        $data['countries'] = $this->admin_model->get_countries();

        $data['user'] = $currUser;

        $this->load->view('templates/ajax/edituser.php', $data);
    }



    public function logout()
    {

        $this->session->unset_userdata('japan_admin_id');
        $this->session->unset_userdata('japan_email');
        $this->session->unset_userdata('submit_alogged_in');

        $this->session->set_flashdata('admin_loggedout', 'You are now logged out');
        redirect('admin/login');
    }





    public function index()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/index.php');
        $this->load->view('templates/admin/footer.php');
    }


    public function order()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/order.php');
        $this->load->view('templates/admin/footer.php');
    }

    public function deliverorder($orderid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $this->admin_model->deliverorder($orderid);
        redirect('admin/activeorders');
    }

    public function cancelorder($orderid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $this->admin_model->cancledorder($orderid);
        redirect('admin/cancledorder');
    }




    public function settings()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/settings.php');
        $this->load->view('templates/admin/footer.php');
    }


    public function administrator()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $data['admins'] = $this->admin_model->get_admins();
        // echo '<pre>' ;
        // print_r($data);
        // echo '<pre>' ;
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/administrator.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    public function users()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['users'] = $this->admin_model->get_users();

        // echo '<pre>' ;
        // print_r($data);
        // echo '<pre>' ;
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/users.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function consigneeform($userid)
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['user'] = $this->admin_model->get_user($userid);
        $data['consignees'] = $this->admin_model->get_consigneeforms($userid);



        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/consigneeform.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    public function addconsigneeform()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('userid', 'userid', 'required');
        $this->form_validation->set_rules('customername', 'customername', 'required');
        $this->form_validation->set_rules('consigneename', 'consigneename', 'required');


        if ($this->form_validation->run() === FALSE) {


            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/consigneeform?fail');
        } else {
            $userid = $this->input->post('userid');
            $this->admin_model->addconsigneeform($userid);



            redirect('admin/consigneeform/' . $userid);
        }
    }


    public function updateconsigneeform()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('userid', 'userid', 'required');
        $this->form_validation->set_rules('customername', 'customername', 'required');
        $this->form_validation->set_rules('consigneename', 'consigneename', 'required');


        if ($this->form_validation->run() === FALSE) {


            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/consigneeform?fail');
        } else {
            $consigneeformid = $this->input->post('consigneeformid');
            $userid = $this->input->post('userid');
            $this->admin_model->updateconsigneeform($consigneeformid);



            redirect('admin/consigneeform/' . $userid);
        }
    }

    public function ajax_consigneeform($consigneeformid)
    {

        $data['consignee'] = $this->admin_model->get_consigneeform($consigneeformid);


        $this->load->view('templates/ajax/editconsigneeform.php', $data);
    }

    public function deleteconsigneeform($consigneeformid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }


        $this->admin_model->deleteconsigneeform($consigneeformid);

        redirect('admin/user/' . $userid);
    }






    function do_upload()
    {

        $config = array(
            'upload_path' => "assets/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => false,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "5000",
            'max_width' => "5000"
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $imgdata = array('upload_data' => $this->upload->data());

            $imgname = $imgdata['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';
            print_r($error);
            echo '<pre>';
            exit;
        }

        return $imgname;
    }


    public function addinvoice($orderid)
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        if ($this->form_validation->run() === FALSE) {

            $data['bank'] = $this->admin_model->get_bank();
            $data['order'] =  $this->admin_model->get_order($orderid);


            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/invoice.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $this->admin_model->addinvoice($orderid);

            redirect('admin/pendingpayment');
        }
    }

    public function deleteunpaidinvoice($invoiceid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->deleteinvoice($invoiceid);

        redirect('admin/unpaidinvoices');
    }

    public function deletepaidinvoice($invoiceid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->deleteinvoice($invoiceid);

        redirect('admin/paidinvoices');
    }

    public function deleteinvoice($invoiceid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->deleteinvoice($invoiceid);

        redirect('admin/manageinvoice');
    }

    public function editinvoice($invoiceid)
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $data['banks'] = $this->admin_model->get_bank();
        $data['invoice'] = $this->admin_model->edit_invoice($invoiceid);
        //     

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/editinvoice.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    public function updateinvoice()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $invoiceid = $this->input->post('invoiceid');
        $this->admin_model->updateinvoice($invoiceid);

        redirect('admin/manageinvoice');
    }

    public function invoice()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $data['users'] = $this->admin_model->get_users();
        $data['bank'] = $this->admin_model->get_bank();
        $data['orders'] = $this->admin_model->get_orders();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/voucher.php', $data);
        $this->load->view('templates/admin/footer.php');
    }



    public function addinvoicedetail()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $orderid = $this->input->post('orderid');

        $this->admin_model->addinvoice($orderid);

        redirect('admin/manageinvoice');
    }


    public function ajax_get_bank_details($bankid)
    {
        $myObj =  $this->admin_model->get_ajax_bank($bankid);
        $myJSON = json_encode($myObj);
        echo $myJSON;
    }

    public function manageinvoice()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['invoices'] = $this->admin_model->get_manage_invoice();


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/manageinvoice.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    public function paidinvoices()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['invoices'] = $this->admin_model->get_paid_invoice();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/paidinvoice.php', $data);
        $this->load->view('templates/admin/footer.php');
    }




    public function paidinvoice($invoiceid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }



        $invoicedata = $this->admin_model->edit_invoice($invoiceid);

        $order =  $this->admin_model->get_order_invoice($invoicedata['orderid']);

        $invoiceamouunt = $invoicedata['pay_amount'];
        $orderamount = $order['orderpaidamount'];

        $ordertotal = $invoiceamouunt + $orderamount;

        $orderid = $order['orderid'];

        $this->admin_model->paidinvoice($invoiceid);
        $this->admin_model->update_order_paid_amount($ordertotal, $orderid);

        redirect('admin/unpaidinvoices');
    }



    public function unpaidinvoices()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['invoices'] = $this->admin_model->get_unpaid_invoice();


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/unpaidinvoice.php', $data);
        $this->load->view('templates/admin/footer.php');
    }




    public function unpaidinvoice($invoiceid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $invoicedata = $this->admin_model->edit_invoice($invoiceid);

        $order =  $this->admin_model->get_order_invoice($invoicedata['orderid']);

        $invoiceamouunt = $invoicedata['pay_amount'];
        $orderamount = $order['orderpaidamount'];

        $ordertotal = $invoiceamouunt - $orderamount;

        $orderid = $order['orderid'];

        $this->admin_model->unpaidinvoice($invoiceid);
        $this->admin_model->update_order_paid_amount($ordertotal, $orderid);


        redirect('admin/paidinvoices');
    }




    // public function viewinvoice()
    // {
    //     if (!$this->session->admindata('japan_admin_id')) {
    //         redirect('admin/login');
    //     }
    //     // echo '<pre>' ;
    //     // print_r($data);
    //     // echo '<pre>' ;
    //     // die;



    //     $this->load->view('templates/admin/header.php');
    //     $this->load->view('templates/admin/navbar.php');
    //     $this->load->view('templates/admin/aside.php');
    //     $this->load->view('templates/admin/viewinvoice.php');
    //     $this->load->view('templates/admin/footer.php');
    // }




    public function country()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }



        // $url = 'https://restcountries.eu/rest/v2/all';
        // $data['api_countries'] = json_decode(file_get_contents($url), true);
        $data['api_countries'] = $this->admin_model->get_api_countries();
        // echo '<pre>' ;
        // print_r($data);
        // echo '<pre>' ;
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/country.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function editcounty($countryid)
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['api_country'] = $this->admin_model->get_country($countryid);


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/editcounty.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function updatecountry()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $imgname01 = $this->do_upload();
        $imgname02 = $this->do_upload();
        $imgname03 = $this->do_upload();

        $this->admin_model->updatecountry($imgname01, $imgname02, $imgname03);
        redirect('admin/editcounty/' . $countryid);
    }




    public function addusers()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'FullName', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');


        if ($this->form_validation->run() === FALSE) {


            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/users?fail');
        } else {
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            //$enc_password = md5($this->input->post('password'));
            $this->user_model->register($enc_password);

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/users');
        }
    }

    //////////////////////////////  stocks


    public function delete_stock_picture($imageid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->db->where('stock_picture_id', $imageid);
        $this->db->delete('stock_pictures');

        $stockid = $_GET['stockid'];
        redirect('admin/editstocks/' . $stockid);
    }
    public function deleteorder($orderid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->deleteorder($orderid);

        redirect('admin/pendingstock');
    }

    public function pendingstock()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['stocks'] = $this->admin_model->get_pendingstock();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/pendingstock.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    public function activestocks()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['stocks'] = $this->admin_model->get_activestock();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/activestock.php', $data);
        $this->load->view('templates/admin/footer.php');
    }


    public function expiredstocks()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $data['stocks'] = $this->admin_model->get_expiredstocks();


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/expiredstock.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function setcompleted($stockid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $this->admin_model->completestatus($stockid);
        redirect('admin/activestocks');
    }

    public function deletestock($stockid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $this->admin_model->deletestock($stockid);

        redirect('admin/pendingstock');
    }

    public function activestatus($stockid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        $this->admin_model->activestatus($stockid);
        redirect('admin/activestocks');
    }

    public function stocks()
    {
        // if(!$this->session->admindata('japan_admin_id'))
        // {
        // 	redirect('admin/login');
        // }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/stocks.php');
        $this->load->view('templates/admin/footer.php');
    }

    public function add_stock_images()
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $imgname = $this->do_upload();

        $this->admin_model->add_image($stockid, $imgname);
        redirect('admin/editstocks/' . $stockid);
    }


    public function editstocks($stockid)
    {
        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }
        // echo '<pre>' ;
        // print_r($data);
        // echo '<pre>' ;
        // die;

        $data['stock'] = $this->admin_model->get_stock($stockid);
        $data['stock_pictures'] = $this->admin_model->get_stock_pictures($stockid);
        $data['auction'] = $this->admin_model->get_auction($stockid);
        $data['ricksu'] = $this->admin_model->get_ricksu($stockid);
        $data['price'] = $this->admin_model->get_price($stockid);
        $data['payment'] = $this->admin_model->get_payment($stockid);
        $data['reserved'] = $this->admin_model->get_reserved($stockid);
        $data['shipment'] = $this->admin_model->get_shipment($stockid);
        $data['document'] = $this->admin_model->get_document($stockid);




        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;
        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/editstocks.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function uploadImage()
    {



        $data = [];



        $count = count($_FILES['files']['name']);



        for ($i = 0; $i < $count; $i++) {



            if (!empty($_FILES['files']['name'][$i])) {



                $_FILES['file']['name'] = $_FILES['files']['name'][$i];

                $_FILES['file']['type'] = $_FILES['files']['type'][$i];

                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];

                $_FILES['file']['error'] = $_FILES['files']['error'][$i];

                $_FILES['file']['size'] = $_FILES['files']['size'][$i];



                $config['upload_path'] = 'assets/uploads/';

                $config['allowed_types'] = 'jpg|jpeg|png|gif';

                $config['max_size'] = '5000';

                $config['file_name'] = $_FILES['files']['name'][$i];



                $this->load->library('upload', $config);



                if ($this->upload->do_upload('file')) {

                    $uploadData = $this->upload->data();

                    $filename = $uploadData['file_name'];



                    $data['totalFiles'][] = $filename;
                }
            }
        }
        $stockid = $this->input->post('stockid');

        foreach ($data['totalFiles'] as $carimages) {

            $this->admin_model->add_image($stockid, $carimages);
        }

        redirect('admin/editstocks/' . $stockid);
    }


    public function updatestock()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $this->admin_model->updatestock($stockid);
        redirect('admin/editstocks/' . $stockid);
    }


    public function updateauction()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $this->admin_model->updateauction($stockid);
        redirect('admin/editstocks/' . $stockid);
    }

    public function updateprice()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $this->admin_model->updateprice($stockid);
        redirect('admin/editstocks/' . $stockid);
    }

    public function updatericksu()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $this->admin_model->updatericksu($stockid);
        redirect('admin/editstocks/' . $stockid);
    }

    public function updatepayment()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $this->admin_model->updatepayment($stockid);
        redirect('admin/editstocks/' . $stockid);
    }

    public function updatereservation()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $this->admin_model->updatereservation($stockid);
        redirect('admin/editstocks/' . $stockid);
    }

    public function updateshipment()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $this->admin_model->updateshipment($stockid);
        redirect('admin/editstocks/' . $stockid);
    }

    public function updatedocument()
    {

        if (!$this->session->admindata('japan_admin_id')) {
            redirect('admin/login');
        }

        $stockid = $this->input->post('stockid');

        $this->admin_model->updatedocument($stockid);
        redirect('admin/editstocks/' . $stockid);
    }

    /////////////////// ajax/////////////////


    public function ajax_stock($stockid)
    {

        $data['stock'] = $this->admin_model->get_stock($stockid);


        $this->load->view('templates/ajax/editstock.php', $data);
    }

    public function ajax_auction($stockid)
    {

        $data['auction'] = $this->admin_model->get_auction($stockid);


        $this->load->view('templates/ajax/editauction.php', $data);
    }

    public function ajax_ricksu($stockid)
    {

        $data['ricksu'] = $this->admin_model->get_ricksu($stockid);


        $this->load->view('templates/ajax/editriksu.php', $data);
    }

    public function ajax_price($stockid)
    {
        $data['price'] = $this->admin_model->get_price($stockid);



        $this->load->view('templates/ajax/editprice.php', $data);
    }

    public function ajax_payment($stockid)
    {

        $data['payment'] = $this->admin_model->get_payment($stockid);


        $this->load->view('templates/ajax/editpayment.php', $data);
    }

    public function ajax_reserved($stockid)
    {


        $data['reservation'] = $this->admin_model->get_reserved($stockid);

        $this->load->view('templates/ajax/editeserved.php', $data);
    }

    public function ajax_shipment($stockid)
    {



        $data['shipment'] = $this->admin_model->get_shipment($stockid);
        $this->load->view('templates/ajax/editshipmemt.php', $data);
    }

    public function ajax_document($stockid)
    {

        $data['document'] = $this->admin_model->get_document($stockid);


        $this->load->view('templates/ajax/editdocument.php', $data);
    }
}
