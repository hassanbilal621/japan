<?php
class Users extends CI_Controller{

	public function __construct()
    {
      parent::__construct();
	  $this->load->model('user_model');
	  $this->load->model('admin_model');
    }



	function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
		$output = NULL;
		if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
			$ip = $_SERVER["REMOTE_ADDR"];
			if ($deep_detect) {
				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		}
		$purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
		$support    = array("country", "countrycode", "state", "region", "city", "location", "address");
		$continents = array(
			"AF" => "Africa",
			"AN" => "Antarctica",
			"AS" => "Asia",
			"EU" => "Europe",
			"OC" => "Australia (Oceania)",
			"NA" => "North America",
			"SA" => "South America"
		);
		if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
			if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
				switch ($purpose) {
					case "location":
						$output = array(
							"city"           => @$ipdat->geoplugin_city,
							"state"          => @$ipdat->geoplugin_regionName,
							"country"        => @$ipdat->geoplugin_countryName,
							"country_code"   => @$ipdat->geoplugin_countryCode,
							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							"continent_code" => @$ipdat->geoplugin_continentCode
						);
						break;
					case "address":
						$address = array($ipdat->geoplugin_countryName);
						if (@strlen($ipdat->geoplugin_regionName) >= 1)
							$address[] = $ipdat->geoplugin_regionName;
						if (@strlen($ipdat->geoplugin_city) >= 1)
							$address[] = $ipdat->geoplugin_city;
						$output = implode(", ", array_reverse($address));
						break;
					case "city":
						$output = @$ipdat->geoplugin_city;
						break;
					case "state":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "region":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "country":
						$output = @$ipdat->geoplugin_countryName;
						break;
					case "countrycode":
						$output = @$ipdat->geoplugin_countryCode;
						break;
				}
			}
		}
		return $output;
	}


// ----- ------------------- Details to fetch country data ---------------------
	// echo ip_info("Visitor", "Country"); // India
	// echo ip_info("Visitor", "Country Code"); // IN
	// echo ip_info("Visitor", "State"); // Andhra Pradesh
	// echo ip_info("Visitor", "City"); // Proddatur
	// echo ip_info("Visitor", "Address"); // Proddatur, Andhra Pradesh, India
	
	// print_r(ip_info("Visitor", "Location")); // Array ( [city] => Proddatur [state] => Andhra Pradesh [country] => India [country_code] => IN [continent] => Asia [continent_code] => AS )
	
      
	public function login(){
		if($this->session->userdata('necxo_user_id')){
            redirect('users/');
		}
		
		// $url = 'https://restcountries.eu/rest/v2/all';
		// $data['api_countries'] = json_decode(file_get_contents($url), true);
		$data['api_countries'] = $this->admin_model->get_api_countries();
		$data['makers'] = $this->admin_model->get_makers();
		$data['types'] = $this->admin_model->get_types();
        $data['title'] = 'Nexco Japan = Login';
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE){
			if($this->input->post('val')){
				$this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
			}

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/navbar.php', $data);
            $this->load->view('templates/users/login.php', $data);
            $this->load->view('templates/users/footer.php');

        } 
        else {
            $username = $this->input->post('username');
			$password = $this->input->post('password');

			$user_id = $this->user_model->login($username, $password);
			//$employee_id = $this->employee_model->login($username, $password);

			if($user_id){
                $user_data = array(
                    'necxo_user_id' => $user_id,
                    'username' => $username,
                    'necxo_logged_in' => true
                );
                $this->session->set_userdata($user_data);
				
				
				//set cookie for 1 year
				$cookie = array(
					'name'   => 'necxo_user_id',
					'value'  => $user_id,
					'expire' => time()+31556926
				);
				$this->input->set_cookie($cookie);

				
                $this->session->set_flashdata('user_loggedin', 'You are now logged in');
                redirect('users/');
			} 
	
            else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('users/login');
            }		
        }
	}




    public function logout(){


        $this->session->unset_userdata('necxo_user_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('necxo_logged_in');
		
		delete_cookie('necxo_user_id');

        $this->session->set_flashdata('user_loggedout', 'You are now logged out');
        redirect('users/login');
	}
	
	public function profile()
	{
		$url = 'https://restcountries.eu/rest/v2/all';
		$data['api_countries'] = json_decode(file_get_contents($url), true);
		
		if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('necxo_user_id');

		$data['user'] = $this->user_model->get_userinfo($userid);

		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;
		$data['title'] = "User Account";
		

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/profile.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function updateprofile(){

		if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('necxo_user_id');


		$this->form_validation->set_rules('name', 'FullName', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');

		if($this->form_validation->run() === FALSE){

			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');
			redirect('users/profile');
	
        }
        else{

			$this->user_model->update($userid);

            redirect('users/profile');
            
        }


	}
  

	public function forgot(){
		if($this->session->userdata('logged_in')){
            redirect('users/');
		}
		$data['title'] = 'Sign Up';
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/forgetpassword.php', $data);
		$this->load->view('templates/users/footer.php');
   
  
	}


	public function register(){
		if($this->session->userdata('logged_in')){
            redirect('users/');
		}
		$data['api_countries'] = $this->admin_model->get_api_countries();
		$data['makers'] = $this->admin_model->get_makers();
		$data['types'] = $this->admin_model->get_types();
        $data['title'] = 'Sign Up';
		$this->form_validation->set_rules('name', 'FullName', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		// $data['makers'] = $this->admin_model->get_makers();


        if($this->form_validation->run() === FALSE){

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/navbar.php');
			$this->load->view('templates/users/regist.php', $data);
			$this->load->view('templates/users/footer.php');
        }
        else{
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            //$enc_password = md5($this->input->post('password'));
            $this->user_model->register($enc_password);
			
			$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

			
            redirect('users/login');
            
        }
  
	}
	
	function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
    
	
	public function index()
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		// $ip = $_SERVER['REMOTE_ADDR'];
		// echo $this->ip_info($ip, "Country");

		// die;



		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);
		// $url = 'https://restcountries.eu/rest/v2/all';
		// $data['api_countries'] = json_decode(file_get_contents($url), true);



		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		$data['types'] = $this->admin_model->get_types();
		$data['makers'] = $this->admin_model->get_makers();
		$data['api_countries'] = $this->admin_model->get_api_countries();
		$data['stocks'] = $this->user_model->get_full_stock();

		$data['stock_pictures'] = $this->admin_model->get_stock_pictures_all();


		
		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/index.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function search()
	{
		if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/search.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function searchlist()
	{
		// $url = 'https://restcountries.eu/rest/v2/all';
		// $data['api_countries'] = json_decode(file_get_contents($url), true);

		// foreach($data['api_countries'] as $country){
		// 	echo $country['name'];

		
		// 	$data = array(
		// 		'country_name' => $country['name'],
		// 		'country_flag' => $country['flag'],
		// 		'country_capital' => $country['capital'],
		// 		'country_region' => $country['region'],
		// 		'country_nativename' => $country['nativeName']
		// 	);

		// 	$this->security->xss_clean($data);
		// 	$this->db->insert('api_countries', $data);
		// }
		// die;
		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

		$data['api_countries'] = $this->admin_model->get_api_countries();
		if(isset($_GET['country'])){
			// $url = 'https://restcountries.eu/rest/v2/name/'.$_GET['country'];
			// $country = json_decode(file_get_contents($url), true);
			// $data['api_country'] = $country;

			// echo '<pre>';
			// print_r($data);
			// echo '<pre>';
			// die;

		}
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }

		if($this->session->userdata('necxo_user_id'))
		{	
			$userid= $this->session->userdata('necxo_user_id');
			$currUser = $this->user_model->get_userinfo($userid);
			$data['user'] = $currUser;	
			// echo "<script> alert('asdsada'); </script>";
		}
        $data['makers'] = $this->admin_model->get_makers();
		$data['models'] = $this->admin_model->get_model();
		$data['types'] = $this->admin_model->get_types();

		$data['stocks'] = $this->admin_model->get_searchstock();

		$data['title'] = "Nexco Japan CO LTD";

		$data['makers'] = $this->admin_model->get_makers();

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/searchlist.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function cardetail()
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/cardetail.php', $data);
        $this->load->view('templates/users/footer.php');
	}
	

	public function changepassword()
	{	
		$url = 'https://restcountries.eu/rest/v2/all';
		$data['makers'] = $this->admin_model->get_makers();
		$data['api_countries'] = json_decode(file_get_contents($url), true);

		if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/changepassword.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function howtobuy()
	{
		$url = 'https://restcountries.eu/rest/v2/all';
		$data['makers'] = $this->admin_model->get_makers();
		$data['api_countries'] = json_decode(file_get_contents($url), true);
		$data['makers'] = $this->admin_model->get_makers();
		$data['types'] = $this->admin_model->get_types();
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";
	
		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/howtobuy.php', $data);
        $this->load->view('templates/users/footer.php');
	}
	public function testi()
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/testi.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function aboutus()
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/aboutus.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function contact()
	{
		// $url = 'https://restcountries.eu/rest/v2/all';
		// $data['api_countries'] = json_decode(file_get_contents($url), true);
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/contact.php', $data);
        $this->load->view('templates/users/footer.php');
	}
	
	

    public function ajax_get_stock(){

		if(!isset($_GET['noofstock'])){
			$noofstock = 20;
		}else{
			$noofstock = $_GET['noofstock'];
		}

		$data['stocks'] = $this->user_model->ajax_get_stock($noofstock);
		$data['stock_pictures'] = $this->user_model->ajax_get_stock_stock_pictures();

		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

		$this->load->view('templates/ajax/ajax_get_stock.php', $data);
	}

	
	public function auction()
	{
		$url = 'https://restcountries.eu/rest/v2/all';
		$data['api_countries'] = json_decode(file_get_contents($url), true);
		if(!$this->session->userdata('necxo_user_id'))
		{
			$userid= $this->session->userdata('necxo_user_id');
			$currUser = $this->user_model->get_userinfo($userid);
		}
	
		// $data['auctions'] = $this->user_model->get_auctions();

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/auction.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function auctionview($stockid)
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		
		if($this->session->userdata('necxo_user_id'))
		{
			$userid= $this->session->userdata('necxo_user_id');
			$currUser = $this->user_model->get_userinfo($userid);
			$data['user'] = $currUser;
		}

		$data['title'] = "User Account";
		$data['stock'] = $this->admin_model->get_stocksignle($stockid);
		$data['stock_pictures'] = $this->admin_model->get_stock_pictures($stockid);

		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;
		$data['makers']= $this->admin_model->get_makers();
		$data['models']= $this->admin_model->get_model();
		$data['countries']= $this->admin_model->get_countries();
		$data['ports']= $this->admin_model->get_ports();


		
	

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/auctionview.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function auctionorder()
	{
		if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}
		
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);
		$data['user'] = $currUser;

		$this->form_validation->set_rules('Name', 'Name', 'required');
		
		

		if($this->form_validation->run() === FALSE){

		// 	$this->session->set_flashdata('user_registered', 'You are sucessfully registered');
		// 	redirect('users/orders');
	
        // }
        // else{

			
			$this->user_model->addauctionorder($userid);

            redirect('users/orders');
            
        }


		
		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

	
	}
	


	public function uploadpicture(){

		if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}

		$userid= $this->session->userdata('necxo_user_id');
		$imgname = $this->do_upload();
		$this->user_model->updateprofile($userid ,$imgname);
		redirect('users/profile');
	}

	function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }


	public function service()
	{
		// $url = 'https://restcountries.eu/rest/v2/all';
		// $data['api_countries'] = json_decode(file_get_contents($url), true);
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/service.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function orders()
	{
		// $url = 'https://restcountries.eu/rest/v2/all';
		// $data['api_countries'] = json_decode(file_get_contents($url), true);
		if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}

		// if($this->session->userdata('necxo_user_id'))
		// {
		
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);
		$data['orders'] = $this->admin_model->get_orders($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/orders.php', $data);
        $this->load->view('templates/users/footer.php');
	}

	public function viewinvoice($orderid){
	if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}
		$data['orderinfo'] = $this->admin_model->get_order($orderid);
		$data['invoices'] = $this->admin_model->get_invoicesinfo($orderid);

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php');
        $this->load->view('templates/users/viewinvoices.php', $data);
        $this->load->view('templates/users/footer.php');
    }

	public function viewdetail()
	{
		// $url = 'https://restcountries.eu/rest/v2/all';
		// $data['api_countries'] = json_decode(file_get_contents($url), true);
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/viewdetail.php', $data);
        $this->load->view('templates/users/footer.php');
	}



    public function invoice($invoiceid)
	{
		if(!$this->session->userdata('necxo_user_id'))
		{
			redirect('users/login');
		}
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$data['invoice'] =$this->admin_model->get_invoice($invoiceid);

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/invoice.php', $data);
        $this->load->view('templates/users/footer.php');
    }

    public function forgetpassword()
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/forgot1.php', $data);
        $this->load->view('templates/users/footer.php');
    }
    
    public function productdetails()
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/productdetails.php', $data);
        $this->load->view('templates/users/footer.php');
	}

    public function usercart()
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/usercart.php', $data);
        $this->load->view('templates/users/footer.php');
	}

    public function userpage()
	{
		// if(!$this->session->userdata('necxo_user_id'))
		// {
		// 	redirect('users/login');
		// }
		$userid= $this->session->userdata('necxo_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/userpage.php', $data);
        $this->load->view('templates/users/footer.php');
	}












	

	
}
