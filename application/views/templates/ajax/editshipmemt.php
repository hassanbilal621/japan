<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="card-content">
            <h4 class="card-title">Shipment Form</h4>
            <?php echo form_open('admin/updateshipment') ?>
            <!-- Form with placeholder -->
            <div class="row">
               <div class="input-field col s4">
                  <h6>Shippingcompany</h6>
                  <input name="shippingcompany" type="text" value="<?php echo $shipment['shippingcompany']; ?>">
                  <input type="hidden" name="stockid" value="<?php echo $shipment['stockid']; ?>" >
               </div>
               <div class="input-field col s4">
                  <h6>Shippingorderno</h6>
                  <input name="shippingorderno" type="text" value="<?php echo $shipment['shippingorderno']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Blno</h6>
                  <input name="blno" type="text" value="<?php echo $shipment['blno']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Shippedcountry</h6>
                  <input name="shippedcountry" type="text" value="<?php echo $shipment['shippedcountry']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Portofloading</h6>
                  <input name="portofloading" type="text" value="<?php echo $shipment['portofloading']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Portofdischarge</h6>
                  <input name="portofdischarge" type="text" value="<?php echo $shipment['portofdischarge']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Name of Ship</h6>
                  <input name="nameofship" type="text" value="<?php echo $shipment['nameofship']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Voyageno</h6>
                  <input name="voyageno" type="text" value="<?php echo $shipment['voyageno']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Shipment Date</h6>
                  <input name="shiping_date" type="text" value="<?php echo $shipment['shiping_date']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Shipment Type</h6>
                  <input name="shipmenttype" type="text" value="<?php echo $shipment['shipmenttype']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Container No</h6>
                  <input name="containerno" type="text" value="<?php echo $shipment['containerno']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Shipping Order Cutting Date</h6>
                  <input name="shippingordercuttingdate" type="text" value="<?php echo $shipment['shippingordercuttingdate']; ?>">
               </div>
            </div><div class="row">
               <div class="input-field col s4">
                  <h6>Consignee Name</h6>
                  <input name="consigneename" type="text" value="<?php echo $shipment['consigneename']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Consignee Address</h6>
                  <input name="consigneeaddress" type="text" value="<?php echo $shipment['consigneeaddress']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Consignee Contact No</h6>
                  <input name="consigneecontactno" type="text" value="<?php echo $shipment['consigneecontactno']; ?>">
               </div>
            </div><div class="row">
               <div class="input-field col s4">
                  <h6>Notify Party Name</h6>
                  <input name="notifypartyname" type="text" value="<?php echo $shipment['notifypartyname']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Notify Party Address</h6>
                  <input name="notifypartyaddress" type="text" value="<?php echo $shipment['notifypartyaddress']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Party Contact No</h6>
                  <input name="partycontactno" type="text" value="<?php echo $shipment['partycontactno']; ?>">
               </div>
            </div><div class="row">
               <div class="input-field col s4">
                  <h6>Final Destination</h6>
                  <input name="finaldestination" type="text" value="<?php echo $shipment['finaldestination']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Measuredm3</h6>
                  <input name="measuredm3" type="text" value="<?php echo $shipment['measuredm3']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Shipping Weight</h6>
                  <input name="shippingweight" type="text" value="<?php echo $shipment['shippingweight']; ?>">
               </div>
            </div>
            <div class="row">
            <div class="input-field col s4">
                  <h6>Accessorie Swith Cargo</h6>
                  <input name="accessorieswithcargo" type="text" value="<?php echo $shipment['accessorieswithcargo']; ?>">
               </div>
               <div class="input-field col s8">
                  <h6>Note</h6>
                  <input name="note_shipment" type="text" value="<?php echo $shipment['note_shipment']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="waves-effect waves-light btn button blue z-depth-2 right" type="submit" name="action">Submit
                     <i class="material-icons right">send</i>
                  </button>
               </div>
            </div>
         </div>
         <?php echo form_open() ?>
      </div>
   </div>
</div>
