<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="card-content">
            <h4 class="card-title">Reservation Form</h4>
            <?php echo form_open('admin/updatereservation') ?>
            <!-- Form with placeholder -->
            <div class="row">
               <div class="input-field col s4">
                  <h6>Reserved By</h6>
                  <input type="hidden" name="stockid" value="<?php echo $reservation['stockid']; ?>" >
                  <input name="reservedby" type="text" value="<?php echo $reservation['reservedby']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Customer Name</h6>
                  <input name="receivedaccount" type="text" value="<?php echo $reservation['customername']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Queno</h6>
                  <input name="queno" type="text" value="<?php echo $reservation['queno']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Reservation Date</h6>
                  <input name="reservationdate" type="text" value="<?php echo $reservation['reservationdate']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Reservation Starting Date</h6>
                  <input name="reservation_startingdate" type="text" value="<?php echo $reservation['reservation_startingdate']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Reservation Expiry Date</h6>
                  <input name="reservation_expiry" type="text" value="<?php echo $reservation['reservation_expiry']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Sold Price</h6>
                  <input name="soldprice" type="text" value="<?php echo $reservation['soldprice']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Payment Trem</h6>
                  <input name="paymenttrem" type="text" value="<?php echo $reservation['paymenttrem']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Active Reservation</h6>
                  <input name="activereservation" type="text" value="<?php echo $reservation['activereservation']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <h6>Note</h6>
                  <input name="note" type="text" value="<?php echo $reservation['note']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="waves-effect waves-light btn button blue z-depth-2 right" type="submit" name="action">Save
                     <i class="material-icons right">save</i>
                  </button>
               </div>
            </div>
         </div>
         <?php echo form_open() ?>
      </div>
   </div>
</div>
