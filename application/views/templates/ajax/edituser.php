<div class="row">
         <div class="col s12">
            <div class="card">
               <div class="col s12">
                  <?php echo form_open('admin/addusers') ?>
                  <!-- Form with placeholder -->
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Name*" name="name" value="<?php echo $user['name']; ?>" type="text">
               
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Email*" name="email" value="<?php echo $user['email']; ?>" type="email">
               
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Password*" name="password" value="" type="password">
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Address*" name="address" value="<?php echo $user['address']; ?>" type="text">
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <input placeholder="Phone*" name="phone" value="<?php echo $user['phone']; ?>"  type="text">
                    
                     </div>
                  </div>
                  <div class="input-field col s12">
                     <div class="row">
                        <label for="phone">Select Country *</label>
                        <div class="selected-box auto-hight">
                        <select class="form-control browser-default" name="country" required>

                           <option disabled selected>Select Country</option>
                           <?php foreach ($countries as $country): ?>

                           <?php if (empty($country['country_name'])) { }
                           else{        
                           ?>
                              <option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                           <?php }?>


                           <?php endforeach; ?>
                        </select>
                        </div>
                     </div>
                  </div>
                  <div class="input-field col s12">
                     <div class="row">
                        <div class="selected-box">
                           <label for="username2">Gender</label>
                           <select class="browser-default" name="gender" value="<?php echo $user['gender']; ?>"  required>
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                        </button>
                     </div>
                  </div>
               </div>
               <?php echo form_open() ?>
            </div>
         </div>
      </div>