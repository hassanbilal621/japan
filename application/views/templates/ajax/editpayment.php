<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="card-content">
            <h4 class="card-title">Payment Form</h4>
            <?php echo form_open('admin/updatepayment') ?>
            <!-- Form with placeholder -->
            <div class="row">
               <div class="input-field col s4">
                  <h6>Receiving Bank Name</h6>
                  <input type="hidden" name="stockid" value="<?php echo $payment['stockid']; ?>" >
                  <input name="receivingbankname" type="text" value="<?php echo $payment['receivingbankname']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Received Account</h6>
                  <input name="received_amount" type="text" value="<?php echo $payment['received_amount']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Payment Id</h6>
                  <input name="paymentid" type="text" value="<?php echo $payment['paymentid']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Sender Name</h6>
                  <input name="sender_name" type="text" value="<?php echo $payment['sender_name']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>currency</h6>
                  <input name="currency" type="text" value="<?php echo $payment['currency']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Exchange Rate</h6>
                  <input name="exchange_rate" type="text" value="<?php echo $payment['exchange_rate']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Customer Name</h6>
                  <input name="customer_name" type="text" value="<?php echo $payment['customer_name']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Purpose</h6>
                  <input name="purpose" type="text" value="<?php echo $payment['purpose']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Date of Receiving</h6>
                  <input name="repairinfo" type="text" value="<?php echo $payment['dateof_receiving']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Total Amount Received</h6>
                  <input name="total_amount_received" type="text" value="<?php echo $payment['total_amount_received']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Bank Charges</h6>
                  <input  name="bankcharges" type="text" value="<?php echo $payment['bankcharges']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Sent Country</h6>
                  <input  name="sent_country" type="text" value="<?php echo $payment['sent_country']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <h6>Note</h6>
                  <input name="note" type="text" value="<?php echo $payment['note']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="waves-effect waves-light btn button red z-depth-2 right" type="submit" name="action">Save
                     <i class="material-icons right">save</i>
                  </button>
               </div>
            </div>
         </div>
         <?php echo form_open() ?>
      </div>
   </div>
</div>
