<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="card-content">
            <h4 class="card-title">Consignee Form</h4>
            <?php echo form_open('admin/updateauction') ?>
            <!-- Form with placeholder -->
            <div class="row">
               <div class="input-field col s6">
                  <h6>Auction House Name</h6>
                  <input name="auction_name_house" type="text" value="<?php echo $auction['auction_name_house']; ?>">
                  <input name="stockid" type="text" value="<?php echo $auction['stockid']; ?>">

               </div>
               <div class="input-field col s6">
                  <h6>Binder Name</h6>
                  <input name="binder_name" type="text" value="<?php echo $auction['binder_name']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Lot NO</h6>
                  <input name="lotno" type="text" value="<?php echo $auction['lotno']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Bidder Id</h6>
                  <input name="binderid" type="text" value="<?php echo $auction['binderid']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Auction Date</h6>
                  <input name="actiondate" type="text" value="<?php echo $auction['actiondate']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Turn</h6>
                  <input name="turn" type="text" value="<?php echo $auction['turn']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Auction Fee</h6>
                  <input name="auction_fee" type="text" value="<?php echo $auction['auction_fee']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Win by</h6>
                  <input name="win_fee" type="text" value="<?php echo $auction['win_fee']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Start Price</h6>
                  <input name="start_price" type="text" value="<?php echo $auction['start_price']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Recycle Fee</h6>
                  <input  name="recycle_fee" type="text" value="<?php echo $auction['recycle_fee']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Win PRice</h6>
                  <input  name="winprice" type="text" value="<?php echo $auction['winprice']; ?>">
               </div>
            </div>

            <div class="row">
               <div class="input-field col s4">
                  <input name="transportaition_date" type="text" value="<?php echo $auction['transportaition_date']; ?>">
                  <h6>Transportation Due Date </h6>
               </div>
               <div class="input-field col s8">
                  <input  name="note" type="text" value="<?php echo $auction['note']; ?>">
                  <h6>Note</h6>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="waves-effect waves-light btn button blue z-depth-2 right" type="submit" name="action">Save
                     <i class="material-icons right">save</i>
                  </button>
               </div>
            </div>
         </div>
         <?php echo form_open() ?>
      </div>
   </div>
</div>
