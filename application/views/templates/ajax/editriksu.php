<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="card-content">
            <h4 class="card-title">Ricksu Form</h4>
            <?php echo form_open('admin/updatericksu') ?>
            <!-- Form with placeholder -->
            <div class="row">
               <div class="input-field col s4">
                  <h6>Ricksu company</h6>
                  <input name="ricksu_company" type="text" value="<?php echo $ricksu['ricksu_company']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Ricksu Fee</h6>
                  <input name="ricksu_fee" type="text" value="<?php echo $ricksu['ricksufee']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Yard Arrival Date</h6>
                  <input name="yard_arrival_date" type="text" value="<?php echo $ricksu['yard_arrival_date']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Loading Point</h6>
                  <input type="hidden" name="stockid" value="<?php echo $ricksu['stockid']; ?>" >
                  <input name="loadingpoint" type="text" value="<?php echo $ricksu['loadingpoint']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Yard Name</h6>
                  <input name="yard_name" type="text" value="<?php echo $ricksu['yard_name']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Remaing Free Days</h6>
                  <input name="remaingfreedays" type="text" value="<?php echo $ricksu['remaingfreedays']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Delevery Report</h6>
                  <input name="delevery_report" type="text" value="<?php echo $ricksu['delevery_report']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Received By</h6>
                  <input name="receivedby" type="text" value="<?php echo $ricksu['receivedby']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Repair Info</h6>
                  <input name="repairinfo" type="text" value="<?php echo $ricksu['repairinfo']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Yard Service</h6>
                  <input name="yard_service" type="text" value="<?php echo $ricksu['yard_service']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Repair Fee</h6>
                  <input  name="repair_fee" type="text" value="<?php echo $ricksu['repair_fee']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Aditional Service</h6>
                  <input  name="aditional_service" type="text" value="<?php echo $ricksu['aditional_service']; ?>">
               </div>
            </div>

            <div class="row">
               <div class="input-field col s4">
                  <h6>Charges Aditional Service</h6>
                  <input name="chargis_for_additional_services" type="text" value="<?php echo $ricksu['chargis_for_additional_services']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Yard Leaving Date</h6>
                  <input  name="yardleavingdate" type="date" value="<?php echo $ricksu['yardleavingdate']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Delivery By</h6>
                  <input name="deliveryby" type="text" value="<?php echo $ricksu['deliveryby']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s6">
                  <h6>Note </h6>
                  <input name="note" type="text" value="<?php echo $ricksu['note']; ?>">
               </div>
               <div class="input-field col s6">
               <h6>Picture</h6>
                  <input  name="picture" type="text" value="<?php echo $ricksu['picture']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="waves-effect waves-light btn button red z-depth-2 right" type="submit" name="action">Save
                     <i class="material-icons right">ave</i>
                  </button>
               </div>
            </div>
         </div>
         <?php echo form_open() ?>
      </div>
   </div>
</div>
