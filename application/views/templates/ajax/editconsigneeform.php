<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <h4 class="card-title">Consignee Form</h4>
                <?php echo form_open('admin/updateconsigneeform') ?>
                <!-- Form with placeholder -->
                <div class="row">
                    <div class="input-field col s2">
                        <h6 for="name2">User ID</h6>
                        <input name="userid" type="text" value="<?php echo $consignee['id']; ?>" readonly>
                        <input type="hidden" name="consigneeformid" value="<?php echo $consignee['consigneeformid']; ?>">
                    </div>
                    <div class="input-field col s4">
                        <h6 for="name2">User Name</h6>
                        <input type="text" value="<?php echo $consignee['name']; ?>" readonly>
                    </div>
                    <div class="input-field col s4">
                        <h6 for="email">Email</h6>
                        <input type="email" value="<?php echo $consignee['email']; ?>" readonly>
                    </div>
                    <div class="input-field col s2">
                        <h6 for="name2">Date</h6>
                        <input type="text" value="<?php echo $consignee['name']; ?>" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <h6 for="name2">Customer Name</h6>
                        <input name="customername" type="text" value="<?php echo $consignee['customername']; ?>">
                    </div>
                    <div class="input-field col s4">
                        <h6 for="name2">Consignee Name</h6>
                        <input name="consigneename" type="text" value="<?php echo $consignee['consigneename']; ?>">
                    </div>
                    <div class="input-field col s4">
                        <h6 for="name2">Consignee Address</h6>
                        <input name="consigneeaddress" type="text" value="<?php echo $consignee['consigneeaddress']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <h6 for="email">Contact No</h6>
                        <input name="contact" type="number" value="<?php echo $consignee['contact']; ?>">
                    </div>
                    <div class="input-field col s4">
                        <h6 for="email">Notify Party Name</h6>
                        <input name="partyname" type="text" value="<?php echo $consignee['partyname']; ?>">
                    </div>
                    <div class="input-field col s4">
                        <h6 for="email">Notify Party Address</h6>
                        <input name="partyaddress" type="text" value="<?php echo $consignee['partyaddress']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <h6 for="name2">Final Destination at Rec. Port</h6>
                        <input name="finaldesport" type="text" value="<?php echo $consignee['finaldesport']; ?>">
                    </div>
                    <div class="input-field col s6">
                        <h6 for="address">Notes</h6>
                        <input name="note" type="text" value="<?php echo $consignee['note']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button class="waves-effect waves-light btn button blue z-depth-2 right" type="submit" name="action">Save
                            <i class="material-icons right">save</i>
                        </button>
                    </div>
                </div>
                <?php echo form_open() ?>
            </div>
        </div>
    </div>
</div>