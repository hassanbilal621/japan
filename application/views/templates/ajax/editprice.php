<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="card-content">
            <h4 class="card-title">price Form</h4>
            <?php echo form_open('admin/updateprice') ?>
            <!-- Form with placeholder -->
            <div class="row">
               <div class="input-field col s4">
                  <h6>Fob Price </h6>
                  <input type="hidden" name="stockid" value="<?php echo $price['stockid']; ?>" >
                  <input name="fobprice" type="text" value="<?php echo $price['fobprice']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Currency </h6>
                  <input name="currency" type="text" value="<?php echo $price['currency']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Freight</h6>
                  <input name="freight" type="text" value="<?php echo $price['freight']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Shipped Country </h6>
                  <input name="shippedcountry" type="text" value="<?php echo $price['shippedcountry']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Shipment Type</h6>
                  <input name="shipment_type" type="text" value="<?php echo $price['shipment_type']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Required Inspection</h6>
                  <input name="required_inspection" type="text" value="<?php echo $price['required_inspection']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Service</h6>
                  <input name="service" type="text" value="<?php echo $price['service']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Avail Dis</h6>
                  <input name="avail_dis" type="text" value="<?php echo $price['avail_dis']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Total Price</h6>
                  <input name="total_price" type="text" value="<?php echo $price['total_price']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <h6>Note</h6>
                  <input name="note" type="text" value="<?php echo $price['note']; ?>">
               </div>
            </div>
            <!-- DISCOUT -->
            <div class="row">
               <div class="input-field col s4">
                  <h6>100% Payment For 3days</h6>
                  <input name="100%paymentfor3days" type="text" value="<?php echo $price['100%paymentfor3days']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>50% Payment For 3days</h6>
                  <input name="50%paymentfor3days" type="text" value="<?php echo $price['50%paymentfor3days']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>30% Payment For 3days</h6>
                  <input name="30%paymentfor3days" type="text" value="<?php echo $price['30%paymentfor3days']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>100% Payment For 1 Week</h6>
                  <input name="100%paymentfor1week" type="text" value="<?php echo $price['100%paymentfor1week']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>50% Payment For 1 Week</h6>
                  <input name="50%paymentfor1week" type="text" value="<?php echo $price['50%paymentfor1week']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>30% Payment For 1 Week</h6>
                  <input name="30%paymentfor1week" type="text" value="<?php echo $price['30%paymentfor1week']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>100% Payment For 3days</h6>
                  <input name="100%payment" type="text" value="<?php echo $price['100%payment']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>50% Payment For 3days</h6>
                  <input name="50%payment" type="text" value="<?php echo $price['50%payment']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>30% Payment For 3days</h6>
                  <input name="30%payment" type="text" value="<?php echo $price['30%payment']; ?>">
               </div>
            </div>
           
            <div class="row">
               <div class="input-field col s12">
                  <button class="waves-effect waves-light btn button blue z-depth-2 right" type="submit" name="action">Save
                     <i class="material-icons right">save</i>
                  </button>
               </div>
            </div>
         </div>
         <?php echo form_open() ?>
      </div>
   </div>
</div>
