<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="card-content">
            <?php echo form_open('admin/updatedocument') ?>
            <!-- Form with placeholder -->
            <center><h1>Document</h1></center>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Mashou</h6>
                  <input type="hidden" name="stockid" value="<?php echo $document['stockid']; ?>" >
                  <input name="mashou" type="text" value="<?php echo $document['mashou']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Mashou Status</h6>
                  <input name="mashou_status" type="text" value="<?php echo $document['mashou_status']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Mashou Date</h6>
                  <input name="mashou_date" type="text" value="<?php echo $document['mashou_date']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Shipingorder</h6>
                  <input name="shipingorder" type="text" value="<?php echo $document['shipingorder']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Shipingorder Status</h6>
                  <input name="shipingorder_status" type="text" value="<?php echo $document['shipingorder_status']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Shipingorder Date</h6>
                  <input name="shipingorder_date" type="text" value="<?php echo $document['shipingorder_date']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Inspection</h6>
                  <input name="inspection" type="text" value="<?php echo $document['inspection']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Inspection Status</h6>
                  <input name="inspection_status" type="text" value="<?php echo $document['inspection_status']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Inspection Date</h6>
                  <input name="inspection_date" type="text" value="<?php echo $document['inspection_date']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Exportcertificate</h6>
                  <input name="exportcertificate" type="text" value="<?php echo $document['exportcertificate']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Exportcertificate Status</h6>
                  <input name="exportcertificate_status" type="text" value="<?php echo $document['exportcertificate_status']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Exportcertificate Date</h6>
                  <input name="exportcertificate_date" type="text" value="<?php echo $document['exportcertificate_date']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Bl_arrival</h6>
                  <input name="bl_arrival" type="text" value="<?php echo $document['bl_arrival']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Bl_arrival Status</h6>
                  <input name="bl_arrival_status" type="text" value="<?php echo $document['bl_arrival_status']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Bl_arrival Date</h6>
                  <input name="bl_arrival_date" type="text" value="<?php echo $document['bl_arrival_date']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>Translations</h6>
                  <input name="translations" type="text" value="<?php echo $document['translations']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Translations Status</h6>
                  <input name="translations_status" type="text" value="<?php echo $document['translations_status']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>Translations Date</h6>
                  <input name="translations_date" type="text" value="<?php echo $document['translations_date']; ?>">
               </div>
            </div>
            <!-- mail -->
            <center><h1>Mail</h1></center>
            <div class="row">
               <div class="input-field col s4">
                  <h6>parcelno</h6>
                  <input name="parcelno" type="text" value="<?php echo $document['parcelno']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>servicescompany</h6>
                  <input name="servicescompany" type="text" value="<?php echo $document['servicescompany']; ?>">
               </div>
              
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>requestby</h6>
                  <input name="requestby" type="text" value="<?php echo $document['requestby']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>requestrecevingdate</h6>
                  <input name="requestrecevingdate" type="text" value="<?php echo $document['requestrecevingdate']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>dateofdipatch</h6>
                  <input name="dateofdipatch" type="text" value="<?php echo $document['dateofdipatch']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>receivername</h6>
                  <input name="receivername" type="text" value="<?php echo $document['receivername']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>receveraddress</h6>
                  <input name="receveraddress" type="text" value="<?php echo $document['receveraddress']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>contactno</h6>
                  <input name="contactno" type="text" value="<?php echo $document['contactno']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>parcelweight</h6>
                  <input name="parcelweight" type="text" value="<?php echo $document['parcelweight']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>documentsdetails</h6>
                  <input name="documentsdetails" type="text" value="<?php echo $document['documentsdetails']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>accessories</h6>
                  <input name="document_accessories" type="text" value="<?php echo $document['document_accessories']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="waves-effect waves-light btn button blue z-depth-2 right" type="submit" name="action">Save
                     <i class="material-icons right">save</i>
                  </button>
               </div>
            </div>
         </div>
         <?php echo form_open() ?>
      </div>
   </div>
</div>
