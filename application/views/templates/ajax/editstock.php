<div class="row">
   <div class="col s12">
      <div class="card">
         <div class="card-content">
            <h4 class="card-title">stock Form</h4>
            <?php echo form_open('admin/updatestock') ?>
            <!-- Form with placeholder -->
            <div class="row">
               <div class="input-field col s4">
                  <h6>chassis_number</h6>
                  <input name="chassis_number" type="text" value="<?php echo $stock['chassis_number']; ?>">
                  <input name="stockid" type="hidden" value="<?php echo $stock['stock_id']; ?>">

               </div>
               <div class="input-field col s4">
                  <h6>km</h6>
                  <input name="km" type="text" value="<?php echo $stock['km']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>height</h6>
                  <input name="height" type="text" value="<?php echo $stock['height']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>color</h6>
                  <input name="color" type="text" value="<?php echo $stock['color']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>km2</h6>
                  <input name="km2" type="text" value="<?php echo $stock['km2']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>length</h6>
                  <input name="length" type="text" value="<?php echo $stock['length']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>drive</h6>
                  <input name="drive" type="text" value="<?php echo $stock['drive']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>seats</h6>
                  <input name="seats" type="text" value="<?php echo $stock['seats']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>width</h6>
                  <input name="width" type="text" value="<?php echo $stock['width']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>options</h6>
                  <input name="options" type="text" value="<?php echo $stock['options']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>engine_code</h6>
                  <input name="engine_code" type="text" value="<?php echo $stock['engine_code']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>m3</h6>
                  <input name="m3" type="text" value="<?php echo $stock['m3']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>transmission</h6>
                  <input name="transmission" type="text" value="<?php echo $stock['transmission']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>doors</h6>
                  <input name="doors" type="text" value="<?php echo $stock['doors']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>totalweight</h6>
                  <input name="totalweight" type="text" value="<?php echo $stock['totalweight']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>package</h6>
                  <input name="package" type="text" value="<?php echo $stock['package']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>enginetype</h6>
                  <input name="enginetype" type="text" value="<?php echo $stock['enginetype']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>accessories</h6>
                  <input name="accessories" type="text" value="<?php echo $stock['accessories']; ?>">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s4">
                  <h6>lhd/rhd</h6>
                  <input name="lhd/rhd" type="text" value="<?php echo $stock['lhd/rhd']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>loadingcapacity</h6>
                  <input name="loadingcapacity" type="text" value="<?php echo $stock['loadingcapacity']; ?>">
               </div>
               <div class="input-field col s4">
                  <h6>interiorgrade</h6>
                  <input name="interiorgrade" type="text" value="<?php echo $stock['interiorgrade']; ?>">
               </div>
               <div class="row">
                  <div class="input-field col s4">
                     <h6>exteriorgrade</h6>
                     <input name="exteriorgrade" type="text" value="<?php echo $stock['exteriorgrade']; ?>">
                  </div>
                  <div class="input-field col s8">
                     <h6>Note</h6>
                     <input name="note" type="text" value="<?php echo $stock['note']; ?>">
                  </div>
               </div>
               <div class="row">
                  <div class="input-field col s12">
                     <button class="waves-effect waves-light btn button red z-depth-2 right" type="submit" name="action">Save
                        <i class="material-icons right">save</i>
                     </button>
                  </div>
               </div>
            </div>
            <?php echo form_open() ?>
         </div>
      </div>
   </div>