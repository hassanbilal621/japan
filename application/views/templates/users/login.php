<!-- 
   !--=================================
     header -- -->
     <section class="inner-intro bg-1 bg-overlay-black-70">
         <div class="container">
            <div class="row text-center intro-title">
               <div class="col-md-6 text-md-left d-inline-block">
                  <h1 class="text-white">Login </h1>
               </div>
               <div class="col-md-6 text-md-right float-right">
                  <ul class="page-breadcrumb">
                     <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                     <li><span>Login</span> </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
     <!-- rigth sidebar start -->
      <section class="page-both-sidebar page-section-ptb">
      <div class="container-fluid">
         <div class="row">
         <?php  $this->load->view('templates/users/rightsidebar.php');?> 

         <div class="col-md-8">
            <div class="container-fluid">
               <?php echo form_open('users/login'); ?>
               <div class="row justify-content-center">
                  <div class="col-lg-7 col-md-12" style="border:1px solid #e3e3e3;">
                     <div class="row justify-content-center">
                        <div class="col-md-12"  style="background:#db2d2e;">
                           <div class="section-title" style="margin-bottom:20px !important; margin-top:20px !important;">
                              <h2  style="color:white;">Login To Your Account</h2>
                           </div>
                        </div>
                     </div>
                     <div class="gray-form clearfix">
                        <div class="form-group">
                           <label for="name">Email*</label>
                           <input id="name" class="form-control"  type="email" placeholder="Enter Your Email" name="username">
                        </div>
                        <div class="form-group">
                           <label for="Password">Password* </label>
                           <input id="Password" class="form-control" type="password" placeholder="Password" name="password">
                        </div>
                        <div class="form-group">
                           <div class="remember-checkbox mb-4">
                              <input type="checkbox" name="one" id="one">
                              <label for="one"> Remember me</label>
                              <a href="<?php echo base_url();?>" class="float-right">Forgot Password?</a>
                           </div>
                        </div>
                        <button type="submit" class="button red"> Log in </button>
                     </div>
                  </div>
               </div>
               <?php echo form_close();?> 
            </div>
         </div>

          <?php  $this->load->view('templates/users/leftsidebar.php');?> 
          <!-- !-- Left Side Bar -- -->
</div>
</div>
</section>