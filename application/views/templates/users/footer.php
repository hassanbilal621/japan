<!--=================================
	footer -->
  <footer class="footer bg-3 bg-overlay-black-90">
	<div class="col-md-12">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="social">
					<ul>
						<li style="background: #3a5897;"><a class="facebook" href="#">facebook <i class="fa fa-facebook"></i> </a></li>
						<li style="background: #41d1da;"><a class="twitter" href="#">twitter <i class="fa fa-twitter"></i> </a></li>
						<li style="background: #c3222b;"><a class="instagram" href="#">instagram <i class="fa fa-instagram"></i> </a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-3 col-md-6">
				<div class="about-content">
					<img class="img-fluid" id="" src="<?php echo base_url(); ?>assets/front-app-assets/images/logo-light.png" alt="">
					<p>We provide everything you need to build an amazing dealership website developed especially for car sellers dealers or auto motor retailers.</p>
				</div>
				<div class="address">
					<ul>
						<li> <i class="fa fa-map-marker"></i><span>220E Front St. Burlington NC 27215</span> </li>
						<li> <i class="fa fa-phone"></i><span>(007) 123 456 7890</span> </li>
						<li> <i class="fa fa-envelope-o"></i><span>support@nexco.com</span> </li>
						<h5 class="text-white">Membership</h5>
						<a href="http://www.jumvea.or.jp/members/-548" rel="nofollow" target="_blank"><img src="<?php echo base_url(); ?>assets\app-assets\images\jamvea.png" alt=""></a>
						<p>Japan Used Motor Vehicle Exporters Association (JUMVEA)</p>
					</ul>
				</div>
			</div>
			<div class="col-lg-2 col-md-6">
				<div class="usefull-link">
					<h6 class="text-white" style="margin: 20px 0 5px 0;padding: 0;font-size: x-large;">Countries</h6>
					<h6 class="text-white" style="margin: 20px 0 5px 0;padding: 0;">Africa</h6>
					<?php
						for ($x = 0; $x <= 13; $x++) {
						      ?>
					<div> <img src="<?php echo base_url(); ?>assets\app-assets\images\flag.png" alt= "" style="height: 10px;"> Africa</div>
					<?php }?>
				</div>
			</div>
			<div class="col-lg-2 col-md-6">
				<div class="usefull-link">
					<h6 class="text-white" style="margin: 40px 0 5px 0;padding: 0;">CARIBBEAN SEA</h6>
					<?php
						for ($x = 0; $x <= 8; $x++) {
						      ?>
					<div> <img src="<?php echo base_url(); ?>assets\app-assets\images\flag.png" alt= "" style="height: 10px;"> CARIBBEAN SEA</div>
					<?php }?>	
				</div>
			</div>
			<div class="col-lg-2 col-md-6">
				<div class="usefull-link">
					<h6 class="text-white" style="margin: 40px 0 5px 0;padding: 0;">Asia</h6>
					<?php
						for ($x = 0; $x <= 5; $x++) {
						      ?>
					<div> <img src="<?php echo base_url(); ?>assets\app-assets\images\flag.png" alt= "" style="height: 10px;">  PAkISTAN</div>
					<?php }?>
					<h6 class="text-white"  style="margin: 20px 0 5px 0;padding: 0;">EUROPE</h6>
					<?php
						for ($x = 0; $x <= 4; $x++) {
						      ?>
					<div> <img src="<?php echo base_url(); ?>assets\app-assets\images\flag.png" alt= "" style="height: 10px;"> Canada</div>
					<?php }?>
				</div>
			</div>
			<div class="col-lg-2 col-md-6">
				<div class="usefull-link">
					<h6 class="text-white" style="margin: 40px 0 5px 0;padding: 0;">OCEANIA</h6>
					<?php
						for ($x = 0; $x <= 6; $x++) {
						      ?>
					<div> <img src="<?php echo base_url(); ?>assets\app-assets\images\flag.png" alt= "" style="height: 10px;">OCEANIA</div>
					<?php }?>
					<h6 class="text-white" style="margin: 20px 0 5px 0;padding: 0;">North America</h6>
					<?php
						for ($x = 0; $x <= 1; $x++) {
						      ?>
					<div> <img src="<?php echo base_url(); ?>assets\app-assets\images\flag.png" alt= "" style="height: 10px;"> Canada</div>
					<?php }?>
				</div>
			</div>
		</div>
		<hr />
		<div class="copyright">
			<div class="row">
				<div class="col-lg-6 col-md-12">
					<div class="text-lg-left text-center">
						<p>©Copyright 2020 Car Dealer Developed by Nexco Japan</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<ul class="list-inline text-lg-right text-center">
						<li><a href="#">privacy policy </a> | </li>
						<li><a href="#">terms and conditions </a> |</li>
						<li><a href="#">contact us </a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--=================================
	footer --> 
<!--=================================
	color customizer --> 
<!-- <div class="style-customizer closed">
	<div class="buy-button"> <a class="opener" href="#"><i class="fa fa-cog fa-spin"></i></a>  </div>
	<div class="clearfix content-chooser">
	   <a target="_blank" class="button" href="https://themeforest.net/item/car-dealer-the-best-car-dealer-automotive-responsive-html5-template/19226545?ref=Potenzaglobalsolutions">purchase now</a> 
	    <h3>Color Schemes</h3>
	    <p>Which theme color you want to use? Here are some predefined colors.</p>
	    <ul class="styleChange clearfix">
	      <li class="skin-default selected" title="Default" data-style="skin-default" ></li>
	      <li class="skin-blue" title="Blue" data-style="skin-blue" ></li>
	      <li class="skin-orange" title="Orange" data-style="skin-orange"></li>
	      <li class="skin-purple" title="purple" data-style="skin-purple"></li>
	      <li class="skin-gold" title="gold" data-style="skin-gold"></li>
	      <li class="skin-green" title="green" data-style="skin-green"></li>
	      <li class="skin-palatinate-blue" title="palatinate-blue" data-style="skin-palatinate-blue" ></li>
	      <li class="skin-yellow" title="Yellow" data-style="skin-yellow"></li>
	    </ul>
	    <ul class="resetAll">
	    <li><a class="button button-reset" href="#">Reset All</a></li>
	  </ul>
	</div>
	</div> -->
<!--=================================
	color customizer --> 
<!--=================================
	back to top -->
<div class="car-top">
	<span><img src="images/car.png" alt=""></span>
</div>
<!--=================================
	back to top -->
<!--=================================
	jquery -->
<!-- jquery  -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.simpleLoadMore.js"></script>
<!-- <script src="jquery.simpleLoadMore.js"></script> -->
<script>
	$('.api_countries').simpleLoadMore({
	   item: 'li',
	   count: 15
	});
	
</script>
<script>
	$('.car_makers').simpleLoadMore({
	   item: 'li',
	   count: 15
	});
	
</script>
<!-- bootstrap -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/popper.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/bootstrap.min.js"></script>
<!-- mega-menu -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/mega-menu/mega_menu.js"></script>
<!-- appear -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/jquery.appear.js"></script>
<!-- jquery-ui -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/jquery-ui.js"></script>
<!-- counter -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/counter/jquery.countTo.js"></script>
<!-- owl-carousel -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/owl-carousel/owl.carousel.min.js"></script>
<!-- select -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/select/jquery-select.js"></script>
<!-- magnific popup -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/slick/slick.min.js"></script>
<!-- revolution -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
<!-- style customizer  -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/style-customizer.js"></script>
<!-- custom -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-app-assets/js/custom.js"></script>
<script type="text/javascript">
	(function($){
	"use strict";
	
	 var tpj=jQuery;
	   var revapi2;
	   tpj(document).ready(function() {
	     if(tpj("#rev_slider_2_1").revolution == undefined){
	       revslider_showDoubleJqueryError("#rev_slider_2_1");
	     }else{
	       revapi2 = tpj("#rev_slider_2_1").show().revolution({
	         sliderType:"standard",
	         sliderLayout:"fullwidth",
	         dottedOverlay:"none",
	         delay:9000,
	         navigation: {
	           keyboardNavigation:"off",
	           keyboard_direction: "horizontal",
	           mouseScrollNavigation:"off",
	                          mouseScrollReverse:"default",
	           onHoverStop:"off",
	           bullets: {
	             enable:true,
	             hide_onmobile:false,
	             style:"hermes",
	             hide_onleave:false,
	             direction:"horizontal",
	             h_align:"center",
	             v_align:"bottom",
	             h_offset:0,
	             v_offset:50,
	                             space:10,
	             tmp:''
	           }
	         },
	         visibilityLevels:[1240,1024,778,480],
	         gridwidth:1570,
	         gridheight:1000,
	         lazyType:"none",
	         shadow:0,
	         spinner:"spinner3",
	         stopLoop:"off",
	         stopAfterLoops:-1,
	         stopAtSlide:-1,
	         shuffle:"off",
	         autoHeight:"off",
	         disableProgressBar:"on",
	         hideThumbsOnMobile:"off",
	         hideSliderAtLimit:0,
	         hideCaptionAtLimit:0,
	         hideAllCaptionAtLilmit:0,
	         debugMode:false,
	         fallbacks: {
	           simplifyAll:"off",
	           nextSlideOnWindowFocus:"off",
	           disableFocusListener:false,
	         }
	       });
	     }
	   }); 
	})(jQuery);
	
</script>
</body>
<!-- Mirrored from themes.potenzaglobalsolutions.com/html/cardealer/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Oct 2019 11:08:33 GMT -->
</html>