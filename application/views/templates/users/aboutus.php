<section class="inner-intro bg-1 bg-overlay-black-70">
   <div class="container">
      <div class="row text-center intro-title">
         <div class="col-sm-6 text-md-left d-inline-block">
            <h1 class="text-white">About us </h1>
         </div>
         <div class="col-sm-6 text-md-right float-right">
            <ul class="page-breadcrumb">
               <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
               <li><a href="#">pages</a> <i class="fa fa-angle-double-right"></i></li>
               <li><span>about us</span> </li>
            </ul>
         </div>
      </div>
   </div>
</section>
<section class="welcome-4 page-section-ptb white-bg">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-md-10">
            <div class="section-title">
               <span>Welcome to </span>
               <h2>the Cardealer online</h2>
               <div class="separator"></div>
               <p>Car Dealer is the best premium HTML5 Template. We provide everything you need to build an <strong>Amazing dealership website</strong>  developed especially for car sellers, dealers or auto motor retailers. You can use this template for creating website based on any framework language.</p>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="owl-carousel owl-theme" data-nav-arrow="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-space="0">
               <div class="item">
                  <img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/bg/06.jpg" alt="">
               </div>
               <div class="item">
                  <img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/bg/01.jpg" alt="">
               </div>
               <div class="item">
                  <img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/bg/02.jpg" alt="">
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-3 col-md-6">
            <div class="feature-box text-center">
               <div class="icon">
                  <i class="glyph-icon flaticon-beetle"></i>
               </div>
               <div class="content">
                  <h6>All brands</h6>
                  <p>Printin  k a galley of type lorem Ipsum is simply dummy text of the and</p>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="feature-box text-center">
               <div class="icon">
                  <i class="glyph-icon flaticon-interface-1"></i>
               </div>
               <div class="content">
                  <h6>Free Support</h6>
                  <p>Simply dummy text of the printin  k a galley of type and lorem Ipsum is </p>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="feature-box text-center">
               <div class="icon">
                  <i class="glyph-icon flaticon-key"></i>
               </div>
               <div class="content">
                  <h6>Dealership</h6>
                  <p>Text of the printin lorem Ipsum is simply dummy k a galley of type and</p>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-6">
            <div class="feature-box text-center">
               <div class="icon">
                  <i class="glyph-icon flaticon-wallet"></i>
               </div>
               <div class="content">
                  <h6>AFFORDABLE</h6>
                  <p>The printin  Lorem Ipsum is simply dummy text of k a galley of type and</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>