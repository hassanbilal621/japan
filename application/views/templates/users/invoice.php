<section>
	<div class="pt-1 pb-0" id="breadcrumbs-wrapper">
		<!-- Search for small screen-->
	</div>
	<div class="col s12">
		<div class="container">
			<div class="seaction">
				<!--Invoice-->
				<div class="row">
					<div class="col s12 m12 l12">
						<div id="basic-tabs" class="card card card-default scrollspy">
							<div class="card-content pt-5 pr-5 pb-5 pl-5">
								<div id="invoice">
									<div class="invoice-header">
										<div class="row section">
											<div class="col s12 m6 l6">
												<img class="mb-2 width-50" src="<?php echo base_url(); ?>assets/front-app-assets/images/logo-dark.png" alt="company logo">
												<p>125, ABC Street, New Yourk, USA</p>
												<p>888-555-2311</p>
											</div>
											<div class="col s12 m6 l6">
												<img src="<?php echo base_url(); ?>assets\uploads/<?php echo $invoice['statusimg']; ?>" alt="<?php echo $invoice['invoicestatus']; ?>"style="width: 40%;float: right;">
												<p style="float: right;margin: 65px -111px 0 0;">Date: <?php echo $invoice['statusdate']; ?></p>
												
											</div>
											
										</div>
										<div class="row section">
											<div class="col s12 m6 l6">
												<h6 class="text-uppercase strong mb-2 mt-3">Recipient</h6>
												<p class="text-uppercase"><?php echo $invoice['name']; ?></p>
												<p><?php echo $invoice['address']; ?></p>
												<p><?php echo $invoice['phone']; ?></p>
											</div>
											<div class="col s12 m6 l6">
												<div class="invoce-no right-align">
													<p><span class="text-uppercase strong">Invoice No. </span><?php echo $invoice['invoice_id']; ?></p>
												</div>
												<div class="invoce-no right-align">
													<p><span class="text-uppercase strong">Invoice Date. </span><?php echo $invoice['date']; ?></p>
												</div>
											</div>
										</div>
									</div>
									<div class="invoice-table">
										<div class="row">
											<div class="col s12 m12 l12">
												<table class="table responsive-table">
													<thead>
														<tr>
															<th>No</th>
															<th>Maker</th>
															<th>Model</th>
															<th>Invoice Amount</th>
															<th>Status</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1.</td>
															<td><?php echo $invoice['model']; ?></td>
															<td><?php echo $invoice['maker']; ?></td>
															<td><?php echo $invoice['pay_amount']; ?></td>
															<td><?php echo $invoice['invoicestatus']; ?></td>
														</tr>
														</tbody>
                        </table>
                        <table class="table responsive-table">
														<tr class="border-none">
															<td colspan="3"></td>
															<td>Sub Total:</td>
															<td><?php echo $invoice['pay_amount']; ?></td>
														</tr>
														<tr class="border-none">
															<td colspan="3"></td>
															<td>Service Tax</td>
															<td>0.00</td>
														</tr>
														<tr class="border-none">
															<td colspan="3"></td>
															<td class="white-text pl-1" style="background-color:  #3299CC;">Grand Total</td>
															<td class="strong white-text" style="background-color:  #3299CC;"><?php echo $invoice['pay_amount']; ?></td>
														</tr>
													
												</table>
											</div>
										</div>
									</div>
									<div class="invoice-footer mt-6">
										<div class="row">
											<div class="col s12 m6 l6">
												<p class="strong">Payment Method</p>
												<p>Please make the cheque to: AMANDA ORTON</p>
												<p class="strong">Terms & Condition</p>
												<ul>
													<li>You know, being a test pilot isn't always the healthiest business in the world.</li>
													<li>We predict too much for the next year and yet far too little for the next 10.</li>
												</ul>
											</div>
											<div class="col s12 m6 l6 center-align">
												<p>Approved By</p>
												<img src="<?php echo base_url(); ?>assets/app-assets/images/misch/signature-scan.png" alt="signature">
												<p class="header">AMANDA ORTON</p>
												<p>Managing Director</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- START RIGHT SIDEBAR NAV -->
			<!-- END RIGHT SIDEBAR NAV -->
		</div>
	</div>
</section>