<section class="inner-intro bg-1 bg-overlay-black-70">
   <div class="container">
      <div class="row text-center intro-title">
         <div class="col-md-6 text-md-left d-inline-block">
            <h1 class="text-white">My Account </h1>
         </div>
         <div class="col-md-6 text-md-right float-right">
            <ul class="page-breadcrumb">
               <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
               <li><span>Profile</span> </li>
            </ul>
         </div>
      </div>
   </div>
</section>
<!--=================================
   inner-intro -->
<!--=================================
   login-form  -->
<section class="login-form page-section-ptb">
   <div class="container">
      <div class="row justify-content-center">    
            <section class="container">
               <div class="row">
                  <div class="col-sm-3">     
                     <ul class="list-group">
                        <li class="list-group-item text-muted"><div class="text-center">
                           <?php echo form_open_multipart('users/uploadpicture'); ?>
                              <img src="<?php if(isset($user['picture'])){ echo base_url()."assets/uploads/".$user['picture']; }else{ echo 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png'; } ?>" class="avatar img-circle img-thumbnail" alt="avatar">
                              <h6> Upload a different photo...</h6>
                              <input type="file" name="userfile" class="text-center center-block file-upload">
                              <button type="submit" class="button red" style="padding: 0x 0px !important;" >Upload Picture </button> 
                           </div>
                           <?php echo form_close();?>
                        
                        </li>
                        <li class="list-group-item text-right"><span class="pull-left"><a href="">Your Orders</a></li>
                    
                     </ul>
                  </div>
                  <!--/col-3-->
                  <div class="col-sm-9">
                     <div class="row justify-content">
                
                        <div class="col-lg-12 col-md-12" style="border:1px solid #e3e3e3;">
                           <div class="row justify-content-center">
                              <div class="col-md-12"  style="background:#db2d2e;">
                                 <div class="section-title" style="margin-bottom:20px !important; margin-top:20px !important;">
                                    <h2 style="color:white;">Update Your Profile</h2>
                                 </div>
                              </div>
                           </div>
                           <?php echo form_open('users/updateprofile');?>
                           <div class="gray-form">
                              <div class="form-group">
                                 <label>Full Name *</label>
                                 <input type="text" class="form-control" name="name" id="first_name" placeholder="" value="<?php echo $user['name']; ?>" title="enter your first name if any.">
                              </div>
                              <div class="form-group">
                                 <label>Email *</label>
                                 <input type="email" class="form-control" name="email" id="email" placeholder="" value="<?php echo $user['email']; ?>" title="enter your email.">
                              </div>
                              <div class="form-group">
                                 <label>Address *</label>
                                 <input type="text" class="form-control" name="address" placeholder="" value="<?php echo $user['address']; ?>" title="enter a location">
                              </div>
                              <div class="form-group">
                                 <label>Phone *</label>
                                 <input type="text" class="form-control" name="phone" id="phone" placeholder="" value="<?php echo $user['phone']; ?>" title="enter your phone number if any.">
                              </div>
                              <div class="form-group">
                                 <label>Counrty *</label>
                                 <input type="text" class="form-control" name="country" readonly id="" placeholder="" value="<?php echo $user['country']; ?>" title="enter your phone number if any.">
                              </div>
                              <div class="form-group">
                                 <label>Gender *</label>
                                 <input type="text" class="form-control" readonly id="" placeholder="Gender" value="<?php echo $user['gender']; ?>" title="">
                              </div>
                              <div class="form-group">
                                 <div class="col-xs-12">
                                    <br>
                                    <button type="submit" class="button red"> Update an account </button>
                                 </div>
                              </div>
                           </div>
                           <?php echo form_close(); ?>
                        </div>
                        <!--/tab-pane-->
                        <!--/tab-pane-->
                     </div>
                     <!--/tab-content-->
                  </div>
                  <!--/col-9-->
               </div>
               <!--/row-->
            </section>
         </div>
     
   </div>
   </div>
</section>
<!--=================================
   login-form  -->