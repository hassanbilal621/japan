<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themes.potenzaglobalsolutions.com/html/cardealer/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 31 Oct 2019 11:07:00 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Nexco Japan LTD CO | Search Online Cars</title>

<!-- Favicon -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/front-app-assets/images/favicon.png" />

<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/bootstrap.min.css" />

<!-- flaticon -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/flaticon.css" />

<!-- mega menu -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/mega-menu/mega_menu.css" />

<!-- font awesome -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/font-awesome.min.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/slick/slick.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/slick/slick-theme.css" />




<!-- owl-carousel -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/owl-carousel/owl.carousel.css" />

<!-- magnific-popup -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/magnific-popup/magnific-popup.css" />

<!-- revolution -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/revolution/css/settings.css" />

<!-- main style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/style.css" />

<!-- responsive -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/responsive.css" />

<!-- Style customizer -->
<link rel="stylesheet" href="#" data-style="styles" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-app-assets/css/style-customizer.css" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-557RCPW');</script>
<!-- End Google Tag Manager -->

</head>

<body>
 


  