<style>
	th,td
	{
	text-align: center;
	border: 2px black solid;
	padding: 8px;
	}
	th
	{
		color: red;
	}
	td
	{
		color: blue;

	}
input
	{
	color: blue;
    text-align: center;
	border: none;
	width: 39%;
	}
	
</style>
<section class="product-listing page-section-ptb">
	<div class="container">
        <div class="row">
            <h2 style="margin: 0 0 20px 15px;">View Order Invoies</h2>
            <div class="col-lg-12 col-md-12">
                <div class="sorting-options-main"style="margin: 30px 0 40px 0;">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Stock Id</th>
                                    <td><?php echo $orderinfo['stock_id']; ?></td>
                                </tr>
                                <tr>
                                <th>Maker</th>
                                <td><?php echo $orderinfo['maker']; ?></td>
                                </tr>
                                <tr>
                                    <th>Model</th>
                                    <td><?php echo $orderinfo['model']; ?></td>
                                </tr>
                                <tr>
                                    <th>Sale Price</th>
                                    <td><?php echo $orderinfo['sale_price']; ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6 col-md-6">     
                            <td><img src="<?php echo base_url(); ?>assets\uploads\<?php echo $orderinfo['stock_picture_filename'];?>"style="margin: 10px 0 6px 44px;border: 3px black solid;width: 235px;float: left;"> </td>
                        </div>
                    </div>
                </div>    
            </div>
        </div>   
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="sorting-options-main">
					
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Invoice Id</th>
                                <th>Invoice Date</th>
                                <th>Invoice Amount</th>
                                <th>Invoice Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <?php
				        	foreach($invoices as $invoice):
				    	?>
                        <tbody >
                            
                            <tr>
                                <td><?php echo $invoice['invoice_id']; ?></td>
                                <td><?php echo $invoice['date']; ?></td>
                                <td><?php echo $invoice['pay_amount']; ?></td>
                                <td><?php echo $invoice['invoicestatus']; ?></td>
                                <td><a class="button red" href="<?php echo base_url(); ?>users/invoice/<?php echo  $invoice['invoice_id']; ?>">View Invoice</a</td>
                            </tr>
                        </tbody>
                        <?php endforeach; ?>
                        
                    </table>

                   <center> <h5>No more invoices</h5></center>
				</div>
			</div>
			
		</div>
	</div>
	</div>
</section>