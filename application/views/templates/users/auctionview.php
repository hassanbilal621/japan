<?php 
// echo '<pre>';
// print_r($stockimgs);
// echo '<pre>';
?>

<!--=================================
	inner-intro -->
  <section class="inner-intro bg-1 bg-overlay-black-70">
	<div class="container">
		<div class="row text-center intro-title">
			<div class="col-md-6 text-md-left d-inline-block">
				<h1 class="text-white">Hyundai Santa Fe </h1>
			</div>
			<div class="col-md-6 text-md-right float-right">
				<ul class="page-breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
					<li><a href="#">pages</a> <i class="fa fa-angle-double-right"></i></li>
					<li><span>details 01</span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--=================================
	inner-intro -->
<!--=================================
	car-details  -->
<section class="car-details page-section-ptb">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h3><?php echo $stock['maker']; ?> - <?php echo $stock['model'];?> </h3>
				<p>Temporibus possimus quasi beatae, You will begin to realize why, consectetur adipisicing elit. aspernatur nemo maiores.</p>
			</div>
			<div class="col-md-3">
				<div class="car-price text-lg-right">
					<strong>$ <?php echo $stock['sale_price']; ?></strong>
					<span>Plus Taxes & Licensing</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="details-nav">
					<ul>
						<li>
							<a href="#" data-toggle="modal" data-target="#exampleModal" class="button red">Request More Info</a>
							<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="exampleModalLabel">Request More Info</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<p class="sub-title">Please fill out the information below and one of our representatives will contact you regarding your more information request. </p>
											<form class="gray-form reset_css" id="rmi_form" action="http://themes.potenzaglobalsolutions.com/html/cardealer/post">
												<input type="hidden" name="action" value="request_more_info" />
												<div class="alrt">
													<span class="alrt"></span>
												</div>
												<div class="form-group">
													<label>Name*</label>
													<input type="text" class="form-control" name="rmi_name" id="rmi_name" />
												</div>
												<div class="form-group">
													<label>Email address*</label>
													<input type="text" class="form-control" name="rmi_email" id="rmi_email" />
												</div>
												<div class="form-group">
													<label>Phone*</label>
													<input type="text" class="form-control"  id="rmi_phone" name="rmi_phone" >
												</div>
												<div class="form-group">
													<label>Message</label>
													<textarea class="form-control" name="rmi_message" id="rmi_message"></textarea>                            
												</div>
												<div class="form-group">
													<label>Preferred Contact*</label>
													<div class="radio">
														<label><input type="radio" name="rmi_radio" value="Email" checked="checked"/>Email</label>
													</div>
													<div class="radio">
														<label><input type="radio" name="rmi_radio" value="Phone" />Phone</label>
													</div>
												</div>
												<div class="form-group">
													<div id="recaptcha1"></div>
												</div>
												<div class="form-group">
													<a class="button red" id="request_more_info_submit">Submit</a>
													<i class="fa fa-refresh fa-spin fa-3x fa-fw load_spiner"  style="display: none;"></i>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</li>
						<!-- <li>
							<a data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo" href="#" class="css_btn active"><i class="fa fa-dashboard "></i>Schedule Test Drive</a>
							<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="exampleModalLabel">Schedule Test Drive</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div id="std_message"></div>
										<div class="modal-body">
											<p class="sub-title">Complete this form to request a test drive of your favorite car. Our Sales Advisor will contact you promptly to confirm your appointment. </p>
											<form class="gray-form reset_css" id="std_form" action="http://themes.potenzaglobalsolutions.com/html/cardealer/post">
												<input type="hidden" name="action" value="schedule_test_drive" />
												<div class="form-group">
													<label>Name*</label>
													<input type="text" class="form-control" id="std_firstname" name="std_firstname" />
												</div>
												<div class="form-group">
													<label>Email address*</label>
													<input type="text" class="form-control"  id="std_email" name="std_email">
												</div>
												<div class="form-group">
													<label>Phone*</label>
													<input type="text" class="form-control"  id="std_phone" name="std_phone" >
												</div>
												<div class="form-group">
													<label>Preferred Day*</label>
													<input type="text" class="form-control"  id="std_day" name="std_day">
												</div>
												<div class="form-group">
													<label>Preferred Time*</label>
													<input type="text" class="form-control"  id="std_time" name="std_time">
												</div>
												<div class="form-group">
													<label>Preferred Contact*</label>
													<div class="radio">
														<label><input type="radio" id="std_optradio" name="std_optradio" value="email" checked>Email</label>
													</div>
													<div class="radio">
														<label><input type="radio" id="std_optradio" name="std_optradio" value="phone">Phone</label>
													</div>
												</div>
												<div class="form-group">
													<div id="recaptcha2"></div>
												</div>
												<div class="form-group">
													<a class="button red" id="schedule_test_drive_submit">Schedule Now</a>
													<i class="fa fa-refresh fa-spin fa-3x fa-fw load_spiner"  style="display: none;"></i>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</li> -->
						<li>
							<a data-toggle="modal" data-target="#exampleModal3" data-whatever="@mdo" href="#" class="button red"><i class="fa fa-tag"></i>Make an Offer</a>
							<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="exampleModalLabel">Make an Offer</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div id="mao_message"></div>
										<div class="modal-body">
											<form class="gray-form reset_css" action="http://themes.potenzaglobalsolutions.com/html/cardealer/post" id="mao_form">
												<input type="hidden" name="action" value="make_an_offer" />
												<div class="form-group">
													<label>Name*</label>
													<input type="text" id="mao_name" name="mao_name" class="form-control">
												</div>
												<div class="form-group">
													<label>Email address*</label>
													<input type="text" id="mao_email" name="mao_email" class="form-control">
												</div>
												<div class="form-group">
													<label>Phone*</label>
													<input type="text" id="mao_phone" name="mao_phone" class="form-control">
												</div>
												<div class="form-group">
													<label>Offered Price*</label>
													<input type="text" id="mao_price" name="mao_price" class="form-control">
												</div>
												<div class="form-group">
													<label>Financing Required*</label>
													<div class="selected-box">
														<select class="selectpicker" id="mao_financing" name="mao_financing">
															<option value="Yes">Yes </option>
															<option value="No">No</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label>additional Comments/Conditions*</label>
													<textarea class="form-control input-message" rows="4" id="mao_comments" name="mao_comments"></textarea>
												</div>
												<div class="form-group">
													<label>Preferred Contact*</label>
													<div class="radio">
														<label><input type="radio" id="mao_radio" name="mao_radio" value="email" checked>Email</label>
													</div>
													<div class="radio">
														<label><input type="radio" id="mao_radio" name="mao_radio" value="phone">Phone</label>
													</div>
												</div>
												<div class="form-group">
													<div id="recaptcha3"></div>
												</div>
												<div class="form-group">
													<a class="button red" id="make_an_offer_submit">Submit</a>
													<i class="fa fa-refresh fa-spin fa-3x fa-fw load_spiner"  style="display: none;"></i>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</li>


						<li>
							<a data-toggle="modal" data-target="#exampleModal4" data-whatever="@mdo" href="#" class="button red"><i class="fa fa-tag"></i>Make a Order</a>
							<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="exampleModalLabel">Make a Order</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<?php echo form_open('Users/auctionorder')?>
										<div id="mao_message"></div>
											<div class="modal-body">
												<div class="row">
												<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2">StockID*</label>
																	<div class="col-lg-10">
																	<strong class="text-right"><?php echo $stock['stock_id']; ?></strong>
																	<input type="hidden" name="stockid" value="<?php echo  $stock['stock_id']; ?>">
																	</div>  
																</div>
															
														</div>
													</div>
													<div class="col-lg-6">
														<div class="price-slide">
														<div class="row">
																	<label class="col-lg-2" >Name*</label>
																	<div class="col-lg-3">
																		<div class="selected-box">
																			<select name='mrs'>
																				<option>Mr.</option>
																				<option>Mrs.</option>
																				<option>Miss.</option>
																				<option>sir.</option>
																				
																				
																			</select>
																		</div>
																	</div>
																	<div class="col-lg-7">
																		<input id="name" class="form-control" type="text"  name="name">
																	</div>
																</div>
															
														</div>
													</div>
																<!-- <div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2">Steering*</label>
																	<div class="col-lg-10">
																		<div class="row">
																			<div class="col-6">
																				<div class="remember-checkbox">
																					<input type="checkbox" name="steering" value="left" id="left" />
																					<label for="left">Left Hand</label>
																				</div>
																				`
																			</div>
																			<div class="col 6">
																				<div class="remember-checkbox">
																					<input type="checkbox" name="steering" value="right" id="right" />
																					<label for="right">Right Hand</label>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
														</div>
													</div> -->
												
												</div>
												<div class="row">
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2" >Model*</label>
																	<div class="col-lg-10">
																	<strong class="text-right"><?php echo $stock['model']; ?></strong>
																	</div>
																</div>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2" >Country*</label>
																	<div class="col-lg-10">
																		<div class="selected-box">
																			<select name="country_name">
																				<option>Country  </option>
																				<?php foreach($countries as $country): ?>
																				<option value="<?php echo $country['geoname_id']; ?>"><?php echo $country['country_name']; ?></option>
																				<?php endforeach; ?>
																			</select>
																		</div>
																	</div>
																</div>
														</div>
													</div>
												</div>
												<div class="row">
												<div class="col-lg-6">
														<div class="price-slide">
														<div class="row">
																	<label class="col-lg-2">Make*</label>
																	<div class="col-lg-10">
																	<strong class="text-right"><?php echo $stock['maker']; ?></strong>
																		
																	</div>
																</div>
																
															
														</div>
													</div>
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2">Port*</label>
																	<div class="col-lg-10" class="selected-box">
																		<select name="port">
																			<option>Port  </option>
																			<?php foreach($ports as $port): ?>
																			<option ><?php echo $port['port_name'];?></option>
																			<?php endforeach; ?>
																		</select>
																	</div>
																</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2">Year*</label>
																	<strong class="text-right"><?php echo $stock['year']; ?></strong>
																	<!-- <div  class="selected-box col-4">
																		<select name="to_year">
																			<option value="0" selected>Any</option>
																			<option value="2019">2019</option>
																			<option value="2018">2018</option>
																			<option value="2017">2017</option>
																			<option value="2016">2016</option>
																			<option value="2015">2015</option>
																			<option value="2014">2014</option>
																			<option value="2013">2013</option>
																			<option value="2012">2012</option>
																			<option value="2011">2011</option>
																			<option value="2010">2010</option>
																			<option value="2009">2009</option>
																			<option value="2008">2008</option>
																			<option value="2007">2007</option>
																			<option value="2006">2006</option>
																			<option value="2005">2005</option>
																			<option value="2004">2004</option>
																			<option value="2003">2003</option>
																			<option value="2002">2002</option>
																			<option value="2001">2001</option>
																			<option value="2000">2000</option>
																			<option value="1999">1999</option>
																			<option value="1998">1998</option>
																			<option value="1997">1997</option>
																			<option value="1996">1996</option>
																			<option value="1995">1995</option>
																			<option value="1994">1994</option>
																			<option value="1993">1993</option>
																			<option value="1992">1992</option>
																			<option value="1991">1991</option>
																			<option value="1990">1990</option>
																			<option value="1989">1989</option>
																			<option value="1988">1988</option>
																			<option value="1987">1987</option>
																			<option value="1986">1986</option>
																			<option value="1985">1985</option>
																			<option value="1984">1984</option>
																			<option value="1983">1983</option>
																			<option value="1982">1982</option>
																			<option value="1981">1981</option>
																			<option value="1980">1980</option>
																		</select>
																	</div>-
																	<div  class="selected-box col-4">
																		<select name="from_year">
																			<option value="0" selected>Any</option>
																			<option value="2019">2019</option>
																			<option value="2018">2018</option>
																			<option value="2017">2017</option>
																			<option value="2016">2016</option>
																			<option value="2015">2015</option>
																			<option value="2014">2014</option>
																			<option value="2013">2013</option>
																			<option value="2012">2012</option>
																			<option value="2011">2011</option>
																			<option value="2010">2010</option>
																			<option value="2009">2009</option>
																			<option value="2008">2008</option>
																			<option value="2007">2007</option>
																			<option value="2006">2006</option>
																			<option value="2005">2005</option>
																			<option value="2004">2004</option>
																			<option value="2003">2003</option>
																			<option value="2002">2002</option>
																			<option value="2001">2001</option>
																			<option value="2000">2000</option>
																			<option value="1999">1999</option>
																			<option value="1998">1998</option>
																			<option value="1997">1997</option>
																			<option value="1996">1996</option>
																			<option value="1995">1995</option>
																			<option value="1994">1994</option>
																			<option value="1993">1993</option>
																			<option value="1992">1992</option>
																			<option value="1991">1991</option>
																			<option value="1990">1990</option>
																			<option value="1989">1989</option>
																			<option value="1988">1988</option>
																			<option value="1987">1987</option>
																			<option value="1986">1986</option>
																			<option value="1985">1985</option>
																			<option value="1984">1984</option>
																			<option value="1983">1983</option>
																			<option value="1982">1982</option>
																			<option value="1981">1981</option>
																			<option value="1980">1980</option>
																		</select>
																	</div> -->
																</div>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2" >Email*</label>
																	<div class="col-lg-10">
																		<input id="name" class="form-control" type="text"  name="email">
																	</div>
																</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2">Price*</label>
																	<strong name="sale_price" class="text-right"><?php echo $stock['sale_price']; ?></strong>
																	<!-- <div  class="selected-box col 5">
																		<select name="maxprice">
																			<option value="0" selected>Any</option>
																			<option value="799">$799</option>
																			<option value="1000">$1,000</option>
																			<option value="1500">$1,500</option>
																			<option value="2000">$2,000</option>
																			<option value="3000">$3,000</option>
																			<option value="4000">$4,000</option>
																			<option value="5000">$5,000</option>
																			<option value="7500">$7,500</option>
																			<option value="10000">$10,000</option>
																			<option value="15000">$15,000</option>
																			<option value="20000">$20,000</option>
																		</select>
																	</div>-
																	<div  class="selected-box col 5">
																		<select name="minprice">
																			<option value="0" selected>Any</option>
																			<option value="799">$799</option>
																			<option value="1000">$1,000</option>
																			<option value="1500">$1,500</option>
																			<option value="2000">$2,000</option>
																			<option value="3000">$3,000</option>
																			<option value="4000">$4,000</option>
																			<option value="5000">$5,000</option>
																			<option value="7500">$7,500</option>
																			<option value="10000">$10,000</option>
																			<option value="15000">$15,000</option>
																			<option value="20000">$20,000</option>
																		</select>
																	</div>/
																	<div  class="selected-box col 2">
																		<select name="currency">
																			<option value="0" selected>YEN</option>
																			<option value="1" >PKR</option>
																			<option value="2" >USD</option>
																			<option value="3" >EURO</option>
																		</select>
																	</div> -->
																</div>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2" >Tel - Mobile*</label>
																	<div  name="phone" class="selected-box col-3">
																		<select>
																			<option value="0" selected>+81</option>
																			<option value="2019">+92</option>
																			<option value="2018">+11</option>
																			<option value="2017">+42</option>
																			<option value="2016">+44</option>
																			<option value="2015">+1</option>
																			<option value="2014">+15</option>
																			<option value="2013">+154</option>
																			<option value="2012">+455</option>
																			<option value="2011">+3</option>
																			<option value="2010">+35</option>
																			<option value="2009">+26</option>
																		</select>
																	</div>	
																	<div class="col-lg-7">
																		<input id="text" name="phone" class="form-control" type="number"  name="Name">
																	</div>
																</div>
														</div>
													</div>
													
												</div>	
												<div class="row">
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																<label class="col-lg-4" >Vehicle Image *</label>
																	<div class="col-8">
																	<input type="file" name="userfile" class="text-center center-block file-upload">
																	</div>
																</div>
															
														</div>
													</div>
													
													<div class="col-lg-6">
														<div class="price-slide">
															<div class="price">
																<div class="row">
																	<div class="col-4">
																		<div class="remember-checkbox">
																			<input type="checkbox" name="tel_type" value="whatsapp" id="1" />
																			<label for="1"><img src="<?php echo base_url(); ?>assets/front-app-assets/images/whatsapp.png" alt=""style="height: 30px;"></label>
																		</div>
																	</div>
																	<div class="col 4">
																		<div class="remember-checkbox">
																			<input type="checkbox" name="tel_type" value="2" id="viber"/>
																			<label for="2"><img src="<?php echo base_url(); ?>assets/front-app-assets/images/viber.png" alt=""style="height: 30px;"></label>
																		</div>
																	</div>
																	<div class="col-4">
																		<div class="remember-checkbox">
																			<input type="checkbox" name="tel_type" value="line" id="3"/>
																			<label for="3"><img src="<?php echo base_url(); ?>assets/front-app-assets/images/line.png" alt=""style="height: 30px;"></label>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													
												</div>
												<div class="row">
													<div class="col-lg-6">
														<div class="price-slide">
																<div class="row">
																	<label class="col-lg-2">Massage* (0/500) </label>
																	<div class="col-10">
   																		 <textarea class="form-control" name="msg" id="exampleFormControlTextarea1" rows="3"></textarea>
																	</div>
																</div>
														</div>
													</div>
													<div class="col-lg-6">
														<div class="form-group">
															<button class="button red"  Type="submit" name="action">Submit</button>
															<i class="fa fa-refresh fa-spin fa-3x fa-fw load_spiner"  style="display: none;"></i>
														</div>
													</div>	
												</div>
											</div>		
										</div>
										<?php echo form_close()?>
									</div>
								</div>	
						</li>



						<!-- <li>
							<a data-toggle="modal" data-target="#exampleModal4" data-whatever="@mdo" href="#" class="css_btn"><i class="fa fa-envelope"></i>Email to a Friend</a>
							<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="exampleModalLabel">Email to a Friend</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div id="etf_message"></div>
										<div class="modal-body">
											<form class="gray-form reset_css" action="http://themes.potenzaglobalsolutions.com/html/cardealer/post" id="etf_form">
												<input type="hidden" name="action" value="email_to_friend" />
												<div>
													<span style="color: red;" class="sp"></span>
												</div>
												<div class="form-group">
													<label>Name*</label>
													<input name="etf_name" type="text" id="etf_name" class="form-control" >
												</div>
												<div class="form-group">
													<label>Email address*</label>
													<input type="text" class="form-control" id="etf_email" name="etf_email" >
												</div>
												<div class="form-group">
													<label>Friends Email*</label>
													<input type="Email" class="form-control" id="etf_fmail" name="etf_fmail">
												</div>
												<div class="form-group">
													<label>Message to friend*</label>
													<textarea class="form-control input-message" id="etf_fmessage" name="etf_fmessage" rows="4"></textarea>
												</div>
												<div class="form-group">
													<div id="recaptcha4"></div>
												</div>
												<div class="form-group">
													<a class="button red" id="email_to_friend_submit">Submit</a>
													<i class="fa fa-refresh fa-spin fa-3x fa-fw load_spiner"  style="display: none;"></i>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</li> -->
						<!-- <li>
							<a data-toggle="modal" data-target="#exampleModal5" data-whatever="@mdo" href="#" class="css_btn"><i class="fa fa-arrow-circle-o-down"></i>Trade-In Appraisal</a>
							<div class="modal fade bd-example-modal-lg" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="exampleModalLabel">Trade-In Appraisal</h4>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div id="tia_message"></div>
										<div class="modal-body">
											<form class="gray-form reset_css" action="http://themes.potenzaglobalsolutions.com/html/cardealer/post" id="tia_form">
												<div class="row">
													<div class="col-md-6">
														<input type="hidden" name="action" value="trade_in_appraisal" />
														<div class="row">
															<div class="col-md-12">
																<h6>Contact Information </h6>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>First Name*</label>
																	<input type="text" class="form-control" name="tia_firstname" id="tia_firstname">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Last Name *</label>
																	<input type="text" class="form-control" name="tia_lastname" id="tia_lastname">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Work Phone</label>
																	<input type="Phone" class="form-control" name="tia_workphone" id="tia_workphone">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Phone*</label>
																	<input type="Phone" class="form-control" name="tia_phone" id="tia_phone">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Email*</label>
																	<input type="Email" class="form-control" name="tia_email" id="tia_email">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Preferred Contact*</label>
																	<div class="radio">
																		<label><input type="radio" name="tia_optradio" id="tia_optradio" value="email" checked>Email</label>
																	</div>
																	<div class="radio">
																		<label><input type="radio" name="tia_optradio" id="tia_optradio" value="phone">Phone</label>
																	</div>
																</div>
															</div>
															<div class="col-md-12">
																<div class="form-group">
																	<label>Comments*</label>
																	<textarea class="form-control input-message" rows="4" name="tia_comments" id="tia_comments"></textarea>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<h6>Options</h6>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Navigation">Navigation</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Sunroof">Sunroof</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Leather">Leather</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning">Air conditioning</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning"> Power Windows</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning"> Power Locks</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning"> Power Seats</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning"> Cruise Control</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning"> Cassette</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning"> DVD Player</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning">  Alloy Wheels</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning">  Satellite Radio</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning">  CD Player / Changer</label>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="checkbox">
																		<label><input type="checkbox" name="tia_options[]" value="Air conditioning"> AM/FM Stereo</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<h6>Vehicle Information </h6>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Year*</label>
																	<input type="text" class="form-control" name="tia_year" id="tia_year">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Make*</label>
																	<input type="text" class="form-control" name="tia_make" id="tia_make">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Model*</label>
																	<input type="text" class="form-control" name="tia_model" id="tia_model">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Exterior Colour*</label>
																	<input type="text" class="form-control" name="tia_colour" id="tia_colour">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>VIN*</label>
																	<input type="text" class="form-control" name="tia_vin" id="tia_vin">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Kilometres *</label>
																	<input type="text" class="form-control" name="tia_kilometers" id="tia_kilometers">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Engine *</label>
																	<input type="text" class="form-control" name="tia_engine" id="tia_engine">
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Doors </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_doors" id="tia_doors">
																			<option value="2">2 </option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Transmission </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_transmission" id="tia_transmission">
																			<option value="Automatic">Automatic</option>
																			<option value="Manual">Manual</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Drivetrain  </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_drivetrain" id="tia_drivetrain">
																			<option value="AWD">AWD</option>
																			<option value="2WD">2WD</option>
																			<option value="4WD">4WD</option>
																		</select>
																	</div>
																</div>
															</div>
															
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<h6>Vehicle Rating </h6>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Body (dents, dings, rust, rot, damage)   </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehicle_rating_body" id="tia_vehicle_rating_body">
																			<option value="1">1 </option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																			<option value="10">10 best</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Tires (tread wear, mismatched)  </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehicle_rating_tires" id="tia_vehicle_rating_tires">
																			<option value="1">1 </option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																			<option value="10">10 best</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Engine (running condition, burns oil, knocking)  </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehicle_rating_engine" id="tia_vehicle_rating_engine">
																			<option value="1">1 </option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																			<option value="10">10 best</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Transmission / Clutch (slipping, hard shift, grinds)   </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehicle_rating_clutch" id="tia_vehicle_rating_clutch">
																			<option value="1">1 </option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																			<option value="10">10 best</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Glass (chips, cracks, pitted)    </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehicle_rating_glass" id="tia_vehicle_rating_glass">
																			<option value="1">1 </option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																			<option value="10">10 best</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Interior (rips, tears, burns, faded/worn) </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehicle_rating_interior" id="tia_vehicle_rating_interior">
																			<option value="1">1 </option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																			<option value="10">10 best</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>Exhaust (rusted, leaking, noisy) </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehicle_rating_exhaust" id="tia_vehicle_rating_exhaust">
																			<option value="1">1 </option>
																			<option value="2">2</option>
																			<option value="3">3</option>
																			<option value="4">4</option>
																			<option value="5">5</option>
																			<option value="6">6</option>
																			<option value="7">7</option>
																			<option value="8">8</option>
																			<option value="9">9</option>
																			<option value="10">10 best</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<h6>VEHICLE HISTORY </h6>
																<div class="form-group">
																	<label>Was it ever a lease or rental return? </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehical_info_1" id="tia_vehical_info_1">
																			<option value="yes">yes </option>
																			<option value="no">No</option>
																		</select>
																	</div>
																</div>
																<div class="form-group">
																	<label>Is the odometer operational and accurate? </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehical_info_2" id="tia_vehical_info_2">
																			<option value="yes">yes </option>
																			<option value="no">No</option>
																		</select>
																	</div>
																</div>
																<div class="form-group">
																	<label>Detailed service records available?  </label>
																	<div class="selected-box">
																		<select class="selectpicker" name="tia_vehical_info_3" id="tia_vehical_info_3">
																			<option value="yes">yes </option>
																			<option value="no">No</option>
																		</select>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="row">
															<div class="col-md-12">
																<h6>Title History </h6>
																<div class="form-group">
																	<label>Is there a lienholder? </label>
																	<input type="Email" class="form-control" name="tia_lineholder_email" id="tia_lineholder_email">
																</div>
																<div class="form-group">
																	<label>Who holds this title? </label>
																	<input type="Email" class="form-control" name="tia_titleholder_email" id="tia_titleholder_email">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 vehicle-assessment">
														<h6>Vehicle Assessment </h6>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label>Does all equipment and accessories work correctly?</label>
															<textarea class="form-control input-message" rows="4" name="tia_textarea_1" id="tia_textarea1"></textarea>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label>Did you buy the vehicle new? </label>
															<textarea class="form-control input-message" rows="4" name="tia_textarea_2" id="tia_textarea2"></textarea>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label>Has the vehicle ever been in any accidents? </label>
															<textarea class="form-control input-message" rows="4" name="tia_textarea_3" id="tia_textarea3"></textarea>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label>Is there existing damage on the vehicle? Where? </label>
															<textarea class="form-control input-message" rows="4" name="tia_textarea_4" id="tia_textarea4"></textarea>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label>Has the vehicle ever had paint work performed? </label>
															<textarea class="form-control input-message" rows="4" name="tia_textarea_5" id="tia_textarea5"></textarea>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label>Is the title designated 'Salvage' or 'Reconstructed'?   </label>
															<textarea class="form-control input-message" rows="4" name="tia_textarea_6" id="tia_textarea6"></textarea>
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group captcha">
															<div id="recaptcha5"></div>
														</div>
														<div class="form-group">
															<a class="button red" id="trade_in_appraisal_submit">Submit</a>
															<i class="fa fa-refresh fa-spin fa-3x fa-fw load_spiner"  style="display: none;"></i>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</li> -->
						<!-- <li><a href="javascript:window.print()"><i class="fa fa-print"></i>Print this page</a></li> -->
						<!-- <li><a href="<?php echo base_url(); ?>users/orders" class="button red">Your Orders</a></li> -->
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="slider-slick">
					<div class="slider slider-for detail-big-car-gallery"> 
						<?php foreach($stock_pictures as $stock_picture): ?>
						<img class="img-fluid" src="<?php echo base_url(); ?>assets/uploads/<?php echo $stock_picture['stock_picture_filename']; ?>" alt="">
						<?php endforeach;?>
					</div>
					<div class="slider slider-nav"> 
					<?php foreach($stock_pictures as $stock_picture): ?>
						<img class="img-fluid" src="<?php echo base_url(); ?>assets/uploads/<?php echo $stock_picture['stock_picture_filename']; ?>" alt="">
						<?php endforeach;?>
					</div>
				</div>
				<div class="tabcontent">
					<h6>Specials</h6>
					<div class="row">
						<div class=col-6>
							<table class="table">
								<thead class="thead-light" style="border: 1px grey solid;">
									<tr>
										<th>Model Code</th>
										<td><?php echo $stock['model_code']; ?></td>
									</tr>
									<tr>
										<th>Chassis</th>
										<td><?php echo $stock['chassis_number']; ?></td>
									</tr>
									<tr>
										<th>Mileage</th>
										<td><?php echo $stock['mileage']; ?></td>
									</tr>
									<tr>
										<th>Engine Type</th>
										<td><?php echo $stock['engine_cc']; ?></td>
									</tr>
									<tr>
										<th>Reg.Year</th>
										<td><?php echo $stock['year']; ?></td>
									</tr>
									<tr>
										<th>Fuel Type</th>
										<td><?php echo $stock['fuel']; ?></td>
									</tr>
									<tr>
										<th>Transmission Type</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>Drive Type</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>Doors</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>No. of Seats</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>Colour</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>Steering</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
								</thead>
							</table>
						</div>
						<div class=col-6>
							<table class="table">
								<thead class="thead-light" style="border: 1px grey solid;">
									<tr>
										<th>Auto Air Conditioning</th>
										<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
									</tr>
									<tr>
										<th>Power Steering</th>
										<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
									</tr>
									<tr>
										<th>Power Windows	</th>
										<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
									</tr>
									<tr>
										<th>ABS</th>
										<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
									</tr>
									<tr>
										<th>Air Bag</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>Power Mirrors</th>
										<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
									</tr>
									<tr>
										<th>Central Locking</th>
										<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
									</tr>
									<tr>
										<th>Keyless Entry</th>
										<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
									</tr>
									<tr>
										<th>Cassette Stereo</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>CD Player</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>CD Changer</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>Sunroof</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
									<tr>
										<th>Leather Seat</th>
										<td><img src="<?php echo base_url(); ?>assets/app-assets/images/ good.png" class="responsive-img" alt="">Good</td>
									</tr>
									<tr>
										<th>HID (Xenon Light)</th>
										<td><?php echo $stock['condition']; ?></td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class=col-6>
						<table class="table">
							<h6>Inspector's Note</h6>
							<h7>Refers to Add.Info</h7>
							<thead class="thead-light" style="border: 1px grey solid;">
								<tr>
									<th>Engine</th>
									<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
								</tr>
								<tr>
									<th>Exhaust Gas</th>
									<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
								</tr>
								<tr>
									<th>Transmission</th>
									<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
								</tr>
								<tr>
									<th>Radiator</th>
									<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
								</tr>
								<tr>
									<th>Interior Smell</th>
									<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
								</tr>
								<tr>
									<th>Windshield</th>
									<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
								</tr>
								<tr>
									<th>Warning Light</th>
									<td><img src="<?php echo base_url(); ?>assets/app-assets/images/good.png" class="responsive-img" alt=""style="height: 20px;margin: 0px 10px 5px 0px;">Good</td>
								</tr>
							</thead>
						</table>
					</div>
					<div class=col-6 style="margin-top: 25px;">
						<table class="table">
							<h5>Equipped Kit</h5>
							<thead class="thead-light" style="border: 1px grey solid;">
								<tr>
									<th>Jack</th>
									<td><?php echo $stock['condition']; ?></td>
								</tr>
								<tr>
									<th>Tool kit</th>
									<td><?php echo $stock['condition']; ?></td>
								</tr>
								<tr>
									<th>Spare Tire/Repair Kit</th>
									<td><?php echo $stock['condition']; ?></td>
								</tr>
								<tr>
									<th>Spare Key</th>
									<td><?php echo $stock['condition']; ?></td>
								</tr>
							</thead>
						</table>
						<table class="table">
							<h5>Useful Information</h5>
							<thead class="thead-light" style="border: 1px grey solid;">
								<tr>
									<th>Type</th>
									<td><?php echo $stock['condition']; ?></td>
								</tr>
								<tr>
									<th>Length-Width-Height</th>
									<td><?php echo $stock['condition']; ?></td>
								</tr>
								<tr>
									<th>Cubic Meter</th>
									<td><?php echo $stock['condition']; ?></td>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<!-- <div class="extra-feature">
					<h6> extra feature</h6>
					<div class="row">
						<div class="col-lg-4">
							<ul class="list-style-1">
								<li><i class="fa fa-check"></i> Security System</li>
								<li><i class="fa fa-check"></i> Air conditioning</li>
								<li><i class="fa fa-check"></i> Alloy Wheels</li>
								<li><i class="fa fa-check"></i> Anti-Lock Brakes (ABS)</li>
								<li><i class="fa fa-check"></i> Anti-Theft</li>
								<li><i class="fa fa-check"></i> Anti-Starter </li>
							</ul>
						</div>
						<div class="col-lg-4">
							<ul class="list-style-1">
								<li><i class="fa fa-check"></i> Security System</li>
								<li><i class="fa fa-check"></i> Air conditioning</li>
								<li><i class="fa fa-check"></i> Alloy Wheels</li>
								<li><i class="fa fa-check"></i> Anti-Lock Brakes (ABS)</li>
								<li><i class="fa fa-check"></i> Anti-Theft</li>
								<li><i class="fa fa-check"></i> Anti-Starter </li>
							</ul>
						</div>
						<div class="col-lg-4">
							<ul class="list-style-1">
								<li><i class="fa fa-check"></i> Security System</li>
								<li><i class="fa fa-check"></i> Air conditioning</li>
								<li><i class="fa fa-check"></i> Alloy Wheels</li>
								<li><i class="fa fa-check"></i> Anti-Lock Brakes (ABS)</li>
								<li><i class="fa fa-check"></i> Anti-Theft</li>
								<li><i class="fa fa-check"></i> Anti-Starter </li>
							</ul>
						</div>
					</div>
				</div> -->
				<div class="feature-car">
				</div>
			</div>
			<div class="col-md-4">
				<div class="car-details-sidebar">
					<div class="details-block details-weight">
						<h5>DESCRIPTION</h5>
						<ul>
							<li> <span>Make</span> <strong class="text-right"><?php echo $stock['maker']; ?></strong></li>
							<li> <span>Model</span> <strong class="text-right"><?php echo $stock['model']; ?></strong></li>
							<li> <span>Registration date </span> <strong class="text-right"><?php echo $stock['year']; ?></strong></li>
							<li> <span>Mileage</span> <strong class="text-right"><?php echo $stock['milage']; ?></strong></li>
							<li> <span>Condition</span> <strong class="text-right"><?php echo $stock['condition']; ?></strong></li>
							<li> <span>Exterior Color</span> <strong class="text-right"><?php echo $stock['color']; ?></strong></li>
							<li> <span>Transmission</span> <strong class="text-right"><?php echo $stock['transmission']; ?></strong></li>
							<li> <span>Engine</span> <strong class="text-right"><?php echo $stock['engine_cc']; ?></strong></li>
							
						</ul>
					</div>
					<!-- <div class="details-social details-weight">
						<h5>Share now</h5>
						<ul>
						  <li><a href="#"> <i class="fa fa-facebook"></i> facebook</a></li>
						  <li><a href="#"> <i class="fa fa-twitter"></i> twitter</a></li>
						  <li><a href="#"> <i class="fa fa-pinterest-p"></i> pinterest</a></li>
						  <li><a href="#"> <i class="fa fa-dribbble"></i> dribbble</a></li>
						  <li><a href="#"><i class="fa fa-linkedin"></i> google plus </a></li>
						  <li><a href="#"> <i class="fa fa-behance"></i> behance</a></li>
						</ul>
						</div> -->
					<div class="details-form contact-2 details-weight">
						<form class="gray-form">
							<h5>SEND ENQUIRY</h5>
							<div class="form-group">
								<label>Name*</label>
								<input type="text" class="form-control" placeholder="Name">
							</div>
							<div class="form-group">
								<label>Email address*</label>
								<input type="text" class="form-control" placeholder="Email">
							</div>
							<div class="form-group">
								<label>password*</label>
								<input type="text" class="form-control" placeholder="Password">
							</div>
							<div class="form-group">
								<label>Comment* </label>
								<textarea class="form-control" rows="4" placeholder="Comment"></textarea>
							</div>
							<div class="form-group">
								<a class="button red" href="#">Request a service</a>
							</div>
						</form>
					</div>
					<div class="details-phone details-weight">
						<div class="feature-box-3">
							<div class="icon">
								<i class="fa fa-headphones"></i>
							</div>
							<div class="content">
								<h4>1-888-345-888 </h4>
								<p>Call our seller to get the best price </p>
							</div>
						</div>
					</div>
					<!-- <div class="details-form contact-2">
						<form class="gray-form">
						 <h5>Financing Calculator</h5>
						 <div class="form-group">
						    <label>Vehicle Price ($)*</label>
						    <input type="text" class="form-control" placeholder="Price">
						 </div>
						  <div class="form-group">
						     <label>Interest rate (%)*</label>
						     <input type="text" class="form-control" placeholder="Rate">
						 </div>
						 <div class="form-group">
						     <label>Period (Month)*</label>
						     <input type="text" class="form-control" placeholder="Month">
						 </div>                  
						 <div class="form-group">
						     <label>Down Payment *</label>
						     <input type="text" class="form-control" placeholder="Payment">
						 </div>
						 <div class="form-group">
						   <a class="button red" href="#">estimate payment</a>
						 </div>
						</form>
						</div> -->
				</div>
			</div>
		</div>
	</div>
</section>
<!--=================================
	car-details -->