<style>
	th,td
	{
	text-align: center;
	border: 2px black solid;
	padding: 8px;
	}
	th
	{
		color: red;
	}
	td
	{
		color: blue;

	}
input
	{
	color: blue;
    text-align: center;
	border: none;
	width: 39%;
	}
	
</style>
<!--=================================
	inner-intro -->
<section class="inner-intro bg-1 bg-overlay-black-70">
	<div class="container">
		<div class="row text-center intro-title">
			<div class="col-md-6 text-md-left d-inline-block">
				<h1 class="text-white">Orders </h1>
			</div>
			<div class="col-md-6 text-md-right float-right">
				<ul class="page-breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
					<li><a href="#">pages</a> <i class="fa fa-angle-double-right"></i></li>
					<li><span>Orders</span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--=================================
	inner-intro -->
<!--=================================
	product-listing  -->
<section class="product-listing page-section-ptb">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-4">
				<div class="listing-sidebar">
					<div class="widget">
						<div class="clearfix">
						</div>
					</div>
					<div class="widget-banner">
						<img class="img-fluid center-block" src="<?php echo base_url(); ?>assets/front-app-assets/images/banner.jpg" alt="">
					</div>
				</div>
			</div>
			
			<div class="col-lg-9 col-md-8 right">
				<div class="sorting-options-main">
					
					<div class="row">
					<div class="car-title">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Order Id</th>
												<th>Order Date</th>
												<th>Maker</th>
												<th>Model</th>
												<th>Order Amount</th>
												<th>Paid Amount</th>
												<th>Action</th>
											</tr>
										</thead>
										<?php
											foreach($orders as $order):
											?>
										<tbody >
											<tr>
												<td><?php echo  $order['orderid']; ?></td>
												<td><?php echo  $order['orderdate']; ?></td>
												<td><?php echo  $order['maker']; ?></td>
												<td><?php echo  $order['model']; ?></td>
												<td><?php echo  $order['sale_price']; ?></td>
												<td><?php echo  $order['orderpaidamount']; ?></td>
												<td><a class="button red" href="<?php echo base_url(); ?>users/viewinvoice/<?php echo  $order['orderid']; ?>">View Invoice</a</td>
											</tr>
										</tbody>
										<?php endforeach; ?>
									</table>
								</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	</div>
</section>
<!--=================================
	product-listing  -->