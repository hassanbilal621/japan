<section class="inner-intro bg-1 bg-overlay-black-70">
   <div class="container">
      <div class="row text-center intro-title">
         <div class="col-sm-6 text-md-left d-inline-block">
            <h1 class="text-white">Testimonials</h1>
         </div>
         <div class="col-sm-6 text-md-right float-right">
            <ul class="page-breadcrumb">
               <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
               <li><a href="#">pages</a> <i class="fa fa-angle-double-right"></i></li>
               <li><span>Testimonials</span> </li>
            </ul>
         </div>
      </div>
   </div>
</section>
<!--=================================
   testimonial -->
<section class="testimonial-3 white-bg page-section-ptb">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="section-title">
               <span>What Our Happy Clients say about us</span>
               <h2>Our Testimonial </h2>
               <div class="separator"></div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="owl-carousel owl-theme" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-space="15">
               <div class="item">
                  <div class="testimonial-block">
                     <div class="row">
                        <div class="col-md-3">
                           <div class="testimonial-avtar">
                              <img class="img-fluid center-block" src="<?php echo base_url(); ?>assets/front-app-assets/images/team/01.jpg" alt="">
                           </div>
                        </div>
                        <div class="col-md-9">
                           <div class="testimonial-content">
                              <p><i class="fa fa-quote-left"></i> <span>You will begin to realize why this exercise is called the Dickens Pattern (with reference to the ghost showing Scrooge some different futures) as you notice that the idea of this exercise is to hypnotize yourself to be aware of two very real possibilities for your future. Two distinct pathways that you could take for your life this very day.</span> <i class="fa fa-quote-right float-right"></i></p>
                           </div>
                           <div class="testimonial-info">
                              <h6>John Doe</h6>
                              <span>Customer</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="item">
                  <div class="testimonial-block">
                     <div class="row">
                        <div class="col-md-3">
                           <div class="testimonial-avtar">
                              <img class="img-fluid center-block" src="<?php echo base_url(); ?>assets/front-app-assets/images/team/02.jpg" alt="">
                           </div>
                        </div>
                        <div class="col-md-9">
                           <div class="testimonial-content">
                              <p><i class="fa fa-quote-left"></i> <span>You will begin to realize why this exercise is called the Dickens Pattern (with reference to the ghost showing Scrooge some different futures) as you notice that the idea of this exercise is to hypnotize yourself to be aware of two very real possibilities for your future. Two distinct pathways that you could take for your life this very day.</span> <i class="fa fa-quote-right float-right"></i></p>
                           </div>
                           <div class="testimonial-info">
                              <h6>Alice Williams</h6>
                              <span>Car Dealer</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="item">
                  <div class="testimonial-block">
                     <div class="row">
                        <div class="col-md-3">
                           <div class="testimonial-avtar">
                              <img class="img-fluid center-block" src="<?php echo base_url(); ?>assets/front-app-assets/images/team/03.jpg" alt="">
                           </div>
                        </div>
                        <div class="col-md-9 ">
                           <div class="testimonial-content">
                              <p><i class="fa fa-quote-left"></i> <span>You will begin to realize why this exercise is called the Dickens Pattern (with reference to the ghost showing Scrooge some different futures) as you notice that the idea of this exercise is to hypnotize yourself to be aware of two very real possibilities for your future. Two distinct pathways that you could take for your life this very day.</span> <i class="fa fa-quote-right float-right"></i></p>
                           </div>
                           <div class="testimonial-info">
                              <h6>Felica Queen</h6>
                              <span>Auto Dealer</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!--=================================
   testimonial -->