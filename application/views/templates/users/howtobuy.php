<section class="inner-intro bg-1 bg-overlay-black-70">
   <div class="container">
      <div class="row text-center intro-title">
         <div class="col-md-6 text-md-left d-inline-block">
            <h1 class="text-white">How to Buy ? </h1>
         </div>
         <div class="col-md-6 text-md-right float-right">
            <ul class="page-breadcrumb">
               <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
               <li><span>How to buy</span> </li>
            </ul>
         </div>
      </div>
   </div>
</section>
<section class="page-both-sidebar page-section-ptb">
      <div class="container-fluid">
         <div class="row">
<!-- right side bar -->
<?php  $this->load->view('templates/users/rightsidebar.php');?> 
  <!-- right side bar end -->




   <div class="col-md-8">
      <div class="row">
         <div class="col-md-12">
            <div class="section-title">
               <span>We’d Love to Hear From You</span>
               <h2>LET'S GET IN TOUCH!</h2>
               <div class="separator"></div>
            </div>
         </div>
      </div>
      <div class="row container">
         <!-- Step-1 Select		S1 -->
         <div class="stepBox" id="S1">
            <h2>Step 1. Select your vehicle.</h2>
            <p class="mb0">
               Choose the vehicle you want to purchase. Our car search engine will help you search through our inventory. You can also customize the search depending on your requirements and preferences. Detailed photos and specifications can be seen for each inventory.<br />
               Car search engine is here: <a href='https://www.Nexcojapan.com/used-cars/'>https://www.Nexcojapan.com/used-cars/</a>            
            </p>
         </div>
         <div class="stepBox" id="S2">
            <h2>Step 2. Get “Free Quote” or “Buy Now”</h2>
            <p style="margin-bottom:10px;">
               Once you have chosen the vehicle, you can choose            
            </p>
            <p style="margin-bottom:8px;">“Free Quote”: <u>For clients who have registered but has no purchase history with Nexco and new clients who are not yet registered.</u></p>
            <p style="margin-left:20px;">
               - For registered clients: you will be required to log in to your account<br />
               - For new clients: you will be asked to enter your details in the separate window for registration.<br />
            </p>
            <p style="margin-bottom:8px;">“Buy Now”: <u>For returning clients who has a purchase history or a money deposit with Nexco.</u></p>
            <p style="margin-left:20px;">- You will be required to log in with your account, AFTER pressing “Buy Now”. Make sure to input correct details such as “destination country” or “port” as the shipping fee will be adjusted accordingly. Once all details are confirmed, you will receive the Proforma Invoice.</p>
            <p>Notes: Your reservation will be reviewed by our sales agents before Proforma Invoice is sent. We will be contacting you via email or phone call, if there are other necessary details that we need or if we see errors in your reservation.</p>
         </div>
         <div class="stepBox" id="S3">
            <h2>Step 3. Make Payment</h2>
            <p>
               <b style="color: #007205;">1. Receive your Proforma Invoice.</b><br/>
               Receive your Proforma Invoice in your email and print it out.<br/>
               <span>
               <b>Warning!</b><br />
               We strongly advise our customers to BEWARE OF FAKE EMAILS about the Payment.<br/>
               Make payment to ONLY INVOICE from our below E-mail address:<br/><b>csd@Nexcojapan.com</b><br />
               </span>
            </p>
            <p class="with_emred" style="margin-bottom: 0px;">
               <b style="color: #007205;">2. Payment</b><br/ >
               Payment is DUE within <em>5 working days</em> after you receive Proforma Invoice.<br/>
               <b>&middot; Bank wire transfer (Telegraphic Transfer)</b><br/>
               All customers should send money only to Nexco CO., LTD. beneficiary accounts in Japan.<br/>
               It will be shown in the proforma invoice, to which one of the following accounts, that you need to pay:            
            </p>
            <p style="margin-bottom: 0px;"><b>Beneficiary bank account (Our bank details)</b></p>
            <div class="dltable_sec howtobuy_table" style="margin-top: -10px;">
               <p>MUFG Bank, Ltd.</p>
               <dl>
                  <dt>Beneficiary</dt>
                  <dd>Nexco CO., LTD.</dd>
               </dl>
               <dl>
                  <dt>Branch</dt>
                  <dd>Yokohama Ekimae Branch</dd>
               </dl>
               <dl>
                  <dt>Swift Code</dt>
                  <dd>BOTKJPJT</dd>
               </dl>
               <dl>
                  <dt>Account Number</dt>
                  <dd>251-0008956xxxxxxx <span class="dltable_sec_parentheses">(seven alphabetic characters as per invoice)</span></dd>
               </dl>
            </div>
            <!-- .dltable_sec -->
            <p style="margin: 10px auto 0px;">or</p>
            <div class="dltable_sec howtobuy_table">
               <p>Mizuho Bank Ltd.</p>
               <dl>
                  <dt>Beneficiary</dt>
                  <dd>Nexco CO., LTD.</dd>
               </dl>
               <dl>
                  <dt>Branch</dt>
                  <dd>Dai5Shuchu Branch</dd>
               </dl>
               <dl>
                  <dt>Swift Code</dt>
                  <dd>MHBKJPJT</dd>
               </dl>
               <dl>
                  <dt>Account Number</dt>
                  <dd>xxxxxxxxxx <span class="dltable_sec_parentheses">(10 numeric digits as per invoice)</span></dd>
               </dl>
            </div>
            <!-- .dltable_sec -->
            <p style="margin: 10px auto 0px;">or</p>
            <span style="font-size:1.3em;">You must use the following bank account when you buy local stock at Durban bond.</span>
            <div class="dltable_sec howtobuy_table">
               <p>Standard Bank</p>
               <dl>
                  <dt>Beneficiary</dt>
                  <dd>MAHASIMHAYA PTY LTD</dd>
               </dl>
               <dl>
                  <dt>Branch</dt>
                  <dd></dd>
               </dl>
               <dl>
                  <dt>Swift Code</dt>
                  <dd>SBZAZAJJ</dd>
               </dl>
               <dl>
                  <dt>Account Number</dt>
                  <dd>090833899</dd>
               </dl>
            </div>
            <!-- .dltable_sec -->
            <p>
               <b>As soon as you complete your payment, please scan your confirmation receipt of payment and send its copy to us by email or fax</b>:<br/>
               &nbsp;csd@Nexcojapan.com<br/>
               or<br/>
               &nbsp;Fax: +81-45-290-9486. <br/>
               We will not begin to process the shipping procedures until your payment is received. Full payment, based on the mutual agreement, must be made for you to receive your car. <br/>
               For further information about shipping and payment, please read our <a href='https://www.Nexcojapan.com/terms'>Terms of Use</a><br/>
               <br/>
               <b style="color: #007205;">3. Cancellation</b><br/>
               In case if the full payment is not confirmed by the due date, your order will be cancelled and your partial payments (if any) will be refunded to you.<br/>
            </p>
         </div>
         <div class="stepBox" id="S4">
            <h2>Step 4: Track Your Shipment</h2>
            <p>
               You will receive the information about booking details of your shipment and other shipping information to your email.<br/>
               Please login to your account page in Nexco and confirm the date of Departure and Arrival for your shipment through “My Account” page on our website.<br/>
               After we ship your car to your port, we will send All necessary original documents to you by DHL carrier.<br/>
               Please be ready OR arrange to receive your car at your Port of Destination.<br/><br/>
               <b>Important Documents - The documents you need to receive for your car:</b><br/>
               <b>Export Certificate:</b><br/>
               The original Export Certificate (Japanese)<br/>
               English Export Certificate (translation is provided by Nexco).<br/>
               <b>Customs Invoice:</b><br/>
               This document is for your local customs department to verify the value of your vehicle.<br/>
               <b>Bill of Lading (BOL):</b><br/>
               You need Bill of Lading (BL) receive your vehicle at the Port of Destination (POD).<br/>
               <a href='https://www.Nexcojapan.com/email_us'><u>Other documents</u></a> may be required depending on your country's regulations. Please our sales representatives for additional documents when they are required.<br/>
            </p>
         </div>
         <div class="stepBox" id="S5">
            <h2>Step 5: Complete Customs Clearance</h2>
            <p>
               Please contact your local Customs office or clearing agent to make clearing procedures, and finally receive your car!
            </p>
         </div>
         <div class="stepBox" id="S7">
            <p style="font-weight: 700;font-size: 14px; line-height: 1.8; text-decoration: underline; color: #fe020e;">Beware of Websites, SNS, E-Mails and Invoices impersonating Nexco</p>
            <p style="margin-bottom: 10px;">It is confirmed that there are websites, SNS, E-Mails and Invoices falsely using Nexco’s name or names deceptively similar to Nexco.</p>
            <p style="margin-bottom: 10px;">Please note that our company is not related to them at all.</p>
            <p style="margin-bottom: 10px;">The website of our company (Nexco) is "<strong>https://www.Nexcojapan.com/</strong>".</p>
            <p style="margin-bottom: 10px;">Please <span style="font-weight: bold;"><a href="#S3">click here</a></span> for E-Mails and invoices.</p>
         </div>
         <div class="stepBox" id="S6">
            <p style="color:red">The Nexco Group, including Nexco CO., LTD., has established a framework for the prevention of anti-social transactions and has been taking initiatives including the confirmation of anti-social affiliations of new business partners and the incorporation of clauses for the elimination of anti-social forces in basic procurement contracts and sales contracts.</p>
         </div>
      </div>
   </div>


          <?php  $this->load->view('templates/users/leftsidebar.php');?> 
          <!-- !-- Left Side Bar -- -->
</div>
</div>
</section>