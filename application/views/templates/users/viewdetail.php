<section class="inner-intro bg-1 bg-overlay-black-70">
	<div class="container">
		<div class="row text-center intro-title">
			<div class="col-md-6 text-md-left d-inline-block">
				<h1 class="text-white">View Detail </h1>
			</div>
			<div class="col-md-6 text-md-right float-right">
				<ul class="page-breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
					<li><a href="#">Order</a> <i class="fa fa-angle-double-right"></i></li>
					<li><span>View Detail</span> </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="product-listing page-section-ptb">
	<div class="container">
		<div class="sorting-options-main">
			<div class="row">
				<div class="col-lg-4 col-md-12">
					<div class="blog-2">
						<div class="blog-image">
							<img class="img-fluid center-block" src="<?php echo base_url(); ?>assets/front-app-assets/images/detail/big/01.jpg" alt="">
							<div class="date">
								<span>aug 17</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-12">
					<div class="car-details">
						<div class="car-title">
							<a href="#">
								<h5>Car Name</h5>
							</a>
							<div class="blog-meta float-right">
								<ul>
									<li><a href="#"> <i class="fa fa-comment"></i><br /> Order Status</a></li>
								</ul>
							</div>
							<div class="row">
								<div class=col-4>
									<table class="table">
										<thead class="thead-light" style="border: 1px grey solid;">
											<tr>
												<th>Model Code</th>
												<td>-</td>
											</tr>
											<tr>
												<th>Chassis</th>
												<td>***</td>
											</tr>
											<tr>
												<th>Mileage</th>
												<td>39,000km</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class=col-4>
									<table class="table">
										<thead class="thead-light" style="border: 1px grey solid;">
											<tr>
												<th>Reg.Year</th>
												<td>2003</td>
											</tr>
											<tr>
												<th>Seatbelts Year</th>
												<td>----</td>
											</tr>
											<tr>
												<th>Fuel Type</th>
												<td>petrol</td>
											</tr>
										</thead>
									</table>
								</div>
								<div class=col-4>
									<table class="table">
										<thead class="thead-light" style="border: 1px grey solid;">
											<tr>
												<th>Transmission Type</th>
												<td>AT</td>
											</tr>
											<tr>
												<th>Drive Type</th>
												<td>4WD</td>
											</tr>
											<tr>
												<th>Doors</th>
												<td>5</td>
											</tr>
										</thead>
									</table>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>