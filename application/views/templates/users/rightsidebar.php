<div class="col-md-2">
   <div class="blog-sidebar">
      <div class="search-block">
            <div class="no-gutter">
               <div class="col-md-12 text-center">
                  <h3 class="title text-white"> <i class="fa fa-search"></i> SEARCH HERE </h3>
               </div>
            </div>
            <!-- <?php echo form_open('users/searchlist'); ?> -->
            <form action="searchlist" method="get">
            <div class="row search-top-2" style="margin-left:0px !important; margin-right:0px !important;">
               <div class="col-md-12">
                  <label>Keywords :</label>
                     <input id="phone" class="form-control" type="text"  name="keyword">
               </div>
               <div class="col-md-12">
                  <span>Select Maker</span>
                  <div class="selected-box">
                        <select class="selectpicker" name="maker">
                           <option>Make </option>
                           <option>BMW</option>
                           <option>Honda </option>
                           <option>Hyundai </option>
                           <option>Nissan </option>
                           <option>Mercedes Benz </option>
                        </select>
                  </div>
               </div>
               <div class="col-md-12">
                  <span>Select Model</span>
                  <div class="selected-box">
                        <select class="selectpicker" name="model">
                           <option>Model</option>
                           <option>3-Series</option>
                           <option>Carrera</option>
                           <option>GT-R</option>
                           <option>Cayenne</option>
                           <option>Mazda6</option>
                           <option>Macan</option>
                        </select>
                  </div>
               </div>
               <div class="col-md-12">
                  <label>Year</label>
         
                  <div class="row">
                     <div class="selected-box col-6">
                        <select name="year">
                        <option value="0" selected>Any</option>
											<option value="2">1970</option>
											<option value="4">1971</option>
											<option value="6">1972</option>
											<option value="7">1973</option>
											<option value="8">1974</option>
											<option value="10">1975</option>
											<option value="15">1976</option>
											<option value="19">1977</option>
											<option value="20">1978</option>
											<option value="30">1979</option>
											<option value="40">1980</option>
                        </select>
                     </div>
                     <div class="selected-box col 6">
                        <select>
                        <option value="0" selected>Any</option>
											<option value="2">2011</option>
											<option value="4">2013</option>
											<option value="6">2013</option>
											<option value="7">2014</option>
											<option value="8">2015</option>
											<option value="10">2016</option>
											<option value="15">2015</option>
											<option value="19">2017</option>
											<option value="20">2018</option>
											<option value="30">2019</option>
											<option value="40">2020</option>
                        </select>
                     </div>
                  </div>
            
               </div>
               <div class="col-md-12">
                  <label>Prices </label>
         
                  <div class="row">
                     <div class="selected-box col-6">
                        <select name="prices">
                        <option value="0" selected>Any</option>
															<option value="799">$799</option>
															<option value="1000">$1,000</option>
															<option value="1500">$1,500</option>
															<option value="2000">$2,000</option>
															<option value="3000">$3,000</option>
															<option value="4000">$4,000</option>
															<option value="5000">$5,000</option>
															<option value="7500">$7,500</option>
															<option value="10000">$10,000</option>
															<option value="15000">$15,000</option>
															<option value="20000">$20,000</option>
                        </select>
                     </div>
                     <div class="selected-box col 6">
                        <select>
                        <option value="0" selected>Any</option>
															<option value="799">$799</option>
															<option value="1000">$1,000</option>
															<option value="1500">$1,500</option>
															<option value="2000">$2,000</option>
															<option value="3000">$3,000</option>
															<option value="4000">$4,000</option>
															<option value="5000">$5,000</option>
															<option value="7500">$7,500</option>
															<option value="10000">$10,000</option>
															<option value="15000">$15,000</option>
															<option value="20000">$20,000</option>
                        </select>
                     </div>
                  </div>
            
               </div>

               <div class="col-md-12">
                  <label>Engine Size </label>
         
                  <div class="row">
                     <div class="selected-box col-6">
                        <select>
                        <option value="0" selected>Any</option>
													<option value="660">660cc</option>
													<option value="1000">1,000cc</option>
													<option value="1300">1,300cc</option>
													<option value="1500">1,500cc</option>
													<option value="2000">2,000cc</option>
													<option value="2500">2,500cc</option>
													<option value="3000">3,000cc</option>
													<option value="4000">4,000cc</option>
                        </select>
                     </div>
                     <div class="selected-box col 6">
                        <select>
                        <option value="0" selected>Any</option>
													<option value="660">660cc</option>
													<option value="1000">1,000cc</option>
													<option value="1300">1,300cc</option>
													<option value="1500">1,500cc</option>
													<option value="2000">2,000cc</option>
													<option value="2500">2,500cc</option>
													<option value="3000">3,000cc</option>
													<option value="4000">4,000cc</option>
                        </select>
                     </div>
                  </div>
            
               </div>


               <div class="col-md-12">
                  <div class="price-slide">
                        <div class="price">
                        <center>
                        <label> <a style="font-size:16px; margin-top: 0px;" class="link" href="#">Advance Search</a> </label>

                           <!-- <label for="amount">Price Range</label>
                                 <input type="text" id="amount" class="amount" value="$50 - $300" />
                                 <div id="slider-range"></div> -->
                                 
                          
                           <button type="submit" class="button" href="#">Search the Vehicle</button>
                           </center>
                        </div>
                  </div>
               </div>
            </div>
            </form>
            <!-- <?php echo form_close(); ?> -->
      </div>
   </div>

   <div class="blog-sidebar">
      <div class="search-block">
            <div class="no-gutter">
               <div class="col-md-12 text-center">
                  <h3 class="title text-white">SEARCH BY MAKER </h3>
               </div>
            </div>
            <div class="row search-top-2" style="margin-left:0px !important; margin-right:0px !important;">
               <ul class="car_makers">
                  <?php foreach($makers as $maker): ?>
                  <li style="list-style-type:none;"><a style="font-size: 19px !important;" href="<?php echo base_url(); ?>users/searchlist?maker=<?php echo $maker['maker_name']?>"><img src="<?php echo base_url(); ?>assets/car-icons/<?php echo $maker['maker_icon']?>" width="36px"> <?php echo $maker['maker_name']?></a></li>
                  <?php endforeach;?>
               </ul>
            </div>
         
      </div>
      <div class="search-block">
            <div class="no-gutter">
               <div class="col-md-12 text-center">
                  <h3 class="title text-white">SEARCH BY Type </h3>
               </div>
            </div>
            <div class="row search-top-2" style="margin-left:0px !important; margin-right:0px !important;">
               <ul class="car_makers">
                  <?php foreach($types as $type): ?>
                  <li style="list-style-type:none;"><a style="font-size: 19px !important;" href="<?php echo base_url(); ?>users/searchlist?maker=<?php echo $type['type_name']?>"><img src="<?php echo base_url(); ?>assets/car-icons/<?php echo $type['type_icon']?>" width="36px"> <?php echo $type['type_name']?></a></li>
                  <?php endforeach;?>
               </ul>
            </div>
         
      </div>
   </div>


   <!-- <div class="blog-sidebar">
      <div class="search-block">
            <div class="no-gutter">
               <div class="col-md-12 text-center">
                  <h3 class="title text-white">LOCATIONS </h3>
               </div>
            </div>
            <div class="row search-top-2" style="margin-left:0px !important; margin-right:0px !important;">

            <div class="widget-link">
               <ul class="api_countries">
                  <?php foreach($api_countries as $api_country): ?>
                     <li><a href="<?php echo base_url() ?>users/searchlist/?country=<?php echo $api_country['name']; ?>" style="font-size: 19px !important;"> <img src="<?php echo $api_country['flag'];?>" width="19px"> <?php echo $api_country['name']; ?> </a></li>
                  <?php endforeach; ?>
               </ul>
            </div>
            </div>
         
      </div>
   </div> -->
</div>





     