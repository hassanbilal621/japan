
<!--=================================
 header -->

 <header id="header" class="topbar-dark">
<div class="topbar">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-5 col-md-12">
        <div class="topbar-left text-lg-left text-center">
           <ul class="list-inline">
              <li style="font-size: 18px;"> <img src="<?php echo base_url(); ?>assets/front-app-assets/images/email.png"style="height: 35px;"> support@website.com</li> 
              <li style="font-size: 18px;"><img src="<?php echo base_url(); ?>assets/front-app-assets/images/whatsapp.png"style="height: 35px;"> +81 80-8856-1417</li>
           </ul>
        </div>
      </div>
      <div class="col-lg-7 col-md-12 text-lg-right text-center">
         <!-- <div class="topbar-profile">
           <ul class="list-inline">
             <li> <a href="#"><i class="fa fa-hand-o-up"></i> Dealer Profile </a>  | </li>
             <li> <a href="#"><i class="fa fa-tag"></i> Seller Profile </a>  </li>
           </ul>
         </div> -->
        <div class="topbar-right">
           <ul class="list-inline">
           <li style="font-size: 18px;"><img src="<?php echo base_url(); ?>assets/front-app-assets/images/c.png"style="height: 35px;"> +81 80-8856-1417</li> 
              <li><img src="<?php echo base_url(); ?>assets/front-app-assets/images/fb.jpg"style="height: 40px;"><a href="#"></a></li>   
           </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<!--=================================
 mega menu -->

<div class="menu">  
  <!-- menu start -->
   <nav id="menu" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items">
     <div class="container-fluid"> 
      <div class="row"> 
       <div class="col-md-12"> 
        <!-- menu logo -->
        <ul class="menu-logo">
            <li>
            <a href="<?php echo base_url(); ?>"><img id="" src="<?php echo base_url(); ?>assets/front-app-assets/images/logo-dark.png" alt="logo"> </a>
            </li>
        </ul>
        <!-- menu links -->
        <ul class="menu-links float-right">
            <li <?php if($this->router->fetch_method() == "search"){echo 'class="active"'; } ?>><a href="<?php echo base_url(); ?>users/searchlist"> search</a> 
                </li>
                <li <?php if($this->router->fetch_method() == "howtobuy"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/howtobuy"> How to buy?</a>
                </li>
                <li <?php if($this->router->fetch_method() == "service"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/service">Services</a>
              </li>
              <li <?php if($this->router->fetch_method() == "contact"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/contact">Contact us</a>
              </li>
                <li><?php if(!$this->session->userdata('necxo_user_id')){ ?>
                    <li <?php if($this->router->fetch_method() == "register"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/register">Register</a></li>
              
                    <li <?php if($this->router->fetch_method() == "login"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/login">Login</a>  </li>
                    <?php }else{ ?>
                      <li <?php if($this->router->fetch_method() == "orders"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/orders">Orders</a></li>
              
                      <li <?php if($this->router->fetch_method() == "profile"){echo 'class="active"'; } ?> ><a href="">My Account <i class="fa fa-angle-down fa-indicator"></i></a>
                      <ul class="drop-down-multilevel right-menu">
                        <li><a href="<?php echo base_url(); ?>users/profile">Profile</a></li>
                        <li><a href="<?php echo base_url(); ?>users/searchlist">Advance Search</a></li>
                        <li><a href="<?php echo base_url(); ?>users/changepassword">Change Password</a></li>
                        <li><a href="<?php echo base_url(); ?>users/logout">Logout</a>  </li>
                    </ul>
                    </li>

                    <?php } ?>
                </li>
        </ul>

       </div>
      </div>
     </div>
    </section>
   </nav>
  <!-- menu end -->
 </div>
</header>

<!--=================================
 header -->
