
<!--=================================
  header -->
  <section class="inner-intro bg-1 bg-overlay-black-70">
  <div class="container">
      <div class="row text-center intro-title">
        <div class="col-md-6 text-md-left d-inline-block">
            <h1 class="text-white">Forgot Password </h1>
        </div>
        <div class="col-md-6 text-md-right float-right">
            <ul class="page-breadcrumb">
              <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
              <li><span>Login</span> </li>
            </ul>
        </div>
      </div>
  </div>
</section>
<!--=================================
  inner-intro -->
<!--=================================
  login-form  -->
<section class="login-form page-section-ptb">
  <div class="container">
  <div class="row">
        <div class="col-md-12">
            <div class="section-title">
              <span>Account recovery </span>
              <h2>Recover your Nexco Account</h2>
              <div class="separator"></div>
            </div>
        </div>
      </div>
  
      <?php echo form_open('users/forgot'); ?>
      <div class="row justify-content-center">
        <div class="col-lg-6 col-md-12">
            <div class="gray-form clearfix">
              <div class="form-group">
                  <label for="name">Enter your email* </label>
                  <input id="name" class="form-control" type="text" placeholder="Email@example.com *****" name="username" required />
              </div>
         
              <div class="form-group">
                  <div class="remember-checkbox mb-4">
                    <a href="<?php echo base_url(); ?>users/forgot" class="float-right">Login now</a>
                  </div>
              </div>
              <button type="submit" class="button red"> Submit </button>
            </div>
        
        </div>
      </div>
      <?php echo form_close(); ?>
  </div>
</section>
<!--=================================
   login-form  -->
