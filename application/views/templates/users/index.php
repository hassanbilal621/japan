
<!-- <section class="slider-parallax car-directory-banner bg-overlay-black-70 bg-17" style="height:700px;">
   <div class="slider-content-middle">
      <div class="container">
      <div class="row">
         <div class="col-md-6">
            <div class="testimonial-center">
               <div class="owl-carousel owl-theme" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-space="30">
               <div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/car/50.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/car/51.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/car/52.jpg" alt="">
							</div>
						</div>
					</div>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="testimonial-center">
               <div class="owl-carousel owl-theme" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-space="30">
               <div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="https://foreignpolicy.com/wp-content/uploads/2019/03/gettyimages-1027569648.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="https://en.dailypakistan.com.pk/digital_images/large/2019-11-29/islamabad-ranked-the-safest-city-in-pakistan-in-world-crime-index-1575015705-3171.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="https://dailytimes.com.pk/assets/uploads/2018/01/12/lahore-uzair.jpg" alt="">
							</div>
						</div>
					</div>
               </div>
            </div>
         </div>
      </div>
         <div class="row">
            <div class="col-md-12" style="margin-top:50px;">
               <div class="slider-content text-center">
                  <h2 class="text-white">Find what are you looking for</h2>               
                  <h4 class="text-white">Over <strong class="text-red">40000</strong>  latest Cars in <strong class="text-red">NEXCO</strong> JAPAN LTD CO</h4>
                  <div class="testimonial-block">
                  <div class="testimonial-content">
                     <i class="fa fa-quote-left"></i>
                     <p>At present, the auto market is dominated by Honda, Toyota and Suzuki. However, on 19 March 2016, Pakistan passed the "Auto Policy 2016-21", which offers tax incentives to new automakers to establish manufacturing plants in the country. ... Pakistan has not enforced any automotive safety standards or model upgrade policies.</p>
                     <i class="fa fa-quote-right"></i>
                  </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

   </div>
</section> -->
<section class="page-both-sidebar page-section-ptb">
	<div class="container-fluid">
		<div class="row">
			<?php  $this->load->view('templates/users/rightsidebar.php');?> 
			<div class="col-md-8">
				<div class="owl-carousel owl-theme" data-nav-arrow="true" data-items="3" data-md-items="3" data-sm-items="2" data-xs-items="1" data-space="10">
					<div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/car/50.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/car/51.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="item content-box-2">
						<div class="car-item gray-bg text-center">
							<div class="car-image">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/front-app-assets/images/car/52.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
<div class="section-title">
         <h2>Recent </h2>
         <div class="separator"></div>
      </div>
            <div class="row"style="padding-bottom: 30px;">
               <?php foreach($stocks as $stock): ?>
               <div class="col-lg-2"> 
                  <div class="car-item gray-bg text-center">
                     <div class="car-image">
                        <?php foreach ($stock_pictures as $stock_picture) : ?>
                        <?php if($stock['stock_id'] == $stock_picture['stock_id_image']) {  ?>
                           <img class="img-fluid" src="<?php echo base_url(); ?>assets/uploads/<?php echo $stock_picture['stock_picture_filename']; ?>" alt="">                        
                        <?php break;  } ?>
                        <?php endforeach; ?>   

                        <div class="car-overlay-banner">
                           <ul>
                              <li><a href="<?php echo base_url(); ?>users/auctionview/<?php echo $stock['stock_id']; ?>"><i class="fa fa-shopping-cart"></i></a></li>
                           </ul>
                        </div>
                     </div>
                     
                     <div class="car-list">
                        <ul class="list-inline">
                           <li><i class="fa fa-shopping-cart"></i><?php echo $stock['milage']; ?><span> (km)</span></li>
                        </ul>
                     </div>
                     <div class="car-content" style="padding: 10px !important;" >
                        <a href="<?php echo base_url(); ?>users/auctionview/<?php echo $stock['stock_id']; ?>" style="margin-bottom: 0px !important;"><?php echo $stock['model']; ?></a>
                        <p style="margin-bottom: 0px !important; margin-top: 0px !important;">Year :  <?php echo $stock['year']; ?></p>
                        <div class="price">
                           <span class="new-price"><?php echo $stock['sale_price']; ?> $</span>
                        </div>
                     </div>
                  </div>
               </div>
               <?php endforeach; ?>
            </div>
            <div class="section-title">
         <h2>New Arrival</h2>
         <div class="separator"></div>
      </div>
            <div class="row"style="padding-bottom: 30px;">
            <?php foreach($stocks as $stock): ?>
               <div class="col-lg-2"> 
                  <div class="car-item gray-bg text-center">
                     <div class="car-image">
                        <?php foreach ($stock_pictures as $stock_picture) : ?>
                        <?php if($stock['stock_id'] == $stock_picture['stock_id_image']) {  ?>
                           <img class="img-fluid" src="<?php echo base_url(); ?>assets/uploads/<?php echo $stock_picture['stock_picture_filename']; ?>" alt="">                        
                        <?php break;  } ?>
                        <?php endforeach; ?>   

                        <div class="car-overlay-banner">
                           <ul>
                              <li><a href="<?php echo base_url(); ?>users/auctionview/<?php echo $stock['stock_id']; ?>"><i class="fa fa-shopping-cart"></i></a></li>
                           </ul>
                        </div>
                     </div>
                     
                     <div class="car-list">
                        <ul class="list-inline">
                           <li><i class="fa fa-shopping-cart"></i><?php echo $stock['milage']; ?><span> (km)</span></li>
                        </ul>
                     </div>
                     <div class="car-content" style="padding: 10px !important;" >
                        <a href="<?php echo base_url(); ?>users/auctionview/<?php echo $stock['stock_id']; ?>" style="margin-bottom: 0px !important;"><?php echo $stock['model']; ?></a>
                        <p style="margin-bottom: 0px !important; margin-top: 0px !important;">Year :  <?php echo $stock['year']; ?></p>
                        <div class="price">
                           <span class="new-price"><?php echo $stock['sale_price']; ?> $</span>
                        </div>
                     </div>
                  </div>
               </div>
               <?php endforeach; ?>
               </div>
         </div>
         <?php  $this->load->view('templates/users/leftsidebar.php');?> 
      </div>
   </div>
</section>





<!--=================================
   custom block -->
<!--=================================
   latest news -->
<!--=================================
   latest news -->
<!--=================================
   play-video -->
<!--=================================
   play-video -->
<!--=================================
   Counter -->
<!--=================================
   Counter -->
<hr class="gray">
<!--=================================
   testimonial -->
<!--=================================
   testimonial -->