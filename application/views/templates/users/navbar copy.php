

<!--=================================
  loading -->
  
  <div id="loading">
  <div id="loading-center">
      <img src="<?php echo base_url(); ?>assets/front-app-assets/images/loader3.gif" alt="">
 </div>
</div>

<!--=================================
  loading -->


<!--=================================
 header -->

 <header id="header" class="fancy">
      <div class="topbar">

      <div class="row">
        <div class="col-lg-6 col-md-12">
          <div class="topbar-left text-lg-left text-center">
            <ul class="list-inline">
              <li style="font-size: 18px;"> <img src="<?php echo base_url(); ?>assets/front-app-assets/images/email.png"style="height: 35px;"> support@website.com</li> 
              <li style="font-size: 18px;"><img src="<?php echo base_url(); ?>assets/front-app-assets/images/whatsapp.png"style="height: 35px;"> +81 80-8856-1417</li>
              </ul>
          </div>
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="topbar-right text-lg-right text-center">
            <ul class="list-inline">
              <li style="font-size: 18px;"><img src="<?php echo base_url(); ?>assets/front-app-assets/images/c.png"style="height: 35px;"> +81 80-8856-1417</li> 
              <li><img src="<?php echo base_url(); ?>assets/front-app-assets/images/fb.jpg"style="height: 40px;"><a href="#"></a></li>      
            </ul>
          </div>
        </div>
      </div>
      </div>


<!--=================================
 mega menu -->

 <div class="menu">  
 
    <div class="row"> 
     <div class="col-md-12"> 
     <!-- menu start -->
     <nav id="menu" class="mega-menu">
        <!-- menu list items container -->
        <section class="menu-list-items">
            <!-- menu logo -->
            <ul class="menu-logo">
            <li>
              <a href="<?php echo base_url(); ?>"><img id="" src="<?php echo base_url(); ?>assets/front-app-assets/images/logo-dark.png" alt="logo"> </a>
            </li>
        </ul>
        <!-- menu links -->
        <ul class="menu-links float-right">
            <!-- active class -->
            <li <?php if($this->router->fetch_method() == "search"){echo 'class="active"'; } ?>><a href="<?php echo base_url(); ?>users/searchlist"> search</a> 
            </li>
            <li <?php if($this->router->fetch_method() == "howtobuy"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/howtobuy"> How to buy?</a>
            </li>
            <li <?php if($this->router->fetch_method() == "service"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/service">Services</a>
          </li>
          <li <?php if($this->router->fetch_method() == "contact"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/contact">Contact us</a>
          </li>
            <li><?php if(!$this->session->userdata('necxo_user_id')){ ?>
                <li <?php if($this->router->fetch_method() == "register"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/register">Register</a></li>
          
                <li <?php if($this->router->fetch_method() == "login"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/login">Login</a>  </li>
                <?php }else{ ?>
                  <li <?php if($this->router->fetch_method() == "orders"){echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>users/orders">Orders</a></li>
          
                  <li <?php if($this->router->fetch_method() == "profile"){echo 'class="active"'; } ?> ><a href="">My Account <i class="fa fa-angle-down fa-indicator"></i></a>
                  <ul class="drop-down-multilevel right-menu">
                    <li><a href="<?php echo base_url(); ?>users/profile">Profile</a></li>
                    <li><a href="<?php echo base_url(); ?>users/searchlist">Advance Search</a></li>
                    <li><a href="<?php echo base_url(); ?>users/changepassword">Change Password</a></li>
                    <li><a href="<?php echo base_url(); ?>users/logout">Logout</a>  </li>
                </ul>
                </li>

                <?php } ?>
            </li>
        </ul>
    </section>
   </nav>
   </div>
   </div>
   </div>
  <!-- menu end -->

</header>
<!--=================================
 header -->