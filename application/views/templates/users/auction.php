<!--=================================
  inner-intro -->
  <section class="slider-parallax car-directory-banner bg-overlay-black-70 bg-17">
   <div class="slider-content-middle">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="slider-content text-center">
               <h2 class="text-white">Find what are you looking for</h2>
               <h4 class="text-white">Over <strong class="text-red">40000</strong>  latest Cars in <strong class="text-red">NEXCO</strong> JAPAN LTD CO</h4>
               <div class="search-tab ">
                  <div id="tabs">
                     <h6>I want to Buy </h6>
                     <ul class="tabs text-left">
                        <li data-tabs="tab1" class="active">  All cars</li>
                        <li data-tabs="tab2"> New Cars </li>
                        <li data-tabs="tab3"> Used Cars</li>
                     </ul>
                     <div class="car-total float-right">
                        <h5 class="text-white"><i class="fa fa-caret-right"></i>(50) <span class="text-red">CARS</span></h5>
                     </div>
                     <?php echo form_open('users/auction'); ?>
                     <div id="tab1" class="tabcontent">
                        <div class="row">
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker" name="maker">
                                    <option value="maker">Maker</option>
                                    <?php foreach($makers as $maker): ?>
                                    <option value="<?php echo $maker['maker_name']; ?>"><?php echo $maker['maker_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker" name="model">
                                    <option>Model</option>
                                    <?php foreach($models as $model): ?>
                                    <option value="<?php echo $model['model_name']; ?>"><?php echo $model['model_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6" name="maxprice">
                              <div class="selected-box">
                                 <select class="selectpicker" name="maker">
                                    <option value="maker">Fuel</option>
                                    <?php foreach($makers as $maker): ?>
                                    <option value="<?php echo $maker['maker_name']; ?>"><?php echo $maker['maker_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="form-group">
                                 <input id="name" type="text" placeholder="Location*" class="form-control placeholder" name="location">
                                 <input id="name" type="hidden" name="indexsearch" value="true">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Transmission </option>
                                    <option>BMW</option>
                                    <option>Honda </option>
                                    <option>Hyundai </option>
                                    <option>Nissan </option>
                                    <option>Mercedes Benz </option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Min Price</option>
                                    <option>3-Series</option>
                                    <option>Carrera</option>
                                    <option>GT-R</option>
                                    <option>Cayenne</option>
                                    <option>Mazda6</option>
                                    <option>Macan</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Max Price </option>
                                    <option>$5,000</option>
                                    <option>$10,000</option>
                                    <option>$15,000</option>
                                    <option>$20,000</option>
                                    <option>$25,000</option>
                                    <option>$30,000</option>
                                    <option>$35,000</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-12">
                              <div class="text-center">
                                 <button class="button btn-block red" type="button">Search</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close();?>
                     <div id="tab2" class="tabcontent">
                     <div class="row">
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker" name="maker">
                                    <option value="maker">Maker</option>
                                    <?php foreach($makers as $maker): ?>
                                    <option value="<?php echo $maker['maker_name']; ?>"><?php echo $maker['maker_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker" name="model">
                                    <option>Model</option>
                                    <?php foreach($models as $model): ?>
                                    <option value="<?php echo $model['model_name']; ?>"><?php echo $model['model_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6" name="maxprice">
                              <div class="selected-box">
                                 <select class="selectpicker" name="maker">
                                    <option value="maker">Fuel</option>
                                    <?php foreach($makers as $maker): ?>
                                    <option value="<?php echo $maker['maker_name']; ?>"><?php echo $maker['maker_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="form-group">
                                 <input id="name" type="text" placeholder="Location*" class="form-control placeholder" name="location">
                                 <input id="name" type="hidden" name="indexsearch" value="true">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Transmission </option>
                                    <option>BMW</option>
                                    <option>Honda </option>
                                    <option>Hyundai </option>
                                    <option>Nissan </option>
                                    <option>Mercedes Benz </option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Min Price</option>
                                    <option>3-Series</option>
                                    <option>Carrera</option>
                                    <option>GT-R</option>
                                    <option>Cayenne</option>
                                    <option>Mazda6</option>
                                    <option>Macan</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Max Price </option>
                                    <option>$5,000</option>
                                    <option>$10,000</option>
                                    <option>$15,000</option>
                                    <option>$20,000</option>
                                    <option>$25,000</option>
                                    <option>$30,000</option>
                                    <option>$35,000</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-12">
                              <div class="text-center">
                                 <button class="button btn-block red" type="button">Search</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="tab3" class="tabcontent">
                     <div class="row">
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker" name="maker">
                                    <option value="maker">Maker</option>
                                    <?php foreach($makers as $maker): ?>
                                    <option value="<?php echo $maker['maker_name']; ?>"><?php echo $maker['maker_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker" name="model">
                                    <option>Model</option>
                                    <?php foreach($models as $model): ?>
                                    <option value="<?php echo $model['model_name']; ?>"><?php echo $model['model_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6" name="maxprice">
                              <div class="selected-box">
                                 <select class="selectpicker" name="maker">
                                    <option value="maker">Fuel</option>
                                    <?php foreach($makers as $maker): ?>
                                    <option value="<?php echo $maker['maker_name']; ?>"><?php echo $maker['maker_name']; ?> </option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="form-group">
                                 <input id="name" type="text" placeholder="Location*" class="form-control placeholder" name="location">
                                 <input id="name" type="hidden" name="indexsearch" value="true">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Transmission </option>
                                    <option>BMW</option>
                                    <option>Honda </option>
                                    <option>Hyundai </option>
                                    <option>Nissan </option>
                                    <option>Mercedes Benz </option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Min Price</option>
                                    <option>3-Series</option>
                                    <option>Carrera</option>
                                    <option>GT-R</option>
                                    <option>Cayenne</option>
                                    <option>Mazda6</option>
                                    <option>Macan</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-6">
                              <div class="selected-box">
                                 <select class="selectpicker">
                                    <option>Max Price </option>
                                    <option>$5,000</option>
                                    <option>$10,000</option>
                                    <option>$15,000</option>
                                    <option>$20,000</option>
                                    <option>$25,000</option>
                                    <option>$30,000</option>
                                    <option>$35,000</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-3 col-md-12">
                              <div class="text-center">
                                 <button class="button btn-block red" type="button">Search</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<!--=================================
  inner-intro -->
<!--=================================
  product-listing  -->
<section class="product-listing page-section-ptb">
  <div class="container">
  <div class="row">
      <div class="col-lg-3 col-md-4">
        <div class="listing-sidebar">
            <div class="widget">
              <div class="widget-search">
                  <h5>Advanced Search</h5>
                  <ul class="list-style
                    -none">
                    <li><i class="fa fa-star"> </i> Results Found <span class="float-right">(39)</span></li>
                    <li><i class="fa fa-shopping-cart"> </i> Compare Vehicles <span class="float-right">(10)</span></li>
                  </ul>
              </div>
              <div class="clearfix">
                  <ul class="list-group">
                    <li class="list-group-item">
                        <h5>Model</h5>
                        <ul style="display: block;">
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox" id="allmodels"> All Models
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> 3-Series
                              </label>
                              </span>
                          </li> 
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> Boxster
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> Carrera
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> Cayenne
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> F-type
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> GT-R
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> GTS
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> M6
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> Macan
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> Mazda6
                              </label>
                              </span>
                          </li>
                          <li><span class="checkbox">
                              <label>
                              <input type="checkbox"> RLX
                              </label>
                              </span>
                          </li>
                        </ul>
                    </li>
                  </ul>
                  </li>
                  </ul>
              </div>
            </div>
            <div class="widget-banner">
              <img class="img-fluid center-block" src="<?php echo base_url(); ?>assets/front-app-assets/images/banner.jpg" alt="">
            </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-8">
        <div class="sorting-options-main">
            <div class="row" id="fetchstock">
             
            </div>
            <!-- <div class="pagination-nav d-flex justify-content-center">
              <ul class="pagination">
                  <li class="active"><a class="loadmore" id="1" value="5">Load More</a></li>
              </ul>
            </div> -->
        </div>
      </div>
  </div>
</section>
<!--=================================
  product-listing  -->

  <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

  <script>

      $(document).ready(function() {
          //set initial state.

          $('#allmodels').change(function() {
              if(this.checked) {
               //   alert("checked");
              }
              if(!this.checked) {
               //   alert("unchecked");
              }
          });
      });

  </script>
  <script type='text/javascript'>

        var loadmorevalue = 0;

        $('.loadmore').click(function(){

          loadmorevalue = loadmorevalue + 5;
          // alert(loadmorevalue);
          $.ajax({
                type: "GET",
                url: "<?php echo base_url();?>users/ajax_get_stock?noofstock="+loadmorevalue,
                data:'country_name=pakistan',
                success: function(data){
                    $("#fetchstock").html(data);
                }
            });
          // $.ajax({
          //   type: "GET",
          //   url: "<?php echo base_url();?>users/ajax_get_stock?noofstock="loadmorevalue,
          //   data:'country_name=pakistan',
          //   success: function(data){
          //       $("#fetchstock").html(data);
          //   }
          // });

          });
  </script>
   <?php if($this->input->post('indexsearch')){ ?>}
   <script type='text/javascript'>
      $(document).ready(function(){
         
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>users/ajax_get_stock",
               data:'country_name=pakistan&name=india',
               success: function(data){
                  $("#fetchstock").html(data);
               }
            });

      });
   </script>
   <?php }else{ ?>

      <script type='text/javascript'>
      $(document).ready(function(){
         
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>users/ajax_get_stock",
               data:'country_name=pakistan',
               success: function(data){
                  $("#fetchstock").html(data);
               }
            });

      });
   </script>
   <?php } ?>}