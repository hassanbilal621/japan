
<!--=================================
 inner-intro -->
 
 <section class="inner-intro bg-1 bg-overlay-black-70">
  <div class="container">
     <div class="row text-center intro-title">
           <div class="col-md-6 text-md-left d-inline-block">
             <h1 class="text-white">Services </h1>
           </div>
           <div class="col-md-6 text-right float-right">
             <ul class="page-breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                <li><span>Services</span> </li>
             </ul>
           </div>
     </div>
  </div>
</section>

<!--=================================
 inner-intro -->


<!--=================================
 service -->

<section class="inner-service white-bg page-section-ptb">
  <div class="container">
   <div class="row no-gutter">
      <div class="col-md-4">
        <div class="feature-box-2 text-center">
          <div class="icon">
           <i class="glyph-icon flaticon-car"></i>
          </div> 
          <div class="content">
          <h5>SUPER FAST</h5>
          <p>Galley of bled it lorem Ipsum is simply dummy text of the k a to make a type book. but also the leap into electronic typesetting.</p>
         </div>
        </div>
        </div>
        <div class="col-md-4">
         <div class="feature-box-2 text-center">
          <div class="icon">
           <i class="glyph-icon flaticon-wallet"></i>
          </div> 
          <div class="content">
          <h5>AFFORDABLE</h5>
          <p>Bled it to make a lorem Ipsum is simply dummy text of the k a galley of type book. but also the leap into electronic typesetting.</p>
         </div>
        </div>
        </div>
        <div class="col-md-4">
         <div class="feature-box-2 no-br text-center">
          <div class="icon">
           <i class="glyph-icon flaticon-gas-station"></i>
          </div> 
          <div class="content">
          <h5>OIL CHANGES</h5>
          <p>Also the leap into electronic typesetting. lorem Ipsum is simply dummy text of the k a galley of bled it to make a type book but </p>
         </div>
        </div>
       </div>
      </div>
      <div class="row no-gutter">
      <div class="col-md-4">
        <div class="feature-box-2 no-bb text-center">
          <div class="icon">
           <i class="glyph-icon flaticon-air-conditioning"></i>
          </div> 
          <div class="content">
          <h5>AIR conditioning</h5>
          <p>Simply dummy text lorem Ipsum is of the k a galley of bled it to make a type book. but also the leap into electronic typesetting.</p>
         </div>
        </div>
        </div>
        <div class="col-md-4">
         <div class="feature-box-2 no-bb text-center">
          <div class="icon">
           <i class="glyph-icon flaticon-gearstick"></i>
          </div> 
          <div class="content">
          <h5>transmission</h5>
          <p>But also the leap into electronic typesetting. lorem Ipsum is simply dummy text of the k a galley of bled it to make a type book. </p>
         </div>
        </div>
        </div>
        <div class="col-md-4">
         <div class="feature-box-2 no-br no-bb text-center">
          <div class="icon">
           <i class="glyph-icon flaticon-key"></i>
          </div> 
          <div class="content">
          <h5>DEALERSHIP</h5>
          <p>Make a type book lorem Ipsum is simply dummy text of the k a galley of bled it to. but also the leap into electronic typesetting.</p>
         </div>
        </div>
       </div>
      </div>
   </div>
</section>

<!--=================================
 service -->


 <!--=================================
 Counter -->

<section class="counter counter-style-2 bg-red bg-1 bg-overlay-black-70 page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-lg-3 col-md-6 item text-left">
      <div class="counter-block"> 
       <div class="separator"></div>
        <div class="info">
           <h6 class="text-white">Vehicles in Stock</h6>
           <i class="glyph-icon flaticon-beetle"></i>
           <b class="timer text-white" data-to="561" data-speed="10000"></b>
          </div> 
        </div>
     </div>
     <div class="col-lg-3 col-md-6 item text-left">
      <div class="counter-block"> 
       <div class="separator"></div>
        <div class="info">
           <h6 class="text-white">Dealer Reviews</h6>
           <i class="glyph-icon flaticon-interface"></i>
           <b class="timer text-white" data-to="856" data-speed="10000"></b>
          </div> 
        </div>
     </div>
     <div class="col-lg-3 col-md-6 item text-left">
      <div class="counter-block"> 
       <div class="separator"></div>
        <div class="info">
           <h6 class="text-white">Happy Customer</h6>
           <i class="glyph-icon flaticon-circle"></i>
           <b class="timer text-white" data-to="789" data-speed="10000"></b>
          </div> 
        </div>
     </div>
      <div class="col-lg-3 col-md-6 item text-left">
      <div class="counter-block"> 
       <div class="separator"></div>
        <div class="info">
           <h6 class="text-white">Awards</h6>
           <i class="glyph-icon flaticon-cup"></i>
           <b class="timer text-white" data-to="356" data-speed="10000"></b>
          </div> 
        </div>
     </div>
    </div>
  </div>
</section>

 <!--=================================
 Counter -->


 <!--=================================
 service-center -->

<section class="service-center white-bg page-section-ptb">
 <div class="container">
   <div class="row">
     <div class="col-md-6">
        <h5>We provide best services processus dynamicus qui sequitur mutationem co mutationem.</h5>          
        <p>Dynamicus qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram.dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram.dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
        <ul class="list-style-1">
          <li><i class="fa fa-check"></i> Dynamicus, qui sequitur mutationem consuetudium lectorum.</li>
          <li><i class="fa fa-check"></i> Sequitur dynamicus, qui mutationem consuetudium lectorum.</li>
          <li><i class="fa fa-check"></i> Mutationem dynamicus, qui sequitur consuetudium lectorum.</li>
          <li><i class="fa fa-check"></i> Consuetudium dynamicus, qui sequitur mutationem lectorum.</li>
        </ul>
    </div>
    <div class="col-md-6">
      <img class="img-fluid center-block" src="<?php echo base_url(); ?>assets/front-app-assets/images/car/22.jpg" alt="">
    </div>
   </div>
  </div>
</section>

<!--=================================
 service-center -->

<hr class="gray" />

<!--=================================
 service schedule -->

<section class="service-schedule white-bg page-section-ptb">
 <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="section-title">
         <span>This is what we do and we do it perfectly</span>
         <h2>service schedule </h2>
         <div class="separator"></div>
      </div>
    </div>
   </div>
   <div class="row">
    <div class="col-md-8">
      <div class="gray-form">
         <div id="formmessage">Success/Error Message Goes Here</div>
           <form class="form-horizontal" id="contactform" role="form" method="post" action="http://themes.potenzaglobalsolutions.com/html/cardealer/php/contact-form.php">
            <div class="contact-form row">
              <div class="col-lg-4 col-sm-12">
               <div class="form-group">
                 <input id="name" type="text" placeholder="Name*" class="form-control"  name="name">
               </div> 
             </div> 
             <div class="col-lg-4 col-sm-12">
               <div class="form-group">
                 <input type="email" placeholder="Email*" class="form-control" name="email">
                </div>
              </div>
              <div class="col-lg-4 col-sm-12">
                <div class="form-group">
                  <input type="text" placeholder="Phone*" class="form-control" name="phone">
                </div>
              </div>
              <div class="col-md-12">
                 <div class="form-group">
                   <textarea class="form-control input-message" placeholder="Comment*" rows="7" name="message"></textarea>
                 </div>
              </div>
              <div class="col-md-12">
                 <input type="hidden" name="action" value="sendEmail"/>
                 <button id="submit" name="submit" type="submit" value="Send" class="button red"> Send your message </button>
               </div>
              </div>
          </form>
          <div id="ajaxloader" style="display:none"><img class="center-block" src="<?php echo base_url(); ?>assets/front-app-assets/images/ajax-loader.gif" alt=""></div> 
       </div>
    </div>
    <div class="col-md-4">
      <div class="opening-hours gray-bg">
        <h6>opening hours</h6>
        <ul class="list-style-none">
          <li><strong>Sunday</strong> <span class="text-red"> closed</span></li>
          <li><strong>Monday</strong> <span> 9:00 AM to 9:00 PM</span></li>
          <li><strong>Tuesday </strong> <span> 9:00 AM to 9:00 PM</span></li>
          <li><strong>Wednesday </strong> <span> 9:00 AM to 9:00 PM</span></li>
          <li><strong>Thursday </strong> <span> 9:00 AM to 9:00 PM</span></li>
          <li><strong>Friday </strong> <span> 9:00 AM to 9:00 PM</span></li>
          <li><strong>Saturday </strong> <span> 9:00 AM to 9:00 PM</span></li>
        </ul>
      </div>
     </div>
    </div>
  </div>
</section>

<!--=================================
service schedule -->