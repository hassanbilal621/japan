<style>
	th,td
	{
	text-align: center;
	border: 2px black solid;
	padding: 8px;
	}
	th
	{
	color: red;
	}
	td
	{
	color: blue;
	}
	input
	{
	color: blue;
	text-align: center;
	border: none;
	width: 39%;
	}
</style>
<!-- <section class="inner-intro bg-1 bg-overlay-black-70">
	<div class="container">
		<div class="row text-center intro-title">
			<div class="col-md-6 text-md-left d-inline-block">
				<h1 class="text-white">Detail Search </h1>
			</div>
			<div class="col-md-6 text-md-right float-right">
				<ul class="page-breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
					<li><span>Detail Search</span> </li>
				</ul>
			</div>
		</div>
	</div>
</section> -->
<section class="page-both-sidebar page-section-ptb">
	<div class="container-fluid">
	<div class="row">
	<!-- right side bar -->
	<?php  $this->load->view('templates/users/rightsidebar.php');?> 
	<!-- right side bar end -->
	<div class="col-md-8">
		<div class="row justify-content-center">
			<div >
				<div class="row">
					<div>
						<div class="sorting-options-main">
							<div class="form-group">
								<?php if(isset($_GET['country'])){ ?> 
								<!-- <div class="row">
									<div class="col-md-3">
									   <div class="testimonial-avtar">
									   <img class="img-fluid center-block border" src="<?php echo $api_country[0]['flag']; ?>" alt="">
									   </div>   
									</div>  
									<div class="col-md-9">
									
									
									<p><b>Country Name : </b> <span> <?php echo $api_country[0]['name']; ?> </span></p>
									<p><b>Capital : </b> <span>  <?php echo $api_country[0]['capital']; ?> </span></p>
									<p><b>Region : </b> <span>  <?php echo $api_country[0]['region']; ?> </span></p>
									<p><b>Sub Region : </b> <span>  <?php echo $api_country[0]['subregion']; ?> </span></p>
									<p><b>Short Name : </b> <span>  <?php echo $api_country[0]['cioc']; ?> </span></p>
									<p><b>Demonym : </b> <span>  <?php echo $api_country[0]['demonym']; ?> </span></p>
									
									
									</div>
									</div> -->
								<?php } ?>
								<label>Keywords :</label>
								<input id="phone" class="form-control" type="text"  name="keyword">
							</div>
							<div class="row">
								<div class="col-lg-3">
									<div class="price-slide">
										<div class="price">
											<div class="selected-box">
												<select>
													<option>Type  </option>
													<?php foreach($types as $type): ?>
													<option><?php echo $type['type_name']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
											<div class="selected-box">
												<select>
													<option>Maker  </option>
													<?php foreach($makers as $maker): ?>
													<option><?php echo $maker['maker_name']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
											<div class="selected-box">
												<select>
													<option>Model  </option>
													<?php foreach($models as $model): ?>
													<option><?php echo $model['model_name']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
											<label>Month</label>
											<div class="row">
												<div class="selected-box col-6">
													<select>
														<option value="0">Any</option>
														<option value="1">Jan</option>
														<option value="2">Feb</option>
														<option value="3">Mar</option>
														<option value="4">Apr</option>
														<option value="5">May</option>
														<option value="6">Jun</option>
														<option value="7">Jul</option>
														<option value="8">Aug</option>
														<option value="9">Sep</option>
														<option value="10">Oct</option>
														<option value="11">Nov</option>
														<option value="12">Dec</option>
													</select>
												</div>
												<div class="selected-box col 6">
													<select>
														<option value="0">Any</option>
														<option value="1">Jan</option>
														<option value="2">Feb</option>
														<option value="3">Mar</option>
														<option value="4">Apr</option>
														<option value="5">May</option>
														<option value="6">Jun</option>
														<option value="7">Jul</option>
														<option value="8">Aug</option>
														<option value="9">Sep</option>
														<option value="10">Oct</option>
														<option value="11">Nov</option>
														<option value="12">Dec</option>
													</select>
												</div>
											</div>
											<label>Year</label>
											<div class="row">
												<div class="selected-box col-6">
													<select>
														<option value="0" selected>Any</option>
														<option value="2019">2019</option>
														<option value="2018">2018</option>
														<option value="2017">2017</option>
														<option value="2016">2016</option>
														<option value="2015">2015</option>
														<option value="2014">2014</option>
														<option value="2013">2013</option>
														<option value="2012">2012</option>
														<option value="2011">2011</option>
														<option value="2010">2010</option>
														<option value="2009">2009</option>
														<option value="2008">2008</option>
														<option value="2007">2007</option>
														<option value="2006">2006</option>
														<option value="2005">2005</option>
														<option value="2004">2004</option>
														<option value="2003">2003</option>
														<option value="2002">2002</option>
														<option value="2001">2001</option>
														<option value="2000">2000</option>
														<option value="1999">1999</option>
														<option value="1998">1998</option>
														<option value="1997">1997</option>
														<option value="1996">1996</option>
														<option value="1995">1995</option>
														<option value="1994">1994</option>
														<option value="1993">1993</option>
														<option value="1992">1992</option>
														<option value="1991">1991</option>
														<option value="1990">1990</option>
														<option value="1989">1989</option>
														<option value="1988">1988</option>
														<option value="1987">1987</option>
														<option value="1986">1986</option>
														<option value="1985">1985</option>
														<option value="1984">1984</option>
														<option value="1983">1983</option>
														<option value="1982">1982</option>
														<option value="1981">1981</option>
														<option value="1980">1980</option>
													</select>
												</div>
												<div class="selected-box col 6">
													<select>
														<option value="0" selected>Any</option>
														<option value="2019">2019</option>
														<option value="2018">2018</option>
														<option value="2017">2017</option>
														<option value="2016">2016</option>
														<option value="2015">2015</option>
														<option value="2014">2014</option>
														<option value="2013">2013</option>
														<option value="2012">2012</option>
														<option value="2011">2011</option>
														<option value="2010">2010</option>
														<option value="2009">2009</option>
														<option value="2008">2008</option>
														<option value="2007">2007</option>
														<option value="2006">2006</option>
														<option value="2005">2005</option>
														<option value="2004">2004</option>
														<option value="2003">2003</option>
														<option value="2002">2002</option>
														<option value="2001">2001</option>
														<option value="2000">2000</option>
														<option value="1999">1999</option>
														<option value="1998">1998</option>
														<option value="1997">1997</option>
														<option value="1996">1996</option>
														<option value="1995">1995</option>
														<option value="1994">1994</option>
														<option value="1993">1993</option>
														<option value="1992">1992</option>
														<option value="1991">1991</option>
														<option value="1990">1990</option>
														<option value="1989">1989</option>
														<option value="1988">1988</option>
														<option value="1987">1987</option>
														<option value="1986">1986</option>
														<option value="1985">1985</option>
														<option value="1984">1984</option>
														<option value="1983">1983</option>
														<option value="1982">1982</option>
														<option value="1981">1981</option>
														<option value="1980">1980</option>
													</select>
												</div>
											</div>
											<label>Steering</label>
											<div class="row">
												<div class="col-6">
													<div class="remember-checkbox">
														<input type="checkbox" name="left" id="left" />
														<label for="left">Left</label>
													</div>
												</div>
												<div class="col 6">
													<div class="remember-checkbox">
														<input type="checkbox" name="right" id="right" />
														<label for="right">Right</label>
													</div>
												</div>
											</div>
											<label>Seats</label>
											<div class="row">
												<div class="selected-box col-6">
													<select>
														<option value="0" selected>Any</option>
														<option value="2">2</option>
														<option value="4">4</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="10">10</option>
														<option value="15">15</option>
														<option value="19">19</option>
														<option value="20">20</option>
														<option value="30">30</option>
														<option value="40">40</option>
													</select>
												</div>
												<div class="selected-box col 6">
													<select>
														<option value="0" selected>Any</option>
														<option value="2">2</option>
														<option value="4">4</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="10">10</option>
														<option value="15">15</option>
														<option value="19">19</option>
														<option value="20">20</option>
														<option value="30">30</option>
														<option value="40">40</option>
													</select>
												</div>
											</div>
											<div class="selected-box">
												<select>
													<option>Tonnage  </option>
													<option>1</option>
													<option>2 </option>
													<option>3 </option>
													<option>4 </option>
													<option>5 </option>
												</select>
											</div>
											<label>Doors</label>
											<div class="row">
												<div class="col-3">
													<div class="remember-checkbox">
														<input type="checkbox" name="2" id="2" />
														<label for="2">2</label>
													</div>
												</div>
												<div class="col 3">
													<div class="remember-checkbox">
														<input type="checkbox" name="3" id="3" />
														<label for="3">3</label>
													</div>
												</div>
												<div class="col-3">
													<div class="remember-checkbox">
														<input type="checkbox" name="4" id="4" />
														<label for="4">4</label>
													</div>
												</div>
												<div class="col 3">
													<div class="remember-checkbox">
														<input type="checkbox" name="5" id="5" />
														<label for="5">5</label>
													</div>
												</div>
											</div>
											<div class="remember-checkbox">
												<input type="checkbox" name="wai" id="wai" />
												<label for="wai">Without Add.info ?</label>
											</div>
											<div id="slider-range"></div>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<div class="price-slide-2">
										<div class="price">
											<label>Mileage</label>
											<div class="row">
												<div class="selected-box col-6">
													<select>
														<option>Show  </option>
														<option>1</option>
														<option>2 </option>
														<option>3 </option>
														<option>4 </option>
														<option>5 </option>
													</select>
												</div>
												<div class="selected-box col 6">
													<select>
														<option>Show  </option>
														<option>1</option>
														<option>2 </option>
														<option>3 </option>
														<option>4 </option>
														<option>5 </option>
													</select>
												</div>
											</div>
											<label>Transmission</label>
											<div class="row">
												<div class="col-4">
													<div class="remember-checkbox">
														<input type="checkbox" name="at" id="at" />
														<label for="at">AT</label>
													</div>
												</div>
												<div class="col 4">
													<div class="remember-checkbox">
														<input type="checkbox" name="mt" id="mt" />
														<label for="mt">MT</label>
													</div>
												</div>
												<div class="col-4">
													<div class="remember-checkbox">
														<input type="checkbox" name="semi" id="semi" />
														<label for="semi">AT (Semi)</label>
													</div>
												</div>
											</div>
											<label>Engine Size</label>
											<div class="row">
												<div class="selected-box col-6">
													<select>
														<option value="0" selected>Any</option>
														<option value="660">660cc</option>
														<option value="1000">1,000cc</option>
														<option value="1300">1,300cc</option>
														<option value="1500">1,500cc</option>
														<option value="2000">2,000cc</option>
														<option value="2500">2,500cc</option>
														<option value="3000">3,000cc</option>
														<option value="4000">4,000cc</option>
													</select>
												</div>
												<div class="selected-box col 6">
													<select>
														<option value="0" selected>Any</option>
														<option value="660">660cc</option>
														<option value="1000">1,000cc</option>
														<option value="1300">1,300cc</option>
														<option value="1500">1,500cc</option>
														<option value="2000">2,000cc</option>
														<option value="2500">2,500cc</option>
														<option value="3000">3,000cc</option>
														<option value="4000">4,000cc</option>
													</select>
												</div>
											</div>
											<label>Price</label>
											<div class="row">
												<div class="selected-box col-6">
													<select>
														<option value="0" selected>Any</option>
														<option value="799">$799</option>
														<option value="1000">$1,000</option>
														<option value="1500">$1,500</option>
														<option value="2000">$2,000</option>
														<option value="3000">$3,000</option>
														<option value="4000">$4,000</option>
														<option value="5000">$5,000</option>
														<option value="7500">$7,500</option>
														<option value="10000">$10,000</option>
														<option value="15000">$15,000</option>
														<option value="20000">$20,000</option>
													</select>
												</div>
												<div class="selected-box col 6">
													<select>
														<option value="0" selected>Any</option>
														<option value="799">$799</option>
														<option value="1000">$1,000</option>
														<option value="1500">$1,500</option>
														<option value="2000">$2,000</option>
														<option value="3000">$3,000</option>
														<option value="4000">$4,000</option>
														<option value="5000">$5,000</option>
														<option value="7500">$7,500</option>
														<option value="10000">$10,000</option>
														<option value="15000">$15,000</option>
														<option value="20000">$20,000</option>
													</select>
												</div>
											</div>
											<label>Drive Type</label>
											<div class="row">
												<div class="col-6">
													<div class="remember-checkbox">
														<input type="checkbox" name="2wd" id="2wd" />
														<label for="2wd">2WD</label>
													</div>
												</div>
												<div class="col 6">
													<div class="remember-checkbox">
														<input type="checkbox" name="4wd" id="4wd" />
														<label for="4wd">4WD</label>
													</div>
												</div>
											</div>
											<div id="slider-range-2"></div>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="price-search">
										<div class="row">
											<div class="col-6">
												<div class="remember-checkbox">
													<input type="checkbox" name="newarrival" id="newarrival" />
													<label for="newarrival">New Arrival</label>
												</div>
												<div class="remember-checkbox">
													<input type="checkbox" name="discounted" id="discounted" />
													<label for="discounted">Discounted</label>
												</div>
												<div class="remember-checkbox">
													<input type="checkbox" name="thisweek" id="thisweek" />
													<label for="thisweek">Discounted This Week</label>
												</div>
											</div>
											<div class="col-6">
												<a href="<?php echo base_url() ?>users/searchlist" class="btn btn-warning btn-lg">View Results</a>
												<div class="row" style="padding:20px;">
													<a href="">clear all</a>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<label>Color</label>
												<div class="row">
													<div class="col-3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Pearl" id="Pearl" />
															<label for="Pearl" style="background:#f4b894;">Pearl</label>
														</div>
													</div>
													<div class="col 3">
														<div class="remember-checkbox">
															<input type="checkbox" name="White" id="White" />
															<label for="White" >White</label>
														</div>
													</div>
													<div class="col-3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Yellow" id="Yellow" />
															<label for="Yellow" style="background:#FFFF00;">Yellow</label>
														</div>
													</div>
													<div class="col 3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Orange" id="Orange" />
															<label for="Orange" style="background:#ffa500;">Orange</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Red" id="Red" />
															<label for="Red" style="background:red;">Red</label>
														</div>
													</div>
													<div class="col 3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Win" id="Win" />
															<label for="Win" style="background:#FFFF00;">Win Red</label>
														</div>
													</div>
													<div class="col-3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Pink" id="Pink" />
															<label for="Pink" style="background:#FFFF00;">Pink</label>
														</div>
													</div>
													<div class="col 3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Purple" id="Purple" />
															<label for="Purple" style="background:#FFFF00;">Purple</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Blue" id="Blue" />
															<label for="Blue" style="background:blue;">Blue</label>
														</div>
													</div>
													<div class="col 3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Green" id="Green" />
															<label for="Green" style="background:green; color:white;">Green</label>
														</div>
													</div>
													<div class="col-3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Brown" id="Brown" />
															<label for="Brown" style="background:#654321; color:white;">Brown</label>
														</div>
													</div>
													<div class="col 3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Beige" id="Beige" />
															<label for="Beige" style="background:#f5f5dc;">Beige</label>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Gold" id="Gold" />
															<label for="Gold" style="background:#ffd700;">Gold</label>
														</div>
													</div>
													<div class="col 3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Silver" id="Silver" />
															<label for="Silver" style="background:#aaa9ad; color:white;">Silver</label>
														</div>
													</div>
													<div class="col-3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Gray" id="Gray" />
															<label for="Gray" style="background:gray; color:white;">Gray</label>
														</div>
													</div>
													<div class="col 3">
														<div class="remember-checkbox">
															<input type="checkbox" name="Black" id="Black" />
															<label for="Black" style="background:Black; color:white;">Black</label>
														</div>
													</div>
												</div>
												<div class="remember-checkbox">
													<input type="checkbox" name="Other" id="Other" />
													<label for="Other">Other</label>
												</div>
											</div>
										</div>
									</div>
								</div>
                     </div>
                     <h6>Search By More Details</h6>
							<div class="row">
								<table id="jsWebKitTable" >
									<thead>
										<tr>
											<th  >Maker</th>
											<th  >Model</th>
											<th  >Photo</th>
											<th  >Price</th>
											<th  >Year</th>
											<th  >Mileage</th>
											<th  >Engine<span> Size/Type</span></th>
											<th  >Trans</th>
											<th  >Fuel<span> Type</span></th>
											<th  >Seats</th>
											<th  >Drive<span> Type</span></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($stocks as $stock): ?>
										<tr>
											<td  ><a href="<?php echo base_url(); ?>users/auctionview/<?php echo $stock['stock_id']; ?>" ><?php echo $stock['maker']; ?></a></td>
											<td  ><?php echo $stock['model']; ?></td>
											<td  ><img src="<?php echo base_url(); ?>assets/app-assets/images/car.jpg" class="responsive-img" alt="" style="height: 40px;"></td>
											<td  ><?php echo $stock['sale_price']; ?> $</td>
											<td  ><?php echo $stock['year']; ?></td>
											<td  ><?php echo $stock['milage']; ?><span> (km)</span></td>
											<td  ><?php echo $stock['engine_cc']; ?> <span> (cc)</span></td>
											<td  ><?php echo $stock['transmission']; ?></td>
											<td  ><?php echo $stock['fuel']; ?></td>
											<td  ><?php echo $stock['seats']; ?></td>
											<td  ><?php echo $stock['drive']; ?></td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
                     </div>
                     <button id="show" class="button " type="button" value="Load More" style="margin: 15px 0 0 0;" >Load More</button>
							
						</div>
					</div>
				</div>
			</div>
			<?php  
				//$this->load->view('templates/users/leftsidebar.php');
				?>
			<!-- !-- Left Side Bar -- -->
		</div>
	</div>

	<?php  $this->load->view('templates/users/leftsidebar.php');?> 
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
	$(function() {
	 var totalrowshidden;
	var rows2display=4;
	var rem=0;
	var rowCount=0;
	var forCntr;
	var forCntr1;
	var MaxCntr=0;
	   $('#hide').click(function() {  
	     var rowCount = $('#jsWebKitTable tr').length;
	 
	     rowCount=rowCount/2;
	     rowCount=Math.round(rowCount)
	 
	     for (var i = 0; i < rowCount; i++) {
	     $('tr:nth-child('+ i +')').hide(300); 
	     }                            
	   });
	
	$('#show').click(function() {
	rowCount = $('#jsWebKitTable tr').length;
	
	MaxCntr=forStarter+rows2display;
	
	 if (forStarter<=$('#jsWebKitTable tr').length)
	    {
	
	    for (var i = forStarter; i < MaxCntr; i++)
	    { 
	       $('tr:nth-child('+ i +')').show(200);
	    }
	
	    forStarter=forStarter+rows2display
	   
	     }
	else
	{
	 $('#show').hide();
	 }   
	     
	     
	     
	   });   
	
	
	
	$(document).ready(function() {
	var rowCount = $('#jsWebKitTable tr').length;
	  
	  
	for (var i = $('#jsWebKitTable tr').length; i > rows2display; i--) {
	rem=rem+1
	     $('tr:nth-child('+ i +')').hide(200);
	 
	     }
	forCntr=$('#jsWebKitTable tr').length-rem;
	forStarter=forCntr+1
	
	});
	
	});
	
</script>