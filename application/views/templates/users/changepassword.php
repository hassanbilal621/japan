<!-- 
   !--=================================
     header -- -->
     <section class="inner-intro bg-1 bg-overlay-black-70">
         <div class="container">
            <div class="row text-center intro-title">
               <div class="col-md-6 text-md-left d-inline-block">
                  <h1 class="text-white">Login </h1>
               </div>
               <div class="col-md-6 text-md-right float-right">
                  <ul class="page-breadcrumb">
                     <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                     <li><span>Login</span> </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
     <!-- rigth sidebar start -->
      <section class="page-both-sidebar page-section-ptb">
      <div class="container-fluid">
         <div class="row">
         <?php  $this->load->view('templates/users/rightsidebar.php');?> 

         <div class="col-md-7">
     
               <div class="container">
               
                  <div class="row justify-content-center">
                     <div class="col-lg-6 col-md-12" style="border:1px solid #e3e3e3;">
                        <div class="row justify-content-center">
                           <div class="col-md-12"  style="background:#db2d2e;">
                              <div class="section-title" style="margin-bottom:20px !important; margin-top:20px !important;">
                                 <h2  style="color:white;">Change Your Password</h2>
                              </div>
                           </div>
                        </div>
                        <div class="gray-form clearfix">
                           <div class="form-group">
                              <label for="pass">Current Password* </label>
                              <input id="name" class="form-control"  type="email" placeholder="Enter Your Email" >
                           </div>
                           <div class="form-group">
                              <label for="pass">New Password* </label>
                              <input id="Password" class="form-control" type="password" placeholder="Enter your new Password" >
                           </div>
                           <div class="form-group">
                              <label for="pass">Re-Enter Your New Password* </label>
                              <input id="Password" class="form-control" type="password" placeholder="Re-enter your new password" >
                           </div>
                        
                           <button type="submit" class="button red"> Change Password </button>
                        </div>
                     </div>
                  </div>
            
               </div>
          
         </div>

          <?php  $this->load->view('templates/users/leftsidebar.php');?> 
          <!-- !-- Left Side Bar -- -->
</div>
</div>
</section>



















