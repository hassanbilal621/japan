    <!-- BEGIN: Page Main-->
    <div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
			<div class="container">
				<div class="row">
					<div class="col s12 m6 l6">
						<h5 class="breadcrumbs-title mt-0 mb-0">Dashboard</h5>
					</div>
					<div class="col s12 m6 l6 right-align-md">
						<ol class="breadcrumbs mb-0">
						  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/">Home</a>
						  </li>
					
						  <li class="breadcrumb-item active">Dashboard
						  </li>
						</ol>
					</div>
				</div>
			</div>
        </div>
        <div class="col s12">
			<div id="card-with-analytics" class="section">
				<div class="row">
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content center-align">
										<i class="material-icons amber-text small-ico-bg mb-5">check</i>
										<h4 class="m-0"><b>21.5k</b></h4>
										<p>Total Stocks</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content center-align">
										<i class="material-icons amber-text small-ico-bg mb-5">sentiment_satisfied</i>
										<h4 class="m-0"><b>1.6k</b></h4>
										<p>Pending Stocks</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content center-align">
										<i class="material-icons amber-text small-ico-bg mb-5">radio_button_unchecked</i>
										<h4 class="m-0"><b>890</b></h4>
										<p>Active Stock</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content center-align">
										<i class="material-icons amber-text small-ico-bg mb-5">check</i>
										<h4 class="m-0"><b>50</b></h4>
										<p>Expired Stocks</p>
		
								</div>
							</div>
					</div>
				</div>

				<div class="row">
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content center-align">
										<i class="material-icons amber-text small-ico-bg mb-5">check</i>
										<h4 class="m-0"><b>21.5k</b></h4>
										<p>Total Orders</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content center-align">
										<i class="material-icons amber-text small-ico-bg mb-5">sentiment_satisfied</i>
										<h4 class="m-0"><b>1.6k</b></h4>
										<p>Pending Payments</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content center-align">
										<i class="material-icons amber-text small-ico-bg mb-5">radio_button_unchecked</i>
										<h4 class="m-0"><b>890</b></h4>
										<p>Pending Shipments</p>
									
								</div>
							</div>
					</div>
					<div class="col s12 m6 l3 card-width">
							<div class="card border-radius-6">
								<div class="card-content center-align">
										<i class="material-icons amber-text small-ico-bg mb-5">check</i>
										<h4 class="m-0"><b>50</b></h4>
										<p>Completed Orders</p>
		
								</div>
							</div>
					</div>
				</div>
			</div>
        </div>
    </div>
    </div>
    <!-- END: Page Main-->