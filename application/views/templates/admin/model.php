<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Model</h4>
                  <div class="row">
                     <div class="col s12">
                        <button class="waves-light blue btn modal-trigger mb-2 mr-1" href="#modal1">Add Model
                        <i class="material-icons left">person_add</i>
                        </button>
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <div class="row">
                                 <div class="col s12">
                                    <div class="card">
                                       <div class="col s12">
                                          <?php echo form_open('admin/model') ?>
                                          <!-- Form with placeholder -->
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="Model Name" name="modelname" type="text">
                                                <label for="model">Model Name</label>
                                             </div>
                                          </div>
                                          
                                          <div class="input-field col s12">
                                             <div class="row">
                                                <!-- <label for="maker">Select Maker *</label> -->
                                                <div class="selected-box auto-hight">
                                                <select class="form-control browser-default" name="makerid" required>

                                                   <option disabled selected>Select Maker</option>
                                                   <?php foreach ($makers as $maker): ?>

                                                   <?php if (empty($maker['maker_name'])) { }
                                                   else{        
                                                   ?>
                                                      <option value="<?php echo $maker['maker_id']; ?>"><?php echo $maker['maker_name']; ?></option>
                                                   <?php }?>


                                                   <?php endforeach; ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                         
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                       </div>
                                       <?php echo form_open() ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>Id</th>
                              <th>Model Name</th>
                              <th>Maker Name</th>
                              <th>Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach($models as $model): ?>
                           <tr>
                           <td><?php echo $model['model_id']; ?></td>
                              <td><?php echo $model['model_name']; ?></td>
                              <td><?php echo $model['maker_name'];?></td>
                            
                              <td> 
                                 <a href="<?php echo base_url();?>admin/deletemodel/<?php echo $model['model_id']; ?>" class="btn  waves-light red">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>                         
                             </td>
                           </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>


   <!-- Modal Structure -->
   <div id="modal11" class="modal">
      <div class="modal-content modal-content2 modal-body">
      </div>
   </div>

   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

   <script type='text/javascript'>


    
      }


   </script>