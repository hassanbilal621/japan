<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Users</h4>
                  <div class="row">
                     <div class="col s12">
                        <button class="waves-effect waves-light btn button red modal-trigger z-depth-2 mr-1 mb-2" href="#modal1">Add
                           <i class="material-icons left">person_add</i>
                        </button>
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <div class="row">
                                 <div class="col s12">
                                    <div class="card">
                                       <div class="col s12">
                                          <?php echo form_open('admin/addusers') ?>
                                          <!-- Form with placeholder -->
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="John Doe" name="name" type="text">
                                                <label for="name2">Name</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="john@domainname.com" name="email" type="email">
                                                <label for="email">Email</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="********" name="password" type="password">
                                                <label for="password">Password</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="Johar" name="address" type="text">
                                                <label for="address">Address</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="+920000000000" name="phone" type="text">
                                                <label for="phone2">Phone</label>
                                             </div>
                                          </div>
                                          <div class="input-field col s12">
                                             <div class="row">
                                                <label for="phone">Select Country *</label>
                                                <div class="selected-box auto-hight">
                                                   <select class="form-control" name="country" required>

                                                      <option disabled selected>Select Country</option>
                                                      <?php foreach ($countries as $country) : ?>

                                                         <?php if (empty($country['country_name'])) { } else {
                                                               ?>
                                                            <option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                                                         <?php } ?>


                                                      <?php endforeach; ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="input-field col s12">
                                             <div class="row">
                                                <div class="selected-box">
                                                   <label for="username2">Gender</label>
                                                   <select name="gender" required>
                                                      <option value="male">Male</option>
                                                      <option value="female">Female</option>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <button class="waves-effect waves-light btn button red z-depth-2 right" type="submit" name="action">Submit
                                                   <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                       </div>
                                       <?php echo form_open() ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Address</th>
                              <th>Phone</th>
                              <th>Country</th>
                              <th>Gender</th>
                              <th>Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($users as $user) : ?>
                              <tr>
                                 <td><?php echo $user['name']; ?></td>
                                 <td><?php echo $user['email']; ?></td>
                                 <td><?php echo $user['address']; ?></td>
                                 <td><?php echo $user['phone']; ?></td>
                                 <td><?php echo $user['country']; ?></td>
                                 <td><?php echo $user['gender']; ?></td>
                                 <td> <button id="<?php echo $user['id']; ?>" onclick="loaduserinfo(this.id)" class="userinfo waves-effect waves-light btn button blue darken-3 z-depth-2">Edit
                                       <i class="material-icons left">edit</i>
                                    </button>
                                    <button class="waves-effect waves-light btn button red z-depth-2" name="action">Delete
                                       <i class="material-icons left">delete_forever</i>
                                    </button>
                                    <a class="waves-effect waves-light btn button ml-3 white z-depth-2" href="<?php echo base_url(); ?>admin/consigneeform/<?php echo $user['id']; ?>" type="submit" name="action">CONSIGNEE FORM
                                       <i class="material-icons left">send</i>
                                    </a>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<!-- Modal Structure -->
<div id="modal11" class="modal">
   <div class="modal-content modal-content2 modal-body">
   </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>
   function loaduserinfo(userid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_usermodal/" + userid,
         data: 'country_name=pakistan',
         success: function(data) {
            $(".modal-content2").html(data);
            $('#modal11').modal('open');
         }
      });
   }
</script>