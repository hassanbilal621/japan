<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4 class="card-title">Consignee Form</h4>
                    <?php echo form_open('admin/addconsigneeform') ?>
                    <!-- Form with placeholder -->
                    <div class="row">
                        <div class="input-field col s2">
                            <input name="userid" type="text" value="<?php echo $user['id']; ?>" readonly>
                            <label for="name2">User ID</label>
                        </div>
                        <div class="input-field col s4">
                            <input type="text" value="<?php echo $user['name']; ?>" readonly>
                            <label for="name2">User Name</label>
                        </div>
                        <div class="input-field col s4">
                            <input type="email" value="<?php echo $user['email']; ?>" readonly>
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field col s2">
                            <input type="text" value="<?php echo $user['name']; ?>" readonly>
                            <label for="name2">Date</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s4">
                            <input name="customername" type="text">
                            <label for="name2">Customer Name</label>
                        </div>
                        <div class="input-field col s4">
                            <input name="consigneename" type="text">
                            <label for="name2">Consignee Name</label>
                        </div>
                        <div class="input-field col s4">
                            <input name="consigneeaddress" type="text">
                            <label for="name2">Consignee Address</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s4">
                            <input name="contact" type="number">
                            <label for="email">Contact No</label>
                        </div>
                        <div class="input-field col s4">
                            <input name="partyname" type="text">
                            <label for="email">Notify Party Name</label>
                        </div>
                        <div class="input-field col s4">
                            <input name="partyaddress" type="text">
                            <label for="email">Notify Party Address</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input name="finaldesport" type="text">
                            <label for="name2">Final Destination at Rec. Port</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="note" type="text">
                            <label for="address">Notes</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="waves-effect waves-light btn button red z-depth-2 right" type="submit" name="action">Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                    <?php echo form_open() ?>
                </div>
                <div class="row" style="padding: 40px;">
                    <table id="page-length-option" class="display">
                        <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Customer Name</th>
                                <th>Consignee Name</th>
                                <th>Consignee Address</th>
                                <th>contact</th>
                                <th>Party Name</th>
                                <th>Party Address</th>
                                <th>Final Destination port</th>
                                <th>Notes</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($consignees as $consignee) : ?>
                                <tr>
                                    <td><?php echo $consignee['name']; ?></td>
                                    <td><?php echo $consignee['customername']; ?></td>
                                    <td><?php echo $consignee['consigneename']; ?></td>
                                    <td><?php echo $consignee['consigneeaddress']; ?></td>
                                    <td><?php echo $consignee['contact']; ?></td>
                                    <td><?php echo $consignee['partyname']; ?></td>
                                    <td><?php echo $consignee['partyaddress']; ?></td>
                                    <td><?php echo $consignee['finaldesport']; ?></td>
                                    <td><?php echo $consignee['note']; ?></td>
                                    <td>
                                        <button id="<?php echo $consignee['consigneeformid']; ?>" onclick="loadconsigneeforminfo(this.id)" class="userinfo waves-effect waves-light btn button blue darken-3 z-depth-2">Edit
                                            <i class="material-icons left">edit</i>
                                        </button>
                                        <a class="waves-effect waves-light btn button red z-depth-2" href="<?php echo base_url(); ?>admin/deleteconsigneeform/<?php echo $consignee['consigneeformid']; ?>" name="action">Delete
                                            <i class="material-icons left">delete_forever</i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






<!-- Modal Structure -->
<div id="modal11" class="modal">
    <div class="modal-content modal-content2 modal-body">
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>
    function loadconsigneeforminfo(consigneeformid) {
        // var consigneeformid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_consigneeform/" + consigneeformid,
            data: 'country_name=pakistan',
            success: function(data) {
                $(".modal-content2").html(data);
                $('#modal11').modal('open');
            }
        });
    }
</script>