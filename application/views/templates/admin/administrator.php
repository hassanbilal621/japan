<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Page Length Options</h4>
                  <div class="row">
                     <div class="col s12">
                     <button class="waves-effect waves-light btn button red modal-trigger z-depth-2 mr-1 mb-2" href="#modal1">Add
                        <i class="material-icons left">person_add</i>
                        </button>
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <div class="row">
                                 <div class="col s12">
                                    <div class="card">
                                       <?php echo form_open('admin/addadmin')?>
                                       <div class="col s12">
                                          <!-- Form with placeholder -->
                                          <h4 class="card-title">Form with placeholder</h4>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="John Doe" name="name" id="name2" type="text">
                                                <label for="name2">Name</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="YourPassword" name="password" id="password2" type="password">
                                                <label for="password">Password</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="john@domainname.com" name="email" id="email2"type="email">
                                                <label for="email">Email</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="Username" name="username" id="username2"type="text">
                                                <label for="username2">Username</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="register date" name="register_date" id="register2" type="text">
                                                <label for="register2">Register Date</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="Assigned Class" name="assigned_class" id="assigned_class2" type="text">
                                                <label for="assigned_class2">Assigned Class</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <select>
                                                   <option name="staff_role" disabled selected>Choose your Role</option>
                                                   <option value="1">Admin</option>
                                                   <option value="2">Manager</option>
                                                   <option value="3">Saler</option>
                                                </select>
                                                <label>Select Role</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <div class="input-field col s12">
                                                   <button class="waves-effect waves-light btn button red z-depth-2 right" type="submit" name="action">Submit
                                                   <i class="material-icons right">send</i>
                                                   </button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <?php echo form_close('')?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <table id="page-length-option" class="display">
                           <thead>
                              <tr>
                                 <th>Name</th>
                                 <th>Username</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php foreach($admins as $admin): ?>
                              <tr>
                                 <td><?php echo $admin['name']; ?></td>
                                 <td><?php echo $admin['username'];?></td>
                                 <td>  <button class="waves-effect waves-light btn button blue darken-3 z-depth-2 " type="submit" name="action">Edit
                                    <i class="material-icons left">edit</i>
                                    </button>
                                    <button class="waves-effect waves-light btn button red z-depth-2  " type="submit" name="action">Delete
                                    <i class="material-icons left">delete_forever</i>
                                    </button>
                                 </td>
                              </tr>
                              <?php endforeach; ?>
                              </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
