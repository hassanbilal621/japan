<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Delivered Orders</h4>
                  <div class="row">
                     <div class="col s12">

                        <table id="page-length-option" class="display">
                           <thead>
                              <tr>
                                 <th>Order id</th>
                                 <th>Name</th>
                                 <th>Maker</th>
                                 <th>Model</th>
                                 <th>Payment</th>
                                 <th>Paid Amount</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody><?php foreach ($orders as $order) : ?>
                                 <tr>

                                    <td><?php echo $order['orderid']; ?></td>
                                    <td><?php echo $order['name']; ?></td>
                                    <td><?php echo $order['maker']; ?></td>
                                    <td><?php echo $order['model']; ?></td>
                                    <td><?php echo $order['sale_price']; ?></td>
                                    <td><?php echo $order['orderpaidamount']; ?></td>

                                    <td>

                                       <a href="<?php echo base_url(); ?>admin/addinvoice/<?php echo $order['orderid']; ?>" class="waves-light btn button blue z-depth-2 mt-2 mr-2 " name="action" style="padding: 0 4px 0 4px;">Add Invoice
                                          <i class=" material-icons left" style="margin: 0 6px 0 0;">add</i>
                                       </a>
                                       <a href="<?php echo base_url(); ?>admin/deleteorder/<?php echo $order['orderid']; ?>" class="waves-light btn button red z-depth-2  mt-2 mr-2 " type="submit" name="action">Delete
                                          <i class="material-icons left">delete_forever</i>
                                       </a>
                                       <a href="<?php echo base_url(); ?>admin/cancelorder/<?php echo $order['orderid']; ?>" class="waves-light btn button black z-depth-2 mt-2 mr-2" type="submit" name="action" style="padding: 0 18px 0 18px;">cencel
                                          <i class="material-icons left">cancel</i>
                                       </a>
                                    </td>
                                 </tr>
                              <?php endforeach; ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>