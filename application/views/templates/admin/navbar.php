   <!-- BEGIN: Header-->
   <header class="page-topbar" id="header">
     <div class="navbar navbar-fixed">
       <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-light">
         <div class="nav-wrapper">
           <ul class="navbar-list left">
             <li><a class="waves-effect waves-block waves-light" href="<?php echo base_url() ?>admin/manageinvoice""><i class=" material-icons"style="font-size: 50px;">keyboard_return</i></a></li>
           </ul>
           <ul class="navbar-list right">
             <li><a class="waves-effect waves-block waves-light" href="<?php echo base_url() ?>admin/search""><img src="<?php echo base_url(); ?>assets/app-assets\images\icon\search.png" alt="avatar"style="width: 45px;margin: 10px;"></i></a></li>
             <li><a class="waves-effect waves-block waves-light" href="<?php echo base_url(); ?>admin/logout"><i><img src="<?php echo base_url(); ?>assets/app-assets\images\icon\logout.png" alt="avatar"style="width: 45px;margin: 10px;"></i></a></li>
           </ul>
         </div>

       </nav>
     </div>
   </header>
   <!-- END: Header-->