<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Type</h4>
                  <div class="row">
                     <div class="col s12">
                        <button class="waves-light blue btn modal-trigger mb-2 mr-1" href="#modal1">Add Type
                        <i class="material-icons left">person_add</i>
                        </button>
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <div class="row">
                                 <div class="col s12">
                                    <div class="card">
                                       <div class="col s12">
                                          <?php echo form_open('admin/type') ?>
                                          <!-- Form with placeholder -->
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="type Name" name="typename" type="text">
                                                <label for="type">Type Name</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input placeholder="Type Icon" name="typeicon" type="text">
                                                <label for="type">Type Icon</label>
                                             </div>
                                          </div>
                                         <div class="row">
                                             <div class="input-field col s12">
                                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                       </div>
                                       <?php echo form_open() ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>Id</th>
                              <th>Type Name</th>
                              <th>Type Icon</th>
                              <th>Actions</th> 
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach($types as $type): ?>
                           <tr>
                           <td><?php echo $type['type_id'];?></td>
                           <td><?php echo $type['type_name'];?></td>
                           <td><?php echo $type['type_icon'];?></td>
                            <td><a href="<?php echo base_url();?>admin/deletetype/<?php echo $type['type_id']; ?>" class="btn  waves-light red">Delete<i class="material-icons left">delete_forever</i></a></td>
                           </tr> 
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>


   <!-- Modal Structure -->
   <div id="modal11" class="modal">
      <div class="modal-content modal-content2 modal-body">
      </div>
   </div>

   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

   <script type='text/javascript'>


    
      }


   </script>