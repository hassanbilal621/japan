<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
	th,td
	{
	text-align: center;
	border: 2px black solid;
	padding: 8px;
	}
	th
	{
		color: red;
	}
	td
	{
		color: blue;

	}
input
	{
	color: blue;
    text-align: center;
	border: none;
	width: 39%;
	}
	
</style>
<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-content">
					<?php echo form_open('admin/addvoucherdetail');?>
					<div class="row" >
						<div class="input-field col s3">
							<h4 class="card-title">Add payment Voucher</h4>
						</div>
						<div class="input-field col s3 right">
							<input type="date" name="date" require="">
						</div>
					</div>
					<div>
                    <div class="row">
						<div class="input-field col s6">
                        
                        <select class="browser-default" name="find">
                            <?php foreach ($orders as $order): ?>
                                <option value="<?php echo $order['orderid']; ?>"><?php echo $order['name']; ?>/Order no/<?php echo $order['orderid']; ?> </option>
                            <?php endforeach; ?>
                        </select>
                        <a href="<?php echo base_url(); ?>admin/addinvoice/<?php echo $order['orderid'];?>" class="waves-light btn button white z-depth-2 mt-2 mr-2 " name="action"style="padding: 0 4px 0 4px;">Add Invoice
                            <i class=" material-icons left"style="margin: 0 6px 0 0;">done</i>
                        </a>
                        </div>
                    </div>   
						<div class="row">
							<div class="input-field col s6">
								<table>
									<tbody>
										<tr>
											<th>User ID</th>
											<td></td>
										</tr>
										<tr>
											<th>Person name</th>
											<td></td>
										</tr>
										<tr>
											<th>Gender</th>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="input-field col s6">
								<table>
									<tbody>
										<tr>
											<th>Email</th>
											<td></td>
										</tr>
										<tr>
											<th>Order Date</th>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div></div>
						<div class="row">
							<div class="col s3">
								<label for="beneficiary">Select Beneficiary *</label>
								<select class="browser-default"  onchange="bank(this.value)" name="beneficiary_id" required>
									<option disabled selected>Select Beneficiary Name</option>
									<?php foreach ($bank as $bank): ?>
									<?php if (empty($bank['beneficiary_name'])) { }
										else{?>
									<option value="<?php echo $bank['bank_id']; ?>"><?php echo $bank['beneficiary_name']; ?></option>
									<?php }?>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col s3">
								<label for="beneficiary">Bank Name *</label>
								<input type="text"  id="bankname" value="" readonly>
							</div>
							<div class="col s3">
								<label for="beneficiary">Account number *</label>
								<input type="number" value="" readonly>
							</div>
						</div>
					</div>
				</div>
				<table style="margin: 30px 10px 2px 31px;width: 94%;" >
					<thead>
						<tr>
							<th>Maker</th>
							<th>Model</th>
							<th>Year</th>
							<th>Engine Number</th>
							<th>Chassis Number</th>
							<th>City/Country</th>
							<th>Colour</th>
						</tr>
					</thead>
					<tbody >
						<tr>
							<td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
						</tr>
					</tbody>
				</table>
				<div class="input-field col s6 right">
					<table style="margin: 31px -3px 30px 15px;width: 94%;" >
						<tbody>
							<tr>
								<th>Car Amount</th>
								<td><input type="number" value="" name='sub_total' placeholder='0.00'  id="order_amount" readonly /></td>
							</tr>
							<tr>
								<th>Paid Amount</th>
								<td><input type="number" value="" name='paid_amount' id="paid_amount" placeholder='0.00'  readonly /></td>
							</tr>
							<tr>
								<th>Total</th>
								<td><input type="number" name='discount_amount'  id="due_amount" value="0.00" placeholder='0.00'  readonly /></td>
							</tr>
							<tr>
								<th>Pay</th>
								<td>
									<div class="input-group mb-2 mb-sm-0">
										<input type="number"  name="pay" onchange="onpay(this.value)" value="" id="pay_amount" placeholder="0.00">
									</div>
								</td>
							</tr>
							<tr>
								<th>Grand Total</th>
								<td><input type="number" name='grand_total' id="total_amount"  value="0.00" placeholder='0.00'  readonly /></td>
							</tr>
						</tbody>
					</table>
					<button class="waves-light btn button red z-depth-2  mb-10 mt-2 mr-2" type="submit" name="action"style="padding: 0 18px 0 18px;">submit
					<i class="material-icons left">send</i> 
					</button>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close();?>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
	$(document).ready(function(){
		var orderamount = document.getElementById("order_amount").value;
		var paidamount = document.getElementById("paid_amount").value;
		var totalremainingdue = Number(orderamount) - Number(paidamount);
		document.getElementById("due_amount").value = totalremainingdue;
	});
	function onpay(){
		var payamount = document.getElementById("pay_amount").value;
		var reamain = document.getElementById("due_amount").value
		var total = Number(reamain) - Number(payamount);
		document.getElementById("total_amount").value = total;
	}
</script>
