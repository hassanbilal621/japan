<?php




// foreach($api_countries as $api_countrie):

//     echo $api_countrie['name'];
//     echo '<br>';

// endforeach;


?>


<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Countries</h4>
                  <div class="row">
                     <div class="col s12">
                        
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>Name</th>
                              <th>Country Code</th>
                              <th>Capital</th>
                              <th>Flag</th>
                              <th>Sub Region</th>
                              <th>Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach($api_countries as $api_country): ?>
                           <tr>
                              <td><?php echo $api_country['country_name']; ?></td>
                              <td><?php echo $api_country['country_nativename'];?></td>
                              <td><?php echo $api_country['country_capital'];?></td>
                              <td><img src="<?php echo $api_country['country_flag'];?>" width="32px" /></td>
                              <td><?php echo $api_country['country_region'];?></td>
                              <td> <a href="<?php echo base_url(); ?>admin/editcounty/<?php echo $api_country['country_id'];?>" class="waves-light btn button blue darken-3  z-depth-2  mt-5 ">View
                                 <i class="material-icons left">view</i>
                              </a>                     
                             </td>
                           </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



   <!-- Modal Structure -->
   <div id="modal11" class="modal">
      <div class="modal-content modal-content2 modal-body">
      </div>
   </div>

   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

   <script type='text/javascript'>


      function loaduserinfo(userid){
         // var userid = this.id;
            $.ajax({
               type: "GET",
               url: "<?php echo base_url();?>admin/ajax_edit_usermodal/"+userid,
               data:'country_name=pakistan',
               success: function(data){
                  $(".modal-content2").html(data);
                  $('#modal11').modal('open');
               }
            });
      }


   </script>