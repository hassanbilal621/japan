<div id="main">
    <div class="row">
        <!-- Page Length Options -->
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h4 class="card-title">Company Info</h4>
                        <div class="row">
                            <div class="col s12">

                                <?php echo form_open('admin/') ?>
                                    <!-- Form with placeholder -->
                                  
                                    <div class="row">
                                       
                                        <div class="input-field col s12">
                                            <input id="name2" type="text" name="name">
                                            <label for="name2">Company Name</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                       
                                    
									<label for="inputEmail3" class="col-sm-2 col-form-label">Logo</label>
									<div class="col-sm-6">
										<img style="width:311px; height:75px;" src="<?php echo base_url(); ?>assets/front-app-assets/images/logo-dark.png" class="logo">
									
								</div>
                                    </div>
                                    <div class="row">
                                       

                                        <div class=" col s12">
                                        <label for="Year2">Change Logo</label>
                                            <input id="Year2" type="file" name="year">
                                            
                                        </div>
                                    </div>
                                 
                                    <div class="row">
                                    <div class="input-field col s12">
                                    <select id="setting_currency" name="setting_currency" value="" class="form-control" data-plugin="select2">
                                        <option value="">Select Currency</option>
                                        <option  value="$">$</option>
                                        <option  value="£">£</option>
                                        <option  value="€">€</option>
                                        <option  value="¥">¥</option>                                        
                                        <option  value="₤">₤</option>
                                        <option  value="Bs">Bs</option>	
                                        <option selected value="Rs">Rs</option> 
                                        <option  value="Rp">Rp</option>                                       
                                        <option  value="&#65020;">&#65020;</option>										
										</select>
 
  </div>

</div>
                                    <div class="row">
                                      

                                        <div class="input-field col s12">
                                            <input id="Fuel2" type="text" name="fuel">
                                            <label for="Fuel2">Fuel</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                       

                                        <div class="input-field col s12">
                                            <input id="address" type="text" name="address">
                                            <label for="address">Address</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                     

                                        <div class="input-field col s12">
                                            <input id="city" type="text"  name="city">
                                            <label for="city">City</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                       

                                        <div class="input-field col s12">
                                            <input id="country" type="text" name="country">
                                            <label for="country">Country</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                       

                                        <div class="input-field col s12">
                                            <input id="phone" type="text" name="phone">
                                            <label for="phone">Phone</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                      

                                        <div class="input-field col s12">
                                            <input id="fax" type="text" name="fax">
                                            <label for="fax">Fax</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                     
                                        <div class="input-field col s12">
                                            <input id="website" type="text" name="website">
                                            <label for="website">Website</label>
                                        </div>
                                    </div>
                                  
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Update Company Info
                                                <i class="material-icons right">update</i>
                                            </button>
                                        </div>
                                    </div>
                            </div>
                            <?php echo form_open() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal Structure -->
<div id="modal11" class="modal">
    <div class="modal-content modal-content2 modal-body">
    </div>
</div>
<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script type='text/javascript'>
    function loaduserinfo(userid) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_usermodal/" + userid,
            data: 'country_name=pakistan',
            success: function(data) {
                $(".modal-content2").html(data);
                $('#modal11').modal('open');
            }
        });
    }
</script>