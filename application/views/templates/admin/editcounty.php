
<style>
.slide
    {
        margin: 25px 0 0 0;
        border: 3px black solid;
        width: 300px;
        float: right;
    }
.prewslide
    {
        margin: 20px 0 0 50px;
    }
</style>
<div id="main">
    <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h4 class="card-title">Edit Stock</h4>
                        <div class="row">
                            <div class="col s12">

                                <?php echo form_open_multipart('admin/updatecountry') ?>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input id="countryname" value="<?php echo $api_country['country_name']; ?>" type="text"name="auction_date" readonly>
                              
                                            <input id="countrynativename" value="<?php echo $api_country['country_nativename']; ?>" type="text"name="auction_date" readonly>
                                        
                                            <input id="countrycapital" value="<?php echo $api_country['country_capital']; ?>" type="text"name="auction_date" readonly>
                                        
                                            <input id="countryregion" value="<?php echo $api_country['country_region']; ?>" type="text"name="auction_date" readonly >
                                        </div>
                                        <div class="input-field col s6">
                                            <img src="<?php echo $api_country['country_flag'];?>"style="margin: 42px 100px 0 0;border: 3px black solid;width: 235px;float: right;"> 
                                        </div>
                                    </div>
                                 
                                    <div class="row">
                                    <div class="input-field col s4">
                                            <input type="hidden" name="country_id" value="<?php echo $api_country['country_id']; ?>" >
                                            <img src="<?php echo $api_country['img01'];?>"class="slide"> 
                                            <input type="file" name="userfile" class="prewslide" onchange="previewimg(this);" accept="image/*" >
                                         </div>
                                    
                                        <div class="input-field col s4">
                                            <img src="<?php echo $api_country['img02'];?>"class="slide"> 
                                            <input type="file" name="userfile" class="prewslide" onchange="previewimg(this);" accept="image/*" >
                                         </div>
                                    
                                    <div class="input-field col s4">
                                            <img src="<?php echo $api_country['img03'];?>"class="slide"> 
                                            <input type="file" name="userfile" class="prewslide"  onchange="previewimg(this);" accept="image/*" >
                                         </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input id="policy" type="text" name="policy" class="validate">
                                            <label for="policy">Policy</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="newarival" type="text"name="newarival" class="validate">
                                            <label for="newarival">New Arival</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input id="brand" type="text"name="brand" class="validate">
                                            <label for="brand">Brand</label>
                                        </div>
                                   
                                        <div class="input-field col s6">
                                            <input id="discount" type="text"name="discount" class="validate">
                                            <label for="discount">Discount</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button  class="waves-light btn button blue darken-3 z-depth-2  mt-2 mr-2  right" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                            </div>
                            <?php echo form_open() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>