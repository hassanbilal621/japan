
  
  <!-- BEGIN: SideNav-->

   <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
      

	<div class="brand-sidebar">
		<h1 class="logo-wrapper">
    <a href="" class="brand-logo darken-1">
      <img src="<?php echo base_url();?>assets\app-assets\images\logo/onlylogo.png" style="width: 100px !important;height: 45px !important;margin: -14px 0 0 -30px;" alt="Nexco Japan">
        <span class="logo-text hide-on-med-and-down"> 
          <img src="<?php echo base_url();?>assets\app-assets\images\logo/logotext.png" style="width: 65% !important;margin: 0 0 13px -36px;" alt="Nexco Japan">
        </span>
      </a>
			<a class="navbar-toggler" href="#">
			<i class="material-icons"style="margin: 0 -10px 0 0;">radio_button_checked</i>
			</a>
		</h1>
	</div>

	<ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="accordion">
 <li class="navigation-header"><a class="navigation-header-text">Dashboard</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/"><i class="material-icons"style="color: #ce1e28;">dashboard</i><span class="menu-title" data-i18n="">Home</span></a>
        </li>
      

      <li class="navigation-header"><a class="navigation-header-text">Users</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/users"><i class="material-icons"style="color: #ce1e28;">perm_identity</i><span class="menu-title" data-i18n="">Clients</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/administrator"><i class="material-icons"style="color: #ce1e28;">person_pin</i><span class="menu-title" data-i18n="">Staff</span></a>
        </li>
     
        <li class="navigation-header"><a class="navigation-header-text">Stocks</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li> 

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/addstock"><i class="material-icons"style="color: #ce1e28;">import_contacts</i><span class="menu-title" data-i18n="">Import Stock</span></a>
        </li>

        <!-- <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/stocks"><i class="material-icons">import_contacts</i><span class="menu-title" data-i18n="">Stocks</span></a>
        </li> -->

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/pendingstock"><i class="material-icons"style="color: #ce1e28;">schedule</i><span class="menu-title" data-i18n="">Pending Stocks</span></a>
        </li>

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/activestocks"><i class="material-icons"style="color: #ce1e28;">assignment</i><span class="menu-title" data-i18n="">Active Stocks</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/expiredstocks"><i class="material-icons"style="color: #ce1e28;">hourglass_empty</i><span class="menu-title" data-i18n="">Expired Stocks</span></a>
        </li>




        <li class="navigation-header"><a class="navigation-header-text">Orders</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li> 


        <!-- <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/stocks"><i class="material-icons">import_contacts</i><span class="menu-title" data-i18n="">Stocks</span></a>
        </li> -->

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/pendingpayment"><i class="material-icons"style="color: #ce1e28;">payment</i><span class="menu-title" data-i18n="">Pending Payments Orders</span></a>
        </li>

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/activeorders"><i class="material-icons"style="color: #ce1e28;">assignment</i><span class="menu-title" data-i18n="">Active Orders</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/deliveredorder"><i class="material-icons"style="color: #ce1e28;">receipt</i><span class="menu-title" data-i18n="">Delivered Orders</span></a>
        </li>

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/cancledorder"><i class="material-icons"style="color: #ce1e28;">cancel</i><span class="menu-title" data-i18n="">Canceled Orders</span></a>
        </li>

        <li class="navigation-header"><a class="navigation-header-text">Invoice</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li> 


        <!-- <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/stocks"><i class="material-icons">import_contacts</i><span class="menu-title" data-i18n="">Stocks</span></a>
        </li> -->

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/invoice"><i class="material-icons"style="color: #ce1e28;">add</i><span class="menu-title" data-i18n="">Add Invoice</span></a>
        </li>

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/manageinvoice"><i class="material-icons"style="color: #ce1e28;">assignment</i><span class="menu-title" data-i18n="">Manage Invoice</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/paidinvoices"><i class="material-icons"style="color: #ce1e28;">receipt</i><span class="menu-title" data-i18n="">Paid Invoice</span></a>
        </li>

        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/unpaidinvoices"><i class="material-icons"style="color: #ce1e28;">subtitles</i><span class="menu-title" data-i18n="">Unpaid Invoice</span></a>
        </li>
        <!-- <li class="navigation-header"><a class="navigation-header-text">Services</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>

        <li class="bold"><a class="waves-effect waves-cyan " href="app-calendar.html"><i class="material-icons">add_a_photo</i><span class="menu-title" data-i18n="">Photography</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="app-calendar.html"><i class="material-icons">import_contacts</i><span class="menu-title" data-i18n="">Paperwork</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="app-calendar.html"><i class="material-icons">gavel</i><span class="menu-title" data-i18n="">Inspection</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="app-calendar.html"><i class="material-icons">library_add</i><span class="menu-title" data-i18n="">Additional Special Requests</span></a>
        </li> -->

        <li class="navigation-header"><a class="navigation-header-text">Setup</a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/bank"><i class="material-icons"style="color: #ce1e28;">account_balance</i><span class="menu-title" data-i18n="">Bank</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/countryport"><i class="material-icons"style="color: #ce1e28;">location_city</i><span class="menu-title" data-i18n="">Country Ports</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/model"><i class="material-icons"style="color: #ce1e28;">directions_car</i><span class="menu-title" data-i18n="">Model</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/type"><i class="material-icons"style="color: #ce1e28;">merge_type</i><span class="menu-title" data-i18n="">Type</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/maker"><i class="material-icons"style="color: #ce1e28;">build</i><span class="menu-title" data-i18n="">Maker</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/country"><i class="material-icons"style="color: #ce1e28;">location_city</i><span class="menu-title" data-i18n="">Country</span></a>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url();?>admin/settings"><i class="material-icons"style="color: #ce1e28;">settings</i><span class="menu-title" data-i18n="">Settings</span></a>
        </li>
        <li class="navigation-header"><a class="navigation-header-text"></a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>
        <li class="navigation-header"><a class="navigation-header-text"></a><i class="navigation-header-icon material-icons">more_horiz</i>
        </li>
      </ul>
 

      <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons"style="color: #ce1e28;">menu</i></a>
    </aside>
    <!-- END: SideNav-->