

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/flatly/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/filter/styless.css">

    

    
    <div class="row">
        <div class="col s12 m12 l12">
        
            <div class="card-panel">
                <div class="row">
                    <div class="container">
     <!--card stats start-->
     <div id="card-stats">
              <div class="row">
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">add_shopping_cart</i>
                        <p>All Orders</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0">690</h5>
                        <p class="no-margin">New</p>
                        <p>6,00,00</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">perm_identity</i>
                        <p>Clients</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0">1885</h5>
                        <p class="no-margin">New</p>
                        <p>1,12,900</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">timeline</i>
                        <p>Companies</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0">80%</h5>
                        <p class="no-margin">Growth</p>
                        <p>3,42,230</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">attach_money</i>
                        <p>All Jobs</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0">$890</h5>
                        <p class="no-margin">Today</p>
                        <p>$25,000</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--card stats end-->

                         <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-8"><h5>All Orders</h5></div>
                                </div>  
                            </div>
                            <div class="container">
                                <input type="search" id="search" class="form-control" placeholder="Type here to Search">
                                <div id="root"></div>
                                <div class="pages"></div>
                            </div>
                            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                            <script src="<?php echo base_url(); ?>assets/filter/table-sortable.js"></script>
                            <script>
                            var data = [
                                        <?php foreach ($orders as $allorder): ?>
                                        {
                                            orderid: "<?php echo $allorder['orderid']; ?>",
                                            date_posted: "<?php echo $allorder['date_posted']; ?>",
                                            desc: "<?php echo $allorder['desc']; ?>",
                                            academic_level_name: "<?php echo $allorder['academic_level_name']; ?>",
                                            typeofpaper_names: "<?php echo $allorder['typeofpaper_names']; ?>",
                                            noofdays_days: "<?php echo $allorder['noofdays_days']." Days"; ?>",
                                            noofpages_numbers: "<?php echo $allorder['noofpages_numbers']." Pages"; ?>",
                                            total: "<?php echo $allorder['academic_price'] + $allorder['noofdays_amount'] + $allorder['typeofpaper_price'] + $allorder['noofpages_amount'] ; ?> PKR Rupees",
                                            actions: '<?php if($allorder['status'] == "complete") { ?> <a href="<?php echo base_url();?>assets/uploads/<?php echo $allorder['download_file']; ?>" class="delete " title="Delete" data-toggle="tooltip"> <button class="btn  light-green darken-2 modal-trigger"><b>Download and View File<b></button></a><?php } elseif($allorder['status'] == "pending")  {  ?> <div class="orange darken-2 delete btn modal-trigger" style="color: rgba(255, 255, 255, 0.901961);">Pending Order</div>  <?php } else { ?><a href="<?php echo base_url(); ?>paypal/express_checkout/index" class="delete" title="Delete" data-toggle="tooltip"> <button class="btn modal-trigger"><b>Pay Now<b></button></a>  <?php } ?>'
                                        },
                                        <?php endforeach; ?>
                                        ]

                                    var columns = {
                                        'orderid': 'Order ID',
                                        'date_posted': 'Date Posted',
                                        'desc': 'Description',
                                        'academic_level_name': 'Academic ',
                                        'typeofpaper_names': 'Type Of Paper',
                                        'noofdays_days': 'No of Days',
                                        'noofpages_numbers': 'No Of Pages',
                                        'total': 'Total Price',
                                        'actions': 'Action'
                                    }
         
                            </script>
                            <script src="<?php echo base_url(); ?>assets/filter/script.js"></script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>