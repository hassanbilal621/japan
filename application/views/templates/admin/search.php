<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>assets/date/daterangepicker.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/date/daterangepicker.js"></script>
<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="card">
                        <div class="card-content">
                            <div class="row mb-3">
                                <div class="col s1 p-0">
                                    <h6>CHASSIS NO</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="chassis_no" placeholder="Type Chassis No">
                                </div>
                                <div class="col s2">
                                    <h6>AVAILABILITY</h6>
                                </div>
                                <div class="col s2">
                                    <label>
                                        <input type="checkbox" id="sold" name="sold" value="sold" />
                                        <span>SOLD</span>
                                    </label>
                                </div>
                                <div class="col s2">
                                    <label>
                                        <input type="checkbox" id="reserved" name="reserved" value="reserved" />
                                        <span>RESERVED</span>
                                    </label>
                                </div>
                                <div class="col s2">
                                    <label>
                                        <input type="checkbox" id="available" name="available" value="available" />
                                        <span>AVAILABLE</span>
                                    </label>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s1 p-0">
                                    <h6>KOBUTSU NO</h6>
                                </div>
                                <div class="col s2">
                                    <input type="text" name="kobutsu_no" placeholder="Type kobutsu No">
                                </div>
                                <div class="col s1 p-0">
                                    <h6>STATUS</h6>
                                </div>
                                <div class="col s2 p-0">
                                    <label>
                                        <input type="checkbox" id="sold" name="sold" value="sold" />
                                        <span>IN TRANSIT</span>
                                    </label>
                                </div>
                                <div class="col s2 p-0">
                                    <label>
                                        <input type="checkbox" id="reserved" name="reserved" value="reserved" />
                                        <span>IN YARD </span>
                                    </label>
                                </div>
                                <div class="col s2 p-0">
                                    <label>
                                        <input type="checkbox" id="available" name="available" value="available" />
                                        <span>SHIPPED </span>
                                    </label>
                                </div>
                                <div class="col s2 p-0">
                                    <label>
                                        <input type="checkbox" id="available" name="available" value="available" />
                                        <span>RELEASED </span>
                                    </label>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s12">
                                    <h6>Stock Id</h6>
                                    <input type="text" name="stockid" placeholder="Type Stock Id">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s12">
                                    <h6>BRAND</h6>
                                    <input type="text" name="timeselection" id="demo1" class="form-control">
                                </div>
                                <div class="col s12">
                                    <h6>AUCTION HOUSE</h6>
                                    <input type="text" name="timeselection" id="demo2" class="form-control">
                                </div>
                                <div class="col s12">
                                    <h6>AGENT</h6>
                                    <input type="text" name="timeselection" id="demo3" class="form-control">
                                </div>
                                <div class="col s12">
                                    <h6>CUSTOMER</h6>
                                    <input type="text" name="timeselection" id="demo4" class="form-control">
                                </div>
                                <div class="col s12">
                                    <h6>COUNTRY</h6>
                                    <input type="text" name="timeselection" id="demo5" class="form-control">
                                </div>
                                <div class="col s12">
                                    <h6>CONSIGNEE</h6>
                                    <input type="text" name="timeselection" id="demo6" class="form-control">
                                </div>

                            </div>
                            <div class="row mb-3">
                                <div class="col s12">
                                    <h6>BUYER</h6>
                                    <input type="text" name="buyer" placeholder="Type buyer Name">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s12">
                                    <h6>SHIP</h6>
                                    <input type="text" name="ship" placeholder="Type Ship">
                                </div>
                            </div>
                            <!-- payment search -->
                            <h3>Payment Search</h3>
                            <div class="row mb-3">
                                <div class="col s2">
                                    <h6>VOYAGE NO</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="voyage" placeholder="Type Voyage No">
                                </div>
                                <div class="col s1">
                                    <h6>Tax Date</h6>
                                </div>
                                <div class="col s6">
                                    <input type="text" name="timeselection" id="demo7" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s2">
                                    <h6>BL NO</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="bl" placeholder="Type Bl No">
                                </div>
                                <div class="col s1">
                                    <h6>RECYCLE Date</h6>
                                </div>
                                <div class="col s6">
                                    <input type="text" name="timeselection" id="demo8" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s2">
                                    <h6>REGISTRATION YEAR</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="registrationyear" placeholder="Type Registration Year">
                                </div>
                                <div class="col s1">
                                    <h6>BUYING PRICE</h6>
                                </div>
                                <div class="col s6">
                                    <input type="text" name="timeselection" id="demo9" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s2">
                                    <h6>Port</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="port" placeholder="Type port">
                                </div>
                                <div class="col s1">
                                    <h6>Auc Fee</h6>
                                </div>
                                <div class="col s6">
                                    <input type="text" name="timeselection" id="demo10" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s2">
                                    <h6>YARD</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="voyage" placeholder="Type Voyage No">
                                </div>
                                <div class="col s1">
                                    <h6>Tax Date</h6>
                                </div>
                                <div class="col s6">
                                    <input type="text" name="timeselection" id="demo11" class="form-control">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s2">
                                    <h6>STATUS</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="status" placeholder="Type Car Status">
                                </div>
                                <div class="col s1">
                                    <h6>COURIER CHARGES</h6>
                                </div>
                                <div class="col s6">
                                    <input type="text" name="startcharges" class="col s5 mr-3" placeholder="Type Charges From">
                                    <input type="text" name="endcharges" class="col s5" placeholder="Type Charges To">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s2">
                                    <h6>PRICE</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="voyage" placeholder="Type price">
                                </div>
                                <div class="col s1">
                                    <h6>Payment</h6>
                                </div>
                                <div class="col s6">
                                    <input type="text" name="startpayment" class="col s5 mr-3" placeholder="Type Payment From">
                                    <input type="text" name="endpayment" class="col s5" placeholder="Type Payment To">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col s1">
                                    <h6>MAKER</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="maker" placeholder="Type Maker">
                                </div>
                                <div class="col s1">
                                    <h6>RHD/LHD</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="rhd-lhd" placeholder="Type RHD/LHD">
                                </div>
                                <div class="col s1">
                                    <h6>SHIFT</h6>
                                </div>
                                <div class="col s3">
                                    <input type="text" name="shift" placeholder="type Shift">
                                </div>
                            </div>
                            <a href="<?php echo base_url() ?>admin/search" class="waves-light btn button red z-depth-2">View Results</a>
                            
                            <div class="row">
                                <table id="jsWebKitTable">
                                    <thead>
                                        <tr>
                                            <th>Maker</th>
                                            <th>Model</th>
                                            <th>Photo</th>
                                            <th>Price</th>
                                            <th>Year</th>
                                            <th>Mileage</th>
                                            <th>Engine<span> Size/Type</span></th>
                                            <th>Trans</th>
                                            <th>Fuel<span> Type</span></th>
                                            <th>Seats</th>
                                            <th>Drive<span> Type</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($stocks as $stock) : ?>
                                            <tr>
                                                <td><a href="<?php echo base_url(); ?>users/auctionview/<?php echo $stock['stock_id']; ?>"><?php echo $stock['maker']; ?></a></td>
                                                <td><?php echo $stock['model']; ?></td>
                                                <td><img src="<?php echo base_url(); ?>assets/app-assets/images/car.jpg" class="responsive-img" alt="" style="height: 40px;"></td>
                                                <td><?php echo $stock['sale_price']; ?> $</td>
                                                <td><?php echo $stock['year']; ?></td>
                                                <td><?php echo $stock['milage']; ?><span> (km)</span></td>
                                                <td><?php echo $stock['engine_cc']; ?> <span> (cc)</span></td>
                                                <td><?php echo $stock['transmission']; ?></td>
                                                <td><?php echo $stock['fuel']; ?></td>
                                                <td><?php echo $stock['seats']; ?></td>
                                                <td><?php echo $stock['drive']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <button id="show" class="button " type="button" value="Load More" style="margin: 15px 0 0 0;">Load More</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('.datepicker').datepicker();
    });
</script>
<script>
    $('#demo1').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo2').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo3').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo4').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo5').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo6').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo7').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo8').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo9').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo10').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo11').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo12').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>
<script>
    $('#demo13').daterangepicker({
        "showWeekNumbers": true,
        "startDate": "02/07/2020",
        "endDate": "02/20/2020"
    }, function(start, end, label) {
        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
    $(function() {
        var totalrowshidden;
        var rows2display = 4;
        var rem = 0;
        var rowCount = 0;
        var forCntr;
        var forCntr1;
        var MaxCntr = 0;
        $('#hide').click(function() {
            var rowCount = $('#jsWebKitTable tr').length;

            rowCount = rowCount / 2;
            rowCount = Math.round(rowCount)

            for (var i = 0; i < rowCount; i++) {
                $('tr:nth-child(' + i + ')').hide(300);
            }
        });

        $('#show').click(function() {
            rowCount = $('#jsWebKitTable tr').length;

            MaxCntr = forStarter + rows2display;

            if (forStarter <= $('#jsWebKitTable tr').length) {

                for (var i = forStarter; i < MaxCntr; i++) {
                    $('tr:nth-child(' + i + ')').show(200);
                }

                forStarter = forStarter + rows2display

            } else {
                $('#show').hide();
            }



        });



        $(document).ready(function() {
            var rowCount = $('#jsWebKitTable tr').length;


            for (var i = $('#jsWebKitTable tr').length; i > rows2display; i--) {
                rem = rem + 1
                $('tr:nth-child(' + i + ')').hide(200);

            }
            forCntr = $('#jsWebKitTable tr').length - rem;
            forStarter = forCntr + 1

        });

    });
</script>