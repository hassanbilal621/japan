<!DOCTYPE html>
<html lang="en">
  <!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Nexco Japan CO LTD</title>
    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url(); ?>assets/admin/images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/admin/images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/themes/semi-dark-menu/materialize.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/themes/semi-dark-menu/style.css" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/custom/custom.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/layouts/page-center.css" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url(); ?>assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
  </head>
	<body class="cyan">
    <div id="login-page" class="row">
      <div class="col s12 z-depth-4 card-panel">
        <?php echo form_open('admin/login'); ?>
            <div class="login-form">
                <div class="row">
                    <div class="input-field col s12 center">
                    <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" class="responsive-img valign">
                    <?php if($this->session->flashdata('login_failed')): ?>
                        <div id="card-alert" class="card gradient-45deg-amber-amber">
                            <div class="card-content white-text">
                                <p> <?php echo $this->session->flashdata('login_failed'); ?></p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    <?php endif; ?>
                    
                    <?php if($this->session->flashdata('user_loggedout')): ?>

                        <div id="card-alert" class="card green">
                            <div class="card-content white-text">
                                <p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    <?php endif; ?>

                    <p class="center login-form-text">Welcome Back! to admin dashboard</p>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                    <i class="material-icons prefix pt-5">person_outline</i>
                    <input id="username" name="username" type="text">
                    <label for="username" class="center-align">Username</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                    <i class="material-icons prefix pt-5">lock_outline</i>
                    <input id="password" name="password" type="password">
                    <label for="password">Password</label>
                    </div>
                </div>
            
                <div class="row">
                    <div class="input-field col s12">
                    <button type="submit" name="login" class="btn red waves-effect waves-light col s12">Login</button>
                    </div>
                </div>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/vendors/jquery-3.2.1.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/custom-script.js"></script>
  </body>
</html>