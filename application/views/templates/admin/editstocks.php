<style>
    img {
        max-width: 200px;
        margin-top: 10px;
        display: content;
    }

    .fileupload {
        padding: 10px;
    }
</style>
<div id="main">
    <div class="row">
        <!-- Page Length Options -->
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h4 class="card-title">dehaze Stock</h4>
                        <div class="row">
                            <div class="col s12">
                                <?php echo form_open_multipart('admin/uploadImage') ?>
                                <div class="row section">
                                    <div class="col s12 m4 l4">
                                        <p>Upload Car Image</p>
                                        <input id="img2" type='file' name='files[]' multiple="" onchange="previewimg(this);" accept="image/*">
                                        <input type="hidden" name="stockid" value="<?php echo $stock['stock_id']; ?>">
                                        <button class="waves-light btn button blue darken-2  z-depth-2  mt-5 " type="sumbit">upload image</button>
                                        <img id="view" src="<?php echo base_url(); ?>assets/images/select.png" alt="your image">
                                    </div>
                                    <div class="col s12 m8 l8">

                                        <div class="carousel">
                                            <?php foreach ($stock_pictures as $stock_picture) : ?>
                                                <a class="carousel-item modal-trigger" id="<?php echo $stock_picture['stock_picture_id']; ?>" onclick="openModal(this.id, this.getAttribute('data-imagenaame'))" data-imagenaame="<?php echo base_url(); ?>assets/uploads/<?php echo $stock_picture['stock_picture_filename']; ?>"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $stock_picture['stock_picture_filename']; ?>"></a>
                                            <?php endforeach; ?>
                                        </div>

                                    </div>
                                    <div id="modal1" class="modal">
                                        <div class="modal-content">
                                            <center> <img src="" id="modalimage" /></center>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="danger btn button" id="deleteimage" href=""> Delete Image </a>
                                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                                        </div>
                                    </div>

                                    <div class="col s12 m12 l12">


                                    </div>
                                </div>
                                <?php echo form_close() ?>
                                <!------------------------------------------ picture form end -------------------------------------------->

                                <!------------------------------------------ Vehical info Button Start-------------------------------------------->
                                <div class="row">
                                    <div class="col s12">
                                        <a id="<?php echo $stock['stock_id']; ?>" onclick="loadstockinfo(this.id)" class="left waves-effect waves-light btn button red darken-2 z-depth-2">Vehicle Info
                                            <i class="material-icons left">dehaze</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php echo form_open('admin/updatestock') ?>
                                    <div class="input-field col s6">
                                        <h6>Maker</h6>
                                        <input type="text" value="<?php echo $stock['maker']; ?>" name="maker">
                                        <input type="hidden" value="<?php echo $stock['stock_id']; ?>" name="stockid">
                                        <h6>Model</h6>
                                        <input type="text" value="<?php echo $stock['model']; ?>" name="model">
                                        <h6>YEAR</h6>
                                        <input type="text" value="<?php echo $stock['year']; ?>" name="year">
                                        <h6>FUEL</h6>
                                        <input type="text" value="<?php echo $stock['fuel']; ?>" name="fuel">
                                    </div>
                                    <div class="input-field col s6">
                                        <h6>Chassis No</h6>
                                        <input type="text" value="<?php echo $stock['chassis_number']; ?>" name="chassis_number">
                                        <h6>Transmition</h6>
                                        <input type="text" value="<?php echo $stock['transmission']; ?>" name="transmission">
                                        <h6>KM</h6>
                                        <input type="text" value="<?php echo $stock['km']; ?>" name="km">
                                        <h6>Engine CC</h6>
                                        <input type="text" value="<?php echo $stock['engine_cc']; ?>" name="engine_cc">
                                    </div>
                                    <button class="right waves-effect waves-light btn button blue mr-3 darken-3 z-depth-2">Save
                                        <i class="material-icons left">save</i>
                                    </button>
                                    <?php echo form_open() ?>
                                </div>
                                <!------------------------------------------ Vehical info feild End-------------------------------------------->

                                <!------------------------------------------ AUCTION INFO Button Start-------------------------------------------->
                                <div class="row">
                                    <div class="col s12">
                                        <a id="<?php echo $stock['stock_id']; ?>" onclick="loadauctioninfo(this.id)" class="left waves-effect waves-light btn button red darken-2 z-depth-2">Auction
                                            <i class="material-icons left">dehaze</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php echo form_open('admin/updateauction') ?>
                                    <div class="input-field col s6">
                                        <h6>Auction Name</h6>
                                        <input type="hidden" value="<?php echo $auction['stock_id']; ?>" name="stockid">
                                        <input type="text" value="<?php echo $auction['actionname']; ?>" name="actionname">
                                        <h6>Auction Date</h6>
                                        <input type="text" value="<?php echo $auction['actiondate']; ?>" name="actiondate">
                                    </div>
                                    <div class="input-field col s6">
                                        <h6>Lot No</h6>
                                        <input type="text" value="<?php echo $auction['lotno']; ?>" name="lotno">
                                        <h6>Transportaition Due Date</h6>
                                        <input type="text" value="<?php echo $auction['transportaition_date']; ?>" name="transportaition_date">

                                    </div>
                                    <button class="right waves-effect waves-light btn button blue mr-3 darken-3 z-depth-2">Save
                                        <i class="material-icons left">save</i>
                                    </button>
                                    <?php echo form_open() ?>
                                </div>
                                <!------------------------------------------ AUCTION INFO feild End-------------------------------------------->

                                <!------------------------------------------ ricksu Button Start-------------------------------------------->
                                <div class="row">
                                    <div class="col s12">
                                        <a id="<?php echo $stock['stock_id']; ?>" onclick="loadricksuinfo(this.id)" class="left waves-effect waves-light btn button red darken-2 z-depth-2">Ricksu
                                            <i class="material-icons left">dehaze</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php echo form_open('admin/updatericksu') ?>
                                    <div class="input-field col s6">
                                        <h6>Yard Name</h6>
                                        <input type="hidden" value="<?php echo $ricksu['stock_id']; ?>" name="stockid">
                                        <input type="text" value="<?php echo $ricksu['yard_name']; ?>" name="yard_name">
                                        <h6>Loading Point</h6>
                                        <input type="text" value="<?php echo $ricksu['loadingpoint']; ?>" name="loadingpoint">
                                        <h6>Remaing Free Days</h6>
                                        <input type="text" value="<?php echo $ricksu['remaingfreedays']; ?>" name="remaingfreedays">
                                    </div>
                                    <div class="input-field col s6">
                                        <h6>Picture</h6>
                                        <img src="<?php echo base_url(); ?>assets/uploads<?php echo $ricksu['picture']; ?>" alt="Yadr Picture">
                                    </div>
                                    <button class="right waves-effect waves-light btn button blue mr-3 darken-3 z-depth-2">Save
                                        <i class="material-icons left">save</i>
                                    </button>
                                    <?php echo form_open() ?>
                                </div>
                                <!------------------------------------------ ricksu feild End-------------------------------------------->

                                <!------------------------------------------ Price Button Start-------------------------------------------->
                                <div class="row">
                                    <div class="col s12">
                                        <a id="<?php echo $stock['stock_id']; ?>" onclick="loadpriceinfo(this.id)" class="left waves-effect waves-light btn button red darken-2 z-depth-2">Price
                                            <i class="material-icons left">dehaze</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php echo form_open('admin/updateprice') ?>
                                    <div class="input-field col s6">
                                        <h6>Sold Price</h6>
                                        <input type="hidden" value="<?php echo $price['stock_id']; ?>" name="stockid">
                                        <input type="text" value="<?php echo $price['soldprice']; ?>" name="soldprice">
                                        <h6>Sold Term</h6>
                                        <input type="text" value="<?php echo $price['soldterm']; ?>" name="soldterm">
                                    </div>
                                    <div class="input-field col s6">
                                        <h6>Currency</h6>
                                        <input type="text" value="<?php echo $price['currency']; ?>" name="currency">
                                        <h6>Currency</h6>
                                        <input type="text" value="<?php echo $price['currency']; ?>" name="currency">
                                    </div>
                                    <button class="right waves-effect waves-light btn button blue mr-3 darken-3 z-depth-2">Save
                                        <i class="material-icons left">save</i>
                                    </button>
                                    <?php echo form_open() ?>

                                </div>
                                <!------------------------------------------ Price feild End-------------------------------------------->

                                <!------------------------------------------ Payment info Button Start-------------------------------------------->
                                <div class="row">
                                    <div class="col s12">
                                        <a id="<?php echo $stock['stock_id']; ?>" onclick="loadPaymentinfo(this.id)" class="left waves-effect waves-light btn button red darken-2 z-depth-2">Payment
                                            <i class="material-icons left">dehaze</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php echo form_open('admin/updatePayment') ?>
                                    <div class="input-field col s6">
                                        <h6>Pay Amount</h6>
                                        <input type="hidden" value="<?php echo $payment['stock_id']; ?>" name="stockid">
                                        <input type="text" value="<?php echo $payment['payamount']; ?>" name="payamount">
                                        <h6>%</h6>
                                        <input type="text" value="<?php echo $payment['inperson']; ?>" name="inperson">
                                    </div>
                                    <div class="input-field col s6">
                                        <h6>Date</h6>
                                        <input type="text" value="<?php echo $payment['date']; ?>" name="date">
                                    </div>
                                    <button class="right waves-effect waves-light btn button blue mr-3 darken-3 z-depth-2">Save
                                        <i class="material-icons left">save</i>
                                    </button>
                                    <?php echo form_open() ?>
                                </div>
                                <!------------------------------------------ Payment info feild End-------------------------------------------->

                                <!------------------------------------------ reservation Button Start-------------------------------------------->
                                <div class="row">
                                    <div class="col s12">
                                        <a id="<?php echo $stock['stock_id']; ?>" onclick="loadreservedinfo(this.id)" class="left waves-effect waves-light btn button red darken-2 z-depth-2">reservation
                                            <i class="material-icons left">dehaze</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php echo form_open('admin/updatereserved') ?>
                                    <div class="input-field col s6">
                                        <h6>Reserved By</h6>
                                        <input type="hidden" value="<?php echo $reserved['stock_id']; ?>" name="stockid">
                                        <input type="text" value="<?php echo $reserved['reservedby']; ?>" name="reservedby">
                                        <h6>Sold By</h6>
                                        <input type="text" value="<?php echo $reserved['soldby']; ?>" name="soldby">
                                    </div>
                                    <div class="input-field col s6">
                                        <h6>Reservation Expiry</h6>
                                        <input type="text" value="<?php echo $reserved['reservation_expiry']; ?>" name="reservation_expiry">
                                        <h6>Date of Sold</h6>
                                        <input type="text" value="<?php echo $reserved['date_of_sold']; ?>" name="date_of_sold">
                                    </div>
                                    <button class="right waves-effect waves-light btn button blue mr-3 darken-3 z-depth-2">Save
                                        <i class="material-icons left">save</i>
                                    </button>
                                    <?php echo form_open() ?>
                                </div>
                                <!------------------------------------------ reservation feild End-------------------------------------------->

                                <!------------------------------------------ shipment Button Start-------------------------------------------->
                                <div class="row">
                                    <div class="col s12">
                                        <a id="<?php echo $stock['stock_id']; ?>" onclick="loadshipmentinfo(this.id)" class="left waves-effect waves-light btn button red darken-2 z-depth-2">Shipment
                                            <i class="material-icons left">dehaze</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php echo form_open('admin/updateshipment') ?>
                                    <div class="input-field col s6">
                                        <h6>Shiping Date</h6>
                                        <input type="hidden" value="<?php echo $shipment['stock_id']; ?>" name="stockid">
                                        <input type="text" value="<?php echo $shipment['shiping_date']; ?>" name="shiping_date">
                                    </div>
                                    <div class="input-field col s6">
                                        <h6>Name of Ship</h6>
                                        <input type="text" value="<?php echo $shipment['nameofship']; ?>" name="nameofship">
                                    </div>
                                    <button class="right waves-effect waves-light btn button blue mr-3 darken-3 z-depth-2">Save
                                        <i class="material-icons left">save</i>
                                    </button>
                                    <?php echo form_open() ?>
                                </div>
                                <!------------------------------------------ shipment feild End-------------------------------------------->

                                <!------------------------------------------ document Button Start-------------------------------------------->
                                <div class="row">
                                    <div class="col s12">
                                        <a id="<?php echo $stock['stock_id']; ?>" onclick="loaddocumentinfo(this.id)" class="left waves-effect waves-light btn button red darken-2 z-depth-2">Document
                                            <i class="material-icons left">dehaze</i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php echo form_open('admin/updatedocument') ?>
                                    <div class="input-field col s6">
                                        <h6>Mashou</h6>
                                        <input type="hidden" value="<?php echo $document['stock_id']; ?>" name="stockid">
                                        <input type="text" value="<?php echo $document['mashou']; ?>" name="mashou">
                                        <h6>Shiping Order</h6>
                                        <input type="text" value="<?php echo $document['shipingorder']; ?>" name="shipingorder">
                                        <h6>Inspection</h6>
                                        <input type="text" value="<?php echo $document['inspection']; ?>" name="inspection">
                                    </div>
                                    <div class="input-field col s6">
                                        <h6>Export Certificate</h6>
                                        <input type="text" value="<?php echo $document['exportcertificate']; ?>" name="exportcertificate">
                                        <h6>Bl_Arrival</h6>
                                        <input type="text" value="<?php echo $document['bl_arrival']; ?>" name="bl_arrival">
                                        <h6>Translations</h6>
                                        <input type="text" value="<?php echo $document['translations']; ?>" name="translations">
                                    </div>
                                    <button id="<?php echo $stock['stock_id']; ?>" onclick="loadricksuinfo(this.id)" class="right waves-effect waves-light btn button blue mr-3 darken-3 z-depth-2">Ricksu
                                        <i class="material-icons left">save</i>
                                    </button>
                                    <?php echo form_open() ?>
                                </div>
                                <!------------------------------------------ document feild End-------------------------------------------->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>


<script>
    function previewimg(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#view')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script>
    $(document).ready(function() {
        $('.carousel').carousel();

        $('.datepicker').datepicker();

        $('.modal').modal();

    });
</script>

<script>
    function openModal(imageid, imagelink) {


        document.getElementById("modalimage").src = imagelink;
        document.getElementById("deleteimage").href = "<?php echo base_url(); ?>admin/delete_stock_picture/" + imageid + "?stockid=<?php echo $stock['stock_id']; ?>";

        $('#modal1').modal('open');
        // alert("sadasd");
        // alert(imageid );

    }
</script>


<!-------------------------------------------------------------- stock model ------------------------------->
<div id="modal_1" class="modal">
    <div class="modal-content modal-body">
    </div>
</div>


<script type='text/javascript'>
    function loadstockinfo(stockid) {
        // var stockid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_stock/" + stockid,
           
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal_1').modal('open');
            }
        });
    }
</script>

<!-------------------------------------------------------------- Auction model ------------------------------->

<div id="modal_2" class="modal">
    <div class="modal-content modal-body">
    </div>
</div>


<script type='text/javascript'>
    function loadauctioninfo(stockid) {
        // var stockid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_auction/" + stockid,
           
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal_2').modal('open');
            }
        });
    }
</script>


<!-------------------------------------------------------------- stock model ------------------------------->


<div id="modal_3" class="modal">
    <div class="modal-content modal-body">
    </div>
</div>


<script type='text/javascript'>
    function loadricksuinfo(stockid) {
        // var stockid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_ricksu/" + stockid,
           
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal_3').modal('open');
            }
        });
    }
</script>

<!-------------------------------------------------------------- stock model ------------------------------->

<div id="modal_4" class="modal">
    <div class="modal-content modal-body">
    </div>
</div>



<script type='text/javascript'>
    function loadpriceinfo(stockid) {
        // var stockid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_price/" + stockid,
           
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal_4').modal('open');
            }
        });
    }
</script>

<!-------------------------------------------------------------- stock model ------------------------------->


<div id="modal_5" class="modal">
    <div class="modal-content modal-body">
    </div>
</div>



<script type='text/javascript'>
    function loadPaymentinfo(stockid) {
        // var stockid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_payment/" + stockid,
           
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal_5').modal('open');
            }
        });
    }
</script>

<!-------------------------------------------------------------- stock model ------------------------------->

<div id="modal_6" class="modal">
    <div class="modal-content modal-body">
    </div>
</div>


<script type='text/javascript'>
    function loadreservedinfo(stockid) {
        // var stockid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_reserved/" + stockid,
           
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal_6').modal('open');
            }
        });
    }
</script>

<!-------------------------------------------------------------- stock model ------------------------------->

<div id="modal_7" class="modal">
    <div class="modal-content modal-body">
    </div>
</div>


<script type='text/javascript'>
    function loadshipmentinfo(stockid) {
        // var stockid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_shipment/" + stockid,
           
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal_7').modal('open');
            }
        });
    }
</script>

<!-------------------------------------------------------------- stock model ------------------------------->

<div id="modal_8" class="modal">
    <div class="modal-content modal-body">
    </div>
</div>


<script type='text/javascript'>
    function loaddocumentinfo(stockid) {
        // var stockid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_document/" + stockid,
           
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal_8').modal('open');
            }
        });
    }
</script>