<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Mange invoice</h4>
                  <div class="row">
                     <div class="col s12">
                  
                        <table id="page-length-option" class="display">
                           <thead>
                              <tr>
                                 <th>invoice No.</th>
                                 <th>Date</th>
                                 <th>Name</th>
                                 <th>Beneficiary Name</th>
                                 <th>Total Amount</th>
                                 <th>Paid Amount</th>
                                 <th>Invoice Amount</th>
                                 <th>Status</th>
                                 <th>Actions</th>
                              </tr>
                           </thead>
                           <tbody>
                           <?php foreach($invoices as $invoice):?>
                              <tr>
                                 <td><?php echo $invoice['invoice_id'];?></td>
                                 <td><?php echo $invoice['date'];?></td>
                                 <td><?php echo $invoice['name'];?></td>
                                 <td><?php echo $invoice['beneficiary_name'];?></td>
                                 <td><?php echo $invoice['sale_price'];?></td>
                                 <td><?php echo $invoice['orderpaidamount'];?></td>
                                 <td><?php echo $invoice['pay_amount'];?></td>
                                 <td><?php echo $invoice['invoicestatus'];?></td>

                                 <td>  
                                 
                                 
                                 <a href="<?php echo base_url(); ?>users/invoice/<?php echo  $invoice['invoice_id']; ?>" class="waves-light btn button white z-depth-2 mt-2 mr-2 " name="action"style="padding: 0 4px 0 4px;">View
                                    <i class=" material-icons left"style="margin: 0 6px 0 0;">done</i>
                                 </a>
                                 <a href="<?php echo base_url(); ?>admin/paidinvoice/<?php echo $invoice['invoice_id'];?>" class="waves-light btn button white z-depth-2 mt-2 mr-2 " name="action"style="padding: 0 4px 0 4px;">Paid invoice
                                    <i class=" material-icons left"style="margin: 0 6px 0 0;">attach_money</i>
                                 </a>
                                 <a href="<?php echo base_url(); ?>admin/editinvoice/<?php echo $invoice['invoice_id'];?>" class="waves-light btn button blue darken-3 z-depth-2 mt-2 mr-2" type="submit" name="action"style="padding: 0 18px 0 18px;">Edit
                                    <i class="material-icons left">edit</i>
                                 </a>
                                 <a href="<?php echo base_url(); ?>admin/deleteunpaidinvoice/<?php echo $invoice['invoice_id'];?>" class="waves-light btn button red z-depth-2  mt-2 mr-2 " type="submit" name="action">Delete
                                    <i class="material-icons left">delete_forever</i>
                                 </a>
                                 </td>
                              </tr>
                                       
                              <?php endforeach; ?>    
                           </tbody>
                        </table>  
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
