<div id="main">
  <div class="row">
  

        
    
      

 <!-- Page Length Options -->
   
      
                
               
 <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">

       
     
          <h4 class="card-title">Page Length Options</h4>
            
            <div class="row">
              <div class="col s12">



              <button class="waves-effect waves-light blue btn modal-trigger mb-2 mr-1" href="#modal1">Add Stocks
              <i class="material-icons left">shopping_basket</i>
              </button>
                
                <div id="modal1" class="modal">
                <div class="modal-content">
                    
                
        
                <div class="row">
    <div class="col s12">
      <div class="card">
               
                <form class="col s12">

                     <!-- Form with placeholder -->
            
                     <h4 class="card-title">Form with placeholder</h4>

                    <div class="row">
                    <div class="input-field col s12">
                        <input id="Auctiondate2" type="text">
                        <label for="Auctiondate2">Auction date </label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Auction2" type="text">
                        <label for="Auction2">Auction</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="LotNumber2" type="text">
                        <label for="LotNumber2">Lot Number</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Maker2" type="text">
                        <label for="Maker2">Maker</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input id="Model2" type="text">
                        <label for="Model2">Model</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="ChassisCode2" type="text">
                        <label for="ChassisCode2">Chassis Code</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Chassis#2" type="text">
                        <label for="Chassis#2">Chassis #</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input id="Year2" type="text">
                        <label for="Year2">Year</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="EngineCC2" type="text">
                        <label for="EngineCC2">Engine CC</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <textarea id="Transmission2" class="materialize-textarea"></textarea>
                        <label for="Transmission2">Transmission</label>
                    </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input id="Version/Class2" type="text">
                <label for="Version/Class2">Version/Class</label>
              </div>
            </div>
            <div class="row">
                    <div class="input-field col s12">
                        <input id="Drive2" type="text">
                        <label for="Drive2">Drive</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input id="EngineCode2" type="text">
                        <label for="EngineCode2">Engine Code</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Fuel2" type="text">
                        <label for="Fuel2">Fuel</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Color2" type="text">
                        <label for="Color2">Color</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Doors2" type="text">
                        <label for="Doors2">Doors</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Seats2" type="text">
                        <label for="Seats2">Seats</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Dimension2" type="text">
                        <label for="Dimension2">Dimension (L x W x H)</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="M32" type="text">
                        <label for="M32">M3</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Weight2" type="text">
                        <label for="Weight2">Weight</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Grade/Package2" type="text">
                        <label for="Grade/Package2">Grade/Package</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Mileage2" type="text">
                        <label for="Mileage2">Mileage</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Condition2" type="text">
                        <label for="Condition2">Condition</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Location2" type="text">
                        <label for="Location2">City Location</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="StatePrice2" type="text">
                        <label for="StatePrice2">State Price</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="WonPrice2" type="text">
                        <label for="WonPrice2">Won Price</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="StandardsFeatures2" type="text">
                        <label for="StandardsFeatures2">Standards Features</label>
                    </div>
                    </div>
                    <div class="row">
                    <div class="input-field col s12">
                        <input  id="Reference#2" type="text">
                        <label for="Reference#2">Reference #</label>
                    </div>
                    </div>
                   
              <div class="row">
                <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      </div>
      </div>
      </div>


                <table id="page-length-option" class="display">
                <thead>
                    <tr>
                      <th>Name</th>
                      <th>Position</th>
                      <th>Office</th>
                      <th>Age</th>
                      <th>Start date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>61</td>
                      <td>2011/04/25</td>
                      <td>  <button class="btn waves-effect waves-light blue " type="submit" name="action">Edit
    <i class="material-icons left">edit</i>
  </button>
  <button class="btn waves-effect waves-light red  " type="submit" name="action">Delete
    <i class="material-icons left">delete_forever</i>
   
  </button></td>
                    </tr>
                    <tr>
                      <td>Garrett Winters</td>
                      <td>Accountant</td>
                      <td>Tokyo</td>
                      <td>63</td>
                      <td>2011/07/25</td>
                      <td>  <button class="btn waves-effect waves-light blue " type="submit" name="action">Edit
    <i class="material-icons left">edit</i>
  </button>
  <button class="btn waves-effect waves-light red  " type="submit" name="action">Delete
    <i class="material-icons left">delete_forever</i>
   
  </button></td>
                    </tr>
                   
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
   
  </div>
  </div>
  </div>
   

  <!-- BEGIN VENDOR JS-->
  <script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->