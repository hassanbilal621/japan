<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Add Bank Account</h4>
                  <div class="box box-block bg-white">
                  <?php echo form_open('admin/bank');?>
                     <div >
                        <div class="row">
                           <div class="col s6">
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input id="name2" name="beneficiary_name" type="text">
                                    <label for="name2">Beneficiary Name</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input id="bankname2" name="bank_name" type="text">
                                    <label for="bankname2">Name Of Bank</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input id="swiftcode2" name="swift_code" type="text">
                                    <label for="swiftcode2">Swift Code</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input id="acceptablecurrency2" name="acceptable_currency" type="text">
                                    <label for="acceptablecurrency2">Acceptable Currency</label>
                                 </div>
                              </div>
                           </div>
                           <div class="col s6">
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input id="accountno2" name="account_no" type="text">
                                    <label for="accountno2">Account No.</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input id="branchname2" name="branch_name" type="text">
                                    <label for="branchname2">Branch Name</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input id="bankaddress2" name="bank_address" type="text">
                                    <label for="bankaddress2">Bank Address</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <div class="input-field col s12">
                                 <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                 <i class="material-icons right">send</i>
                                 </button>
                              </div>
                           </div>
                        </div>
                  </div>
                  <?php echo form_close(); ?>
                  </div>							
               </div>
            </div>
         </div>
    </div>
   </div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->